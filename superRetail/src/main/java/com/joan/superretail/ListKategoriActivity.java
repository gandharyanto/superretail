package com.joan.superretail;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.ListPelangganAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.model.Jenis;

import java.util.ArrayList;
import java.util.Collections;

public class ListKategoriActivity extends Activity {

    TextView titleTV;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ListView listPelangganLV;
    ArrayList<String> idList = new ArrayList<String>();
    ArrayList<String> nameList = new ArrayList<String>();
    ArrayList<String> countList = new ArrayList<String>();
    ArrayList<Jenis> jenisList = new ArrayList<Jenis>();
    String idPelanggan;
    String levelHarga;
    String nama;
    String alamat;
    String aktif;
    String perintah;
    String so;
    Cursor pelangganCsr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_kategori);
        dataBase = DataBaseManager.instance();
        fm = new FontManager(this);
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        listPelangganLV = (ListView) findViewById(R.id.listKategoriLV);
        Bundle extras = getIntent().getExtras();
        idPelanggan = extras.getString("idPelanggan");
        levelHarga = extras.getString("levelHarga");
        nama = extras.getString("nama");
        alamat = extras.getString("alamat");
        aktif = extras.getString("aktif");
        so = extras.getString("so");
        titleTV.setTypeface(medium);
//		Toast.makeText(getApplicationContext(),extras.getString("perintah"),Toast.LENGTH_LONG).show();
        perintah = extras.getString("perintah");

        dataBase.exportDB();
        if (extras.getString("perintah").equals("sales")) {
            pelangganCsr = dataBase.selectJenis();
            if (pelangganCsr.getCount() > 0) {
                idList.clear();
                nameList.clear();
                jenisList.clear();
                pelangganCsr.moveToFirst();
                while (pelangganCsr.isAfterLast() == false) {
                    String id = pelangganCsr.getString(0);
                    String Nama = pelangganCsr.getString(1);
                    Cursor countCsr;
                    //if (levelHarga.equals("1") || levelHarga.equals("2") || levelHarga.equals("6")) {
                    if (so.equalsIgnoreCase("so1")) {
                        countCsr = dataBase.selectKategoriCount12(id, idPelanggan);
                    } else {
                        countCsr = dataBase.selectKategoriCount3(id, idPelanggan);
                    }
                    countCsr.moveToFirst();
                    String counter = countCsr.getString(0);
//				Cursor levelCsr = dataBase.selectLevelPelanggan(idPelanggan);
//    			levelCsr.moveToFirst();
//    			String LevelHarga = levelCsr.getString(0);
//    			if(levelHarga.equals("1")||levelHarga.equals("2"))
//    			{
                    if (!counter.equals("0")) {
                        countList.add(counter);
                        idList.add(id);
                        nameList.add(Nama + " (" + counter + ")");
                        Jenis jens = new Jenis(id, Nama + " (" + counter + ")", counter);
                        jenisList.add(jens);
                    }
//    			}
//    			else
//    			{
//    				countList.add(counter);
//    				idList.add(id);
//    				nameList.add(Nama+" ("+counter+")");
//    			}


                    pelangganCsr.moveToNext();
                }
                Collections.sort(jenisList, Jenis.StuNameComparator);
                listPelangganLV.setAdapter(new ListPelangganAdapter(context, nameList, jenisList));
                listPelangganLV.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        if (Integer.parseInt(countList.get(position)) > 0) {

                            try {
                                Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                                intent.putExtra("idPelanggan", idPelanggan);
                                intent.putExtra("idKategori", jenisList.get(position).getId());
                                intent.putExtra("levelHarga", levelHarga);
                                intent.putExtra("counter", jenisList.get(position).getCount());
                                intent.putExtra("perintah", "sales");
                                intent.putExtra("so", so);
                                startActivityForResult(intent, 1);
                            } catch (Exception e) {
                                alert.showAlert(e.toString());
                            }

                        } else {
                            alert.showAlert("Tidak ada barang dalam kategori ini !");
                        }

                    }
                });

            } else {
                alert.showAlert("Tidak ada data ! Sync data terlebih dahulu !");
            }
        } else {
            pelangganCsr = dataBase.selectJenis();
            if (pelangganCsr.getCount() > 0) {
                idList.clear();
                nameList.clear();
                jenisList.clear();
                pelangganCsr.moveToFirst();
                while (pelangganCsr.isAfterLast() == false) {
                    String id = pelangganCsr.getString(0);
                    String Nama = pelangganCsr.getString(1);
                    Cursor countCsr;
                    if (levelHarga.equals("1") || levelHarga.equals("2") || levelHarga.equals("3")  || levelHarga.equals("6")) {
                        countCsr = dataBase.selectKategoriPreOrderCount12(id, idPelanggan);
                    } else {
                        countCsr = dataBase.selectKategoriPreOrderCount3(id, idPelanggan);
                    }
                    countCsr.moveToFirst();
                    String counter = countCsr.getString(0);
                    if (!counter.equals("0")) {
                        countList.add(counter);
                        idList.add(id);
                        nameList.add(Nama + " (" + counter + ")");
                        Jenis jens = new Jenis(id, Nama + " (" + counter + ")", counter);
                        jenisList.add(jens);
                    }
                    pelangganCsr.moveToNext();
                }
                Collections.sort(jenisList, Jenis.StuNameComparator);
                listPelangganLV.setAdapter(new ListPelangganAdapter(context, nameList, jenisList));
                listPelangganLV.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        if (Integer.parseInt(countList.get(position)) > 0) {

                            try {
                                Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                                intent.putExtra("idPelanggan", idPelanggan);
                                intent.putExtra("idKategori", jenisList.get(position).getId());
                                intent.putExtra("levelHarga", levelHarga);
                                intent.putExtra("counter", jenisList.get(position).getCount());
                                intent.putExtra("perintah", "pre");
                                intent.putExtra("so", so);
                                startActivityForResult(intent, 1);
                            } catch (Exception e) {
                                alert.showAlert(e.toString());
                            }

                        } else {
                            alert.showAlert("Tidak ada barang dalam kategori ini !");
                        }

                    }
                });

            } else {
                alert.showAlert("Tidak ada data ! Sync data terlebih dahulu !");
            }
        }


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(ListKategoriActivity.this);
            builder.setMessage("Apakah Anda Benar-Benar Ingin Keluar?")
                    .setCancelable(false)
                    .setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    Intent intent = new Intent(ListKategoriActivity.this, DashboardCustomerActivity.class);
                                    intent.putExtra("idPelanggan", idPelanggan.toString());
                                    intent.putExtra("levelHarga", levelHarga.toString());
                                    intent.putExtra("alamat", alamat.toString());
                                    intent.putExtra("nama", nama.toString());
                                    intent.putExtra("aktif", aktif);
                                    startActivity(intent);
                                    finish();
//					 ListKategoriActivity.this.finish();
                                }
                            })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();

                        }
                    }).show();
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
    }
}
