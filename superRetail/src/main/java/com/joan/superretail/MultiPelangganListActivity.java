package com.joan.superretail;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.model.ListPelanggan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MultiPelangganListActivity extends Activity {

    ArrayList<ListPelanggan> pelangganList = new ArrayList<ListPelanggan>();
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    TextView titleTV, lanjutTV;
    MyCustomAdapter dataAdapter = null;
    String intentType;
    LinearLayout dateLL;
    TextView checkTV;
    TextView fromDateTV, toDateTV;
    boolean isFromDate = false;
    boolean isToDate = false;
    boolean isCheck = false;
    boolean isSingle = false;
    int type;
    private int year;
    private int month;
    private int day;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String dayStr = Integer.toString(selectedDay);
            String monthStr = Integer.toString(selectedMonth + 1);
            if (selectedDay < 10) {
                dayStr = "0" + dayStr;
            }
            if (selectedMonth < 10) {
                monthStr = "0" + monthStr;
            }
//            year  = selectedYear;
//            month = selectedMonth;
//            day   = selectedDay;
            String dateStr = Integer.toString(selectedYear) + "-" + monthStr + "-" + dayStr;
            if (type == 1) {
                fromDateTV.setText(setDate(dateStr));
                isFromDate = true;
            } else if (type == 2) {
                toDateTV.setText(setDate(dateStr));
                isToDate = true;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar_reminder);
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        Calendar c = Calendar.getInstance();
        dateLL = (LinearLayout) findViewById(R.id.dateLL);
        fromDateTV = (TextView) findViewById(R.id.fromDateTV);
        toDateTV = (TextView) findViewById(R.id.toDateTV);
        checkTV = (TextView) findViewById(R.id.checkTV);
        Bundle extras = getIntent().getExtras();
        intentType = extras.getString("intentType");
        if (intentType.equals("1") || intentType.equals("3") || intentType.equals("2") || intentType.equals("5")) {
            dateLL.setVisibility(LinearLayout.GONE);
        }
        fromDateTV.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                type = 1;
                Calendar c = Calendar.getInstance();
                day = c.get(Calendar.DATE);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                showDialog(12);
            }
        });
        toDateTV.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                type = 2;
                Calendar c = Calendar.getInstance();
                day = c.get(Calendar.DATE);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                showDialog(11);
            }
        });
        checkTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isCheck == true) {
                    for (int i = 0; i < pelangganList.size(); i++) {
                        ListPelanggan lists = pelangganList.get(i);
                        lists.setSelected(false);
                    }
                    checkTV.setText("Check All");
                    isCheck = false;

                } else {
                    for (int i = 0; i < pelangganList.size(); i++) {
                        ListPelanggan lists = pelangganList.get(i);
                        lists.setSelected(true);
                    }

                    checkTV.setText("Uncheck All");
                    isCheck = true;

                }

                dataAdapter.notifyDataSetChanged();

            }
        });
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        titleTV.setTypeface(medium);
        lanjutTV = (TextView) findViewById(R.id.lanjutTV);
        lanjutTV.setTypeface(bold);
        Cursor pelangganCsr = dataBase.selectIdNamaPelanggan();
        if (pelangganCsr.getCount() > 0) {
            pelangganList.clear();
            pelangganCsr.moveToFirst();
            while (pelangganCsr.isAfterLast() == false) {
                String id = pelangganCsr.getString(0);
                String name = pelangganCsr.getString(1);
                ListPelanggan pelanggan = new ListPelanggan(id, name, false);
                pelangganList.add(pelanggan);
                pelangganCsr.moveToNext();
            }
            dataAdapter = new MyCustomAdapter(this,
                    R.layout.pelanggan_checkbox, pelangganList);
            ListView listView = (ListView) findViewById(R.id.listKategoriLV);
            // Assign adapter to ListView
            listView.setAdapter(dataAdapter);


            listView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {


                    ListPelanggan country = pelangganList.get(position);
                    if (country.getSelected() == false) {
                        country.setSelected(true);
                    } else {
                        country.setSelected(false);
                    }
                    dataAdapter.notifyDataSetChanged();
                }
            });
            lanjutTV.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int counTemp = 0;
                    boolean isChecked = false;
                    String query = "";
                    for (int i = 0; i < pelangganList.size(); i++) {
                        ListPelanggan country = pelangganList.get(i);
                        if (country.getSelected() == true) {

                            if (isChecked == false) {
                                query = query + "'" + country.getId() + "'";
                            } else {
                                query = query + ",'" + country.getId() + "'";
                            }
                            isChecked = true;
                            counTemp++;
                        }

                    }
                    if (isChecked == true) {
                        if (counTemp == 1) {
                            isSingle = true;
                        }
                        if (intentType.equals("1")) {
                            Intent intent = new Intent(getApplicationContext(), ArReminderDetailActivity.class);
                            intent.putExtra("query", query);
                            startActivity(intent);

                        } else if (intentType.equals("5")) {
                            Intent intent = new Intent(getApplicationContext(), CnAvailableActivity.class);
                            intent.putExtra("query", query);
                            startActivity(intent);

                        } else {
                            if (intentType.equals("2")) {

//										if(isFromDate==true&&isToDate==true)
//										{
//											String fromDateStr = setDateReverse(fromDateTV.getText().toString());
//											String toDateStr = setDateReverse(toDateTV.getText().toString());
                                Intent intent = new Intent(getApplicationContext(), SOReportActivity.class);
                                intent.putExtra("query", query);
                                intent.putExtra("fromDate", "");
                                intent.putExtra("toDate", "");
                                startActivity(intent);
//										}

                            } else if (intentType.equals("4")) {

                                if (isFromDate == true && isToDate == true) {
                                    String fromDateStr = setDateReverse(fromDateTV.getText().toString());
                                    String toDateStr = setDateReverse(toDateTV.getText().toString());
//											Toast.makeText(getApplicationContext(),fromDateStr,Toast.LENGTH_LONG).show();
//											Toast.makeText(getApplicationContext(),toDateStr,Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), SalesReportActivity.class);
                                    intent.putExtra("query", query);
                                    intent.putExtra("fromDate", fromDateStr);
                                    intent.putExtra("toDate", toDateStr);
                                    if (isSingle == true) {
                                        intent.putExtra("isSingle", "1");
                                    } else {
                                        intent.putExtra("isSingle", "1");
                                    }

                                    startActivity(intent);
                                }

                            } else if (intentType.equals("3")) {
                                Intent intent = new Intent(getApplicationContext(), DeliveryActivity.class);
                                intent.putExtra("query", query);
                                intent.putExtra("fromDate", "");
                                intent.putExtra("toDate", "");
                                startActivity(intent);
                            }
                        }


                    }

                }
            });
        } else {
            alert.showAlert("Tidak ada data. Mohon sync data terlebih dahulu");
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();
        int tempday = c.get(Calendar.DATE);
        int tempmonth = c.get(Calendar.MONTH);
        int tempyear = c.get(Calendar.YEAR);

        return new DatePickerDialog(this, pickerListener, tempyear, tempmonth, tempday);
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }

    private String setDateReverse(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ") && !dateString.equals("") && !dateString.equals(null)) {
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            result = fmtOut.format(date);
        }

        return result;
    }

    private class MyCustomAdapter extends ArrayAdapter<ListPelanggan> {

        private ArrayList<ListPelanggan> countryList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<ListPelanggan> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<ListPelanggan>();
            this.countryList.addAll(countryList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            final ListPelanggan country = countryList.get(position);
//		   if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.pelanggan_checkbox, null);
            holder = new ViewHolder();
            holder.code = (TextView) convertView.findViewById(R.id.code);
            holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
            holder.name.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    country.setSelected(isChecked);
                }
            });
            convertView.setTag(holder);
//		   }
//		   else {
//		    holder = (ViewHolder) convertView.getTag();
//		   }
//

            holder.code.setText(country.getName());
            holder.name.setChecked(country.getSelected());
            holder.name.setText(null);
            holder.code.setTypeface(medium);
            return convertView;

        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

    }
}
