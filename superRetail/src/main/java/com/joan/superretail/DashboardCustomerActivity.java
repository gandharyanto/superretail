package com.joan.superretail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joan.superretail.constant.Constant;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.IPManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.service.DataServices;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DashboardCustomerActivity extends Activity {
    String idPelanggan;
    String levelHarga;
    String alamatPelanggan;
    String namaPelanggan;
    String kontakPelanggan;
    String aktifPelanggan;
    TextView namaPelangganTV, alamatPelangganTV, currentSalesTV, arOutstandingTV, totalArTV, statusTV, kontakTV;
    Button btnSoStatus, btnSalesOrder, btnSalesOrder2, btnPreOrder, btnReturnStatus, btnDeliveryStatus, btnArDetail, btnSalesReport, btnCnAvailable, btnArStatus, btnSync;
    LinearLayout llSO1, llSO2;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    IPManager ipManager;
    SimpleDateFormat dateFormat;
    boolean isFromDate = false;
    boolean isToDate = false;
    int type;
    double jmlSales, jmlArOutstanding, jmlArReminder;
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;
    boolean isSync = false;
    String fromDate, toDate;
    String dateFrom = "";
    String dateTo = "";
    GPSTracker gpsTracker;
    double myLatitude;
    double myLongitude;
    private int year;
    private int month;
    private int day;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String dayStr = Integer.toString(selectedDay);
            String monthStr = Integer.toString(selectedMonth + 1);
            if (selectedDay < 10) {
                dayStr = "0" + dayStr;
            }
            if (selectedMonth < 10) {
                monthStr = "0" + monthStr;
            }
//            year  = selectedYear;
//            month = selectedMonth;
//            day   = selectedDay;

            if (type == 3) {
//                Toast.makeText(getApplicationContext(),"DONE",Toast.LENGTH_LONG).show();
                String dateStr = Integer.toString(selectedYear) + "-" + monthStr + "-" + dayStr;
                toDate = (setDate(dateStr));
//                dialog.show();
//                getSalesReport(idPelanggan);
                String fromDateStr = setDateReverse(fromDate.toString());
                String toDateStr = setDateReverse(toDate.toString());
                Intent intent = new Intent(getApplicationContext(), SalesReportActivity.class);
//                Toast.makeText(getApplicationContext(),fromDateStr,Toast.LENGTH_LONG).show();
                intent.putExtra("query", "'" + idPelanggan + "'");
                intent.putExtra("fromDate", fromDateStr);
                intent.putExtra("toDate", toDateStr);
                intent.putExtra("isSingle", "1");
                startActivity(intent);
                type = 4;
            }
            if (type == 2) {
                isToDate = true;
                type = 3;
            }
            if (type == 1) {
                isFromDate = true;
                String dateStr = Integer.toString(selectedYear) + "-" + monthStr + "-" + dayStr;
                fromDate = (setDate(dateStr));
//                Toast.makeText(getApplicationContext(),fromDate,Toast.LENGTH_LONG).show();
                type = 2;
                Calendar c = Calendar.getInstance();
                day = c.get(Calendar.DATE);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                showDialog(11);
            }

        }
    };

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dushboard_customer);
        namaPelangganTV = (TextView) findViewById(R.id.tvNama);
        alamatPelangganTV = (TextView) findViewById(R.id.tvAlamat);
        currentSalesTV = (TextView) findViewById(R.id.tvCurrentSales);
        arOutstandingTV = (TextView) findViewById(R.id.tvArOutstanding);
        totalArTV = (TextView) findViewById(R.id.tvTotalAr);
        statusTV = (TextView) findViewById(R.id.tvStatus);
        kontakTV = (TextView) findViewById(R.id.tvKontak);

        btnSoStatus = (Button) findViewById(R.id.btnSoStatus);
        btnSalesOrder = (Button) findViewById(R.id.btnSalesOrder);
        btnSalesOrder2 = (Button) findViewById(R.id.btnSalesOrder2);
        btnPreOrder = (Button) findViewById(R.id.btnPreOrder);
        btnReturnStatus = (Button) findViewById(R.id.btnReturnStatus);
        btnDeliveryStatus = (Button) findViewById(R.id.btnDeliveryStatus);
        btnArDetail = (Button) findViewById(R.id.btnArDetail);
        btnSalesReport = (Button) findViewById(R.id.btnSalesReport);
        btnCnAvailable = (Button) findViewById(R.id.btnCnAvailable);
        btnArStatus = (Button) findViewById(R.id.btnArStatus);
        btnSync = (Button) findViewById(R.id.btnSync);
        llSO1 = findViewById(R.id.llSO1);
        llSO2 = findViewById(R.id.llSO2);
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        dataBase = DataBaseManager.instance();
        context = this;
        ipManager = new IPManager();
        if (Constant.SERVICE) {
            context.startService(new Intent(context, DataServices.class));
        }
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Bundle extras = getIntent().getExtras();
        idPelanggan = extras.getString("idPelanggan");
        Cursor csr = dataBase.selectIdNamaPelangganById(idPelanggan);
        csr.moveToFirst();
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("levelHarga")) {
            levelHarga = extras.getString("levelHarga");
        } else {
            levelHarga = csr.getString(1);
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("alamat")) {
            alamatPelanggan = extras.getString("alamat");
        } else {
            alamatPelanggan = csr.getString(2);
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("kontak")) {
            kontakPelanggan = extras.getString("kontak");
        } else {
            String Kontak = csr.getString(3);
            String Kontak2 = csr.getString(4);
            String isiKontak = "";
            if ((Kontak.equals("")) && (Kontak2.equals(""))) {
                isiKontak = "-";
            } else if ((!Kontak.equals("")) && (!Kontak2.equals(""))) {
                isiKontak = Kontak + " / " + Kontak2;
            } else if ((!Kontak.equals("")) && (Kontak2.equals(""))) {
                isiKontak = Kontak;
            } else if ((Kontak.equals("")) && (!Kontak2.equals(""))) {
                isiKontak = Kontak2;
            }
            kontakPelanggan = isiKontak;
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("nama")) {
            namaPelanggan = extras.getString("nama");
        } else {
            namaPelanggan = csr.getString(0);
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("aktif")) {
            aktifPelanggan = extras.getString("aktif");
        } else {
            aktifPelanggan = csr.getString(5);
        }
        if (kontakPelanggan != null) {
            if (!kontakPelanggan.equals("")) {
                kontakPelanggan = kontakPelanggan.replace(" / ", "\n");
            }
        }

        if (levelHarga.equals("1") || levelHarga.equals("2") || levelHarga.equals("6")) {
            llSO2.setVisibility(View.GONE);
        }

        alamatPelangganTV.setText(alamatPelanggan);
        namaPelangganTV.setText(namaPelanggan);
        kontakTV.setText(kontakPelanggan);
        if ((aktifPelanggan.equals("0")) || (aktifPelanggan.equals("null")) || (aktifPelanggan == null)) {
            statusTV.setText("OPEN");
            statusTV.setTextColor(Color.parseColor("#4CAF50"));
        } else {
            statusTV.setText("HOLD");
            statusTV.setTextColor(Color.RED);
        }
        showResultSync();
        btnSoStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getSOReport(idPelanggan);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), SOReportActivity.class);
                                intent.putExtra("query", idPelanggan);
                                intent.putExtra("fromDate", "");
                                intent.putExtra("toDate", "");
                                startActivity(intent);
                            }
                        }).show();

            }
        });
        btnSalesOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBase.deleteJnsOrder();
                dataBase.deleteOrderTemp();
                ContentValues values = new ContentValues();
                values.put("jns_order", "Sales");
                dataBase.insert("jns_order", values);
                Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                intent.putExtra("idPelanggan", idPelanggan);
                intent.putExtra("levelHarga", levelHarga);
                intent.putExtra("alamat", alamatPelanggan);
                intent.putExtra("nama", namaPelanggan);
                intent.putExtra("aktif", aktifPelanggan);
                intent.putExtra("perintah", "sales");
                intent.putExtra("so", "so1");
                startActivityForResult(intent, 1);
                finish();
            }
        });
        btnSalesOrder2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBase.deleteJnsOrder();
                dataBase.deleteOrderTemp();
                ContentValues values = new ContentValues();
                values.put("jns_order", "Sales");
                dataBase.insert("jns_order", values);
                Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                intent.putExtra("idPelanggan", idPelanggan);
                intent.putExtra("levelHarga", levelHarga);
                intent.putExtra("alamat", alamatPelanggan);
                intent.putExtra("nama", namaPelanggan);
                intent.putExtra("aktif", aktifPelanggan);
                intent.putExtra("perintah", "sales");
                intent.putExtra("so", "so2");
                startActivityForResult(intent, 1);
                finish();
            }
        });

        gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            myLatitude = gpsTracker.latitude;
            myLongitude = gpsTracker.longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }

        btnPreOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*dataBase.deleteJnsOrder();
                dataBase.deleteOrderTemp();
                ContentValues values = new ContentValues();
                values.put("jns_order", "Pre");
                dataBase.insert("jns_order", values);
                Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                intent.putExtra("idPelanggan", idPelanggan);
                intent.putExtra("levelHarga", levelHarga);
                intent.putExtra("alamat", alamatPelanggan);
                intent.putExtra("nama", namaPelanggan);
                intent.putExtra("aktif", aktifPelanggan);
                intent.putExtra("perintah", "pre");
                intent.putExtra("so", "pre");
                startActivityForResult(intent, 1);
                finish();*/

                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                                            dialog.show();
                                            Cursor csr = dataBase.selectAccount();
                                            csr.moveToFirst();
                                            String idSalesman = csr.getString(3);
                                            sendLog(idSalesman, String.valueOf(myLatitude), String.valueOf(myLongitude), "Sync Data");
                                            getBarang();
                                        } else {
//											Toast.makeText(getApplicationContext(),"DISABLE",Toast.LENGTH_LONG).show();
                                            gpsTracker.showSettingsAlert();
                                        }

                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
//                                dialog.cancel();
                                dataBase.deleteJnsOrder();
                                dataBase.deleteOrderTemp();
                                ContentValues values = new ContentValues();
                                values.put("jns_order", "Pre");
                                dataBase.insert("jns_order", values);
                                Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                                intent.putExtra("idPelanggan", idPelanggan);
                                intent.putExtra("levelHarga", levelHarga);
                                intent.putExtra("alamat", alamatPelanggan);
                                intent.putExtra("nama", namaPelanggan);
                                intent.putExtra("aktif", aktifPelanggan);
                                intent.putExtra("perintah", "pre");
                                intent.putExtra("so", "pre");
                                startActivityForResult(intent, 1);
                                finish();
                            }
                        }).show();
            }
        });

        btnReturnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getReturnReport(idPelanggan);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
//                                dialog.cancel();
                                Intent intent = new Intent(getApplicationContext(), ReturnReportActivity.class);
                                intent.putExtra("id", idPelanggan);
                                intent.putExtra("fromDate", "");
                                intent.putExtra("toDate", "");
                                startActivity(intent);
                            }
                        }).show();
            }
        });

        btnArDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSync = false;
                Intent intent = null;
                intent = new Intent(getApplicationContext(), ARDetailActivity.class);
                intent.putExtra("id", idPelanggan);
                intent.putExtra("fromDate", "");
                intent.putExtra("toDate", "");
                startActivity(intent);
//                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
//                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
//                        .setCancelable(false)
//                        .setPositiveButton("Ya",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dg,
//                                                        int id) {
//                                        dialog.show();
//                                        getArDetail(idPelanggan);
//                                    }
//                                })
//                        .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
////                                dialog.cancel();
//                                Intent intent = null;
//                                intent = new Intent(getApplicationContext(), ARDetailActivity.class);
//                                intent.putExtra("id", idPelanggan);
//                                intent.putExtra("fromDate", "");
//                                intent.putExtra("toDate", "");
//                                startActivity(intent);
//
//                            }
//                        }).show();

            }
        });

        btnSalesReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSync = false;
                type = 1;
                Calendar c = Calendar.getInstance();
                day = c.get(Calendar.DATE);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                showDialog(12);
//                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
//                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
//                        .setCancelable(false)
//                        .setPositiveButton("Ya",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dg,
//                                                        int id) {
//                                        dialog.show();
//                                        getSalesReport(idPelanggan);
//                                    }
//                                })
//                        .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dg,
//                                                int id) {
////                                dialog.cancel();
//                                type = 1;
//                                Calendar c = Calendar.getInstance();
//                                day = c.get(Calendar.DATE);
//                                month = c.get(Calendar.MONTH);
//                                year = c.get(Calendar.YEAR);
//                                showDialog(12);
//
//                            }
//                        }).show();
            }
        });

        btnCnAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getCnAvailable(idPelanggan);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), CnAvailableActivity.class);
                                intent.putExtra("query", idPelanggan);
                                startActivity(intent);
                            }
                        }).show();

            }
        });

        btnArStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSync = false;
//                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
//                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
//                        .setCancelable(false)
//                        .setPositiveButton("Ya",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dg,
//                                                        int id) {
//                                        dialog.show();
//                                        getArReminder(idPelanggan);
//                                    }
//                                })
//                        .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int id) {
////                                dialog.cancel();
//                                Intent intent = new Intent(getApplicationContext(), ArReminderDetailActivity.class);
//                                intent.putExtra("query", idPelanggan);
//                                startActivity(intent);
//                            }
//                        }).show();
                Intent intent = new Intent(getApplicationContext(), ArReminderDetailActivity.class);
                intent.putExtra("query", idPelanggan);
                startActivity(intent);
            }
        });
        btnDeliveryStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
                builder.setMessage("Apakah Anda Ingin Melakukan Sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getDeliveryReport(idPelanggan);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
//                                dialog.cancel();
                                Intent intent = new Intent(getApplicationContext(), DeliveryActivity.class);
                                intent.putExtra("query", idPelanggan);
                                intent.putExtra("fromDate", "");
                                intent.putExtra("toDate", "");
                                startActivity(intent);
                            }
                        }).show();
            }
        });

        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                day = c.get(Calendar.DATE);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                String strDay, strMonth = "";
                if (month + 1 < 10) {
                    strMonth = "0" + String.valueOf(month + 1);
                } else {
                    strMonth = String.valueOf(month + 1);
                }
                if (day < 10) {
                    strDay = "0" + String.valueOf(day);
                } else {
                    strDay = String.valueOf(day);
                }
                dateFrom = String.valueOf(year) + "-" + strMonth + "-01";
                dateTo = String.valueOf(year) + "-" + strMonth + "-" + strDay;


//                Toast.makeText(getApplicationContext(),dateFrom + "$$" + dateTo,Toast.LENGTH_LONG).show();
                isSync = true;
                dialog.show();
                getSalesReport(idPelanggan);
            }
        });


    }

    public void getBarang() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
//                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.NEW_SYNC_BARANG);
                        HttpPost post = new HttpPost(Constant.NEW_BASE_URL + Constant.NEW_SYNC_BARANGV2);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "barang null", Toast.LENGTH_SHORT).show();
                                        //getDiscount();
                                        dataBase.deleteJnsOrder();
                                        dataBase.deleteOrderTemp();
                                        ContentValues values = new ContentValues();
                                        values.put("jns_order", "Pre");
                                        dataBase.insert("jns_order", values);
                                        Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                                        intent.putExtra("idPelanggan", idPelanggan);
                                        intent.putExtra("levelHarga", levelHarga);
                                        intent.putExtra("alamat", alamatPelanggan);
                                        intent.putExtra("nama", namaPelanggan);
                                        intent.putExtra("aktif", aktifPelanggan);
                                        intent.putExtra("perintah", "pre");
                                        intent.putExtra("so", "pre");
                                        startActivityForResult(intent, 1);
                                        finish();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteBarang1("1");
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String id = jsonArray.getJSONObject(i).getString("id");
                                                String idKategori = jsonArray.getJSONObject(i).getString("idKategori");
                                                String idJenis = jsonArray.getJSONObject(i).getString("idJenis");
                                                String idMerk = jsonArray.getJSONObject(i).getString("idMerk");
                                                String idGrup = jsonArray.getJSONObject(i).getString("idGrup");
                                                String idPemasok = jsonArray.getJSONObject(i).getString("idPemasok");
                                                String Kode = jsonArray.getJSONObject(i).getString("Kode");
                                                String Nama = jsonArray.getJSONObject(i).getString("Nama");
                                                String Kategori = jsonArray.getJSONObject(i).getString("Kategori");
                                                String Jenis = jsonArray.getJSONObject(i).getString("Jenis");
                                                String Merk = jsonArray.getJSONObject(i).getString("Merk");
                                                String Grup = jsonArray.getJSONObject(i).getString("Grup");
                                                String Pemasok = jsonArray.getJSONObject(i).getString("Pemasok");
                                                String Barcode = jsonArray.getJSONObject(i).getString("Barcode");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String SatuanSedang = jsonArray.getJSONObject(i).getString("SatuanSedang");
                                                String SatuanBesar = jsonArray.getJSONObject(i).getString("SatuanBesar");
                                                double IsiSedang = jsonArray.getJSONObject(i).getDouble("IsiSedang");
                                                double IsiBesar = jsonArray.getJSONObject(i).getDouble("IsiBesar");
                                                String HargaBeli = jsonArray.getJSONObject(i).getString("HargaBeli");
                                                String HargaBeliGross = jsonArray.getJSONObject(i).getString("HargaBeliGross");
                                                String HargaJual1 = jsonArray.getJSONObject(i).getString("HargaJual1");
                                                String HargaJual2 = jsonArray.getJSONObject(i).getString("HargaJual2");
                                                String HargaJual3 = jsonArray.getJSONObject(i).getString("HargaJual3");
                                                String HargaJual4 = jsonArray.getJSONObject(i).getString("HargaJual4");
                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String DateModified = jsonArray.getJSONObject(i).getString("DateModified");
                                                String MinStok = jsonArray.getJSONObject(i).getString("MinStok");
                                                String MaxStok = jsonArray.getJSONObject(i).getString("MaxStok");
                                                double Stok = jsonArray.getJSONObject(i).getDouble("Stok");
                                                String Stok2 = jsonArray.getJSONObject(i).getString("Stok2");
                                                String Custom1 = "";
                                                String Custom2 = "";
                                                String Custom3 = "";
//												String Custom1 = jsonArray.getJSONObject(i).getString("Custom1");
//												String Custom2 = jsonArray.getJSONObject(i).getString("Custom2");
//												String Custom3 = jsonArray.getJSONObject(i).getString("Custom3");
                                                String D1 = jsonArray.getJSONObject(i).getString("D1");
                                                String D2 = jsonArray.getJSONObject(i).getString("D2");
                                                String D3 = jsonArray.getJSONObject(i).getString("D3");
                                                String D4 = jsonArray.getJSONObject(i).getString("D4");
                                                String D5 = jsonArray.getJSONObject(i).getString("D5");
                                                String Tipe = jsonArray.getJSONObject(i).getString("Tipe");
                                                String Berat = jsonArray.getJSONObject(i).getString("Berat");
                                                String UniqueKey = jsonArray.getJSONObject(i).getString("UniqueKey");

//												IsiSedang = IsiSedang.replace(".0000", "");
//												IsiBesar = IsiBesar.replace(".0000", "");
//												Stok = Stok.replace(".0000", "");

                                                ContentValues values = new ContentValues();
                                                values.put("id", id);
                                                values.put("idKategori", idKategori);
                                                values.put("idJenis", idJenis);
                                                values.put("idMerk", idMerk);
                                                values.put("idGrup", idGrup);
                                                values.put("idPemasok", idPemasok);
                                                values.put("Kode", Kode);
                                                values.put("Nama", Nama);
                                                values.put("Kategori", Kategori);
                                                values.put("Jenis", Jenis);
                                                values.put("Merk", Merk);
                                                values.put("Grup", Grup);
                                                values.put("Pemasok", Pemasok);
                                                values.put("Barcode", Barcode);
                                                values.put("Satuan", Satuan);
                                                values.put("SatuanSedang", SatuanSedang);
                                                values.put("SatuanBesar", SatuanBesar);
                                                values.put("IsiSedang", (int) IsiSedang);
                                                values.put("IsiBesar", (int) IsiBesar);
                                                values.put("Stok", (int) Stok);
                                                values.put("HargaBeli", HargaBeli);
                                                values.put("HargaBeliGross", HargaBeliGross);
                                                values.put("HargaJual1", HargaJual1);
                                                values.put("HargaJual2", HargaJual2);
                                                values.put("HargaJual3", HargaJual3);
                                                values.put("HargaJual4", HargaJual4);
                                                values.put("Keterangan", Keterangan);
                                                values.put("DateModified", DateModified);
                                                values.put("MinStok", MinStok);
                                                values.put("MaxStok", MaxStok);

                                                values.put("Stok2", Stok2);
                                                values.put("Custom1", Custom1);
                                                values.put("Custom2", Custom2);
                                                values.put("Custom3", Custom3);
                                                values.put("D1", D1);
                                                values.put("D2", D2);
                                                values.put("D3", D3);
                                                values.put("D4", D4);
                                                values.put("D5", D5);
                                                values.put("Tipe", Tipe);
                                                values.put("Berat", Berat);
                                                values.put("UniqueKey", UniqueKey);
                                                values.put("del", "1");
                                                dataBase.insert("barang", values);
                                            }

                                            Toast.makeText(getApplicationContext(), "Sync barang success", Toast.LENGTH_SHORT).show();
                                            //getDiscount();

                                            dataBase.deleteJnsOrder();
                                            dataBase.deleteOrderTemp();
                                            ContentValues values = new ContentValues();
                                            values.put("jns_order", "Pre");
                                            dataBase.insert("jns_order", values);
                                            Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
                                            intent.putExtra("idPelanggan", idPelanggan);
                                            intent.putExtra("levelHarga", levelHarga);
                                            intent.putExtra("alamat", alamatPelanggan);
                                            intent.putExtra("nama", namaPelanggan);
                                            intent.putExtra("aktif", aktifPelanggan);
                                            intent.putExtra("perintah", "pre");
                                            intent.putExtra("so", "pre");
                                            startActivityForResult(intent, 1);
                                            finish();
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_barang : " + e.toString());
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_barang : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("sync_barang : " + e.toString());
                }
            });
        }
    }

    public void sendLog(final String idSalesman, final String Latitude, final String Longitude, final String Remarks) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SEND_LOG);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        json.put("Latitude", Latitude);
                        json.put("Longitude", Longitude);
                        json.put("Remarks", Remarks);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        response = client.execute(post);
                        StringBuilder sb = new StringBuilder();
                        InputStream in = response.getEntity().getContent();
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        String arrResult = sb.toString();
//						Toast.makeText(getApplicationContext(),arrResult,Toast.LENGTH_LONG).show();
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                alert.showAlert("sendlog : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    alert.showAlert(e.toString());
                }
            });
        }
    }

    public void showResultSync() {
        Cursor salesCount = dataBase.selectSyncTemp();
        if (salesCount.getCount() > 0) {
            salesCount.moveToFirst();
            String a = salesCount.getString(0).replace(",", "");
            String b = salesCount.getString(1).replace(",", "");
            String c = salesCount.getString(2).replace(",", "");
            int d = (int) Math.round(Double.parseDouble(a));
            int e = (int) Math.round(Double.parseDouble(b));
            int f = (int) Math.round(Double.parseDouble(c));
            currentSalesTV.setText("Current Sales : Rp." + formatter.format((double) d));
            arOutstandingTV.setText("AR Outstanding : Rp." + formatter.format((double) e));
            totalArTV.setText("Total AR : Rp." + formatter.format((double) f));
        }
    }

    public void getSOReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SO_REPORT_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "so report pelanggan null", Toast.LENGTH_LONG).show();
//                                        getSOReportDetail(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSOReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {

                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
//												String TanggalTerima = jsonArray.getJSONObject(i).getJSONObject("TanggalTerima").getString("date");
//												String TanggalFaktur = jsonArray.getJSONObject(i).getJSONObject("TanggalFaktur").getString("date");
//												String JatuhTempo = jsonArray.getJSONObject(i).getJSONObject("JatuhTempo").getString("date");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");

                                                Tanggal = getDate(Tanggal);
//												JatuhTempo = getDate(JatuhTempo);
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
//												values.put("TanggalFaktur", "");
//												values.put("JatuhTempo", JatuhTempo);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("Total", Total);
                                                dataBase.insert("so_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync so report pelanggan success", Toast.LENGTH_SHORT).show();
                                            getSOReportDetail(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("SO Report Pelanggan : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("SO Report Pelanggan : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("SO Report Pelanggan : " + e.toString());
                }
            });
        }
    }

    public void getSOReportDetail(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SO_REPORT_DETAIL_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "so report detail pelangggan null", Toast.LENGTH_LONG).show();
                                        getSalesReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSOReportDetail();
                                            for (int i = 0; i < jsonArray.length(); i++) {

                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String KodeBarang = jsonArray.getJSONObject(i).getString("KodeBarang");
                                                String NamaBarang = jsonArray.getJSONObject(i).getString("NamaBarang");
                                                String Quantity = jsonArray.getJSONObject(i).getString("Quantity");
                                                String QuantityKirim = jsonArray.getJSONObject(i).getString("QuantityKirim");
                                                String QuantitySisa = jsonArray.getJSONObject(i).getString("QuantitySisa");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String Isi = jsonArray.getJSONObject(i).getString("Isi");
                                                String HargaJual = jsonArray.getJSONObject(i).getString("HargaJual");
                                                String Diskon1 = jsonArray.getJSONObject(i).getString("Diskon1");
                                                String Diskon2 = jsonArray.getJSONObject(i).getString("Diskon2");
                                                String Subtotal = jsonArray.getJSONObject(i).getString("Subtotal");
                                                String Status = jsonArray.getJSONObject(i).getString("Status");
                                                if (Status.equals("")) {
                                                    Status = "PENDING";
                                                }
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("idBarang", idBarang);
                                                values.put("KodeBarang", KodeBarang);
                                                values.put("NamaBarang", NamaBarang);
                                                values.put("Quantity", Quantity);
                                                values.put("QuantityKirim", QuantityKirim);
                                                values.put("QuantitySisa", QuantitySisa);
                                                values.put("Satuan", Satuan);
                                                values.put("Isi", Isi);
                                                values.put("HargaJual", HargaJual);
                                                values.put("Diskon1", Diskon1);
                                                values.put("Diskon2", Diskon2);
                                                values.put("Subtotal", Subtotal);
                                                values.put("Status", Status);
                                                dataBase.insert("so_report_detail", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync so report detail pelanggan success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), SOReportActivity.class);
                                            intent.putExtra("query", idPelanggan);
                                            intent.putExtra("fromDate", "");
                                            intent.putExtra("toDate", "");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("SO Report Detail Pelanggan : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("SO Report Detail Pelanggan : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("SO Report Detail Pelanggan : " + e.toString());
                }
            });
        }
    }

    public void getArDetail(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_ARDETAIL_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            final String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        if (isSync == false) {
                                            dialog.dismiss();
                                            alert.showAlert("AR Detail null result");
                                        } else {
                                            dialog.dismiss();
                                            alert.showAlert("AR Detail null result");
                                            totalArTV.setText("Total AR : Rp.0");
                                        }
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteARDetail();
                                            jmlArReminder = 0;
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal;
                                                try {
                                                    Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                    Tanggal = getDate(Tanggal);
                                                } catch (Exception e) {
                                                    Tanggal = "-";
                                                }

                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String Debit = jsonArray.getJSONObject(i).getString("Debit");
                                                String Kredit = jsonArray.getJSONObject(i).getString("Kredit");
                                                String Saldo = jsonArray.getJSONObject(i).getString("Saldo");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                int a = (int) Math.round(Double.parseDouble(Debit));
                                                int b;
                                                if ((Kredit != null) && (!Kredit.equals("null"))) {
                                                    b = (int) Math.round(Double.parseDouble(Kredit));
                                                } else {
                                                    b = 0;
                                                }
                                                int c = (int) Math.round(Double.parseDouble(Saldo));

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Keterangan", Keterangan);
                                                values.put("Debit", String.valueOf(a));
                                                values.put("Kredit", String.valueOf(b));
                                                values.put("Saldo", String.valueOf(c));
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("ar_detail", values);
                                                jmlArReminder = Double.parseDouble(Saldo);
                                            }

                                            Toast.makeText(getApplicationContext(), "AR detail pelanggan success", Toast.LENGTH_SHORT).show();

                                            ContentValues dataValues = new ContentValues();
                                            Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
                                            if (lastSyncCsr.getCount() > 0) {
                                                lastSyncCsr.moveToFirst();
                                                String dataSyncTime = lastSyncCsr.getString(1);
                                                String reportSyncTime = lastSyncCsr.getString(0);
                                                if (dataSyncTime.length() > 0) {
                                                    dataValues.put("picture", dataSyncTime);
                                                } else {
                                                    dataValues.put("picture", "");
                                                }
                                                if (reportSyncTime.length() > 0) {
                                                    dataValues.put("data", reportSyncTime);
                                                } else {
                                                    dataValues.put("data", "");
                                                }

                                            } else {
                                                dataValues.put("picture", "");
                                                dataValues.put("data", "");
                                            }
                                            dataBase.deleteDataSync();
                                            dataBase.insert("data_sync", dataValues);
                                            if (isSync == false) {
                                                dialog.dismiss();
                                                Intent intent = null;
                                                intent = new Intent(getApplicationContext(), ARDetailActivity.class);
                                                intent.putExtra("id", idSalesman);
                                                intent.putExtra("fromDate", "");
                                                intent.putExtra("toDate", "");
                                                startActivity(intent);
                                            } else {
                                                dialog.dismiss();
                                                totalArTV.setText("Total AR : Rp." + formatter.format(jmlArReminder));
//                                                Toast.makeText(getApplicationContext(),formatter.format(jmlSales)+"#"+formatter.format(jmlArOutstanding)+"#"+formatter.format(jmlArReminder),Toast.LENGTH_LONG).show();
                                                ContentValues values = new ContentValues();
                                                values.put("currentsales", formatter.format(jmlSales));
                                                values.put("arout", formatter.format(jmlArOutstanding));
                                                values.put("totalar", formatter.format(jmlArReminder));
                                                dataBase.insert("sync_temp", values);
                                                showResultSync();
                                            }
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("AR Detail : " + e.toString() + ", Result :" + arrResult);
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("AR Detail : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }

            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("AR Detail : " + e.toString());
                }
            });
        }
    }

    public void getReturnReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_RETURN_REPORT_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {
                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "return report null", Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
//                                        getSOReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            dataBase.deleteReturnReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String KodeBarang = jsonArray.getJSONObject(i).getString("KodeBarang");
                                                String NamaBarang = jsonArray.getJSONObject(i).getString("NamaBarang");
                                                String Quantity = jsonArray.getJSONObject(i).getString("Quantity");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String Isi = jsonArray.getJSONObject(i).getString("Isi");
                                                String HargaJual = jsonArray.getJSONObject(i).getString("HargaJual");
                                                String Subtotal = jsonArray.getJSONObject(i).getString("Subtotal");

                                                Tanggal = getDate(Tanggal);

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("idBarang", idBarang);
                                                values.put("KodeBarang", KodeBarang);
                                                values.put("NamaBarang", NamaBarang);
                                                values.put("Quantity", Quantity);
                                                values.put("Satuan", Satuan);
                                                values.put("Isi", Isi);
                                                values.put("HargaJual", HargaJual);
                                                values.put("Subtotal", Subtotal);
                                                dataBase.insert("return_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Return report pelanggan success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), ReturnReportActivity.class);
                                            intent.putExtra("id", idSalesman);
                                            intent.putExtra("fromDate", "");
                                            intent.putExtra("toDate", "");
                                            startActivity(intent);
//                                            getSOReport(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Return Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Return Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Return Report : " + e.toString());
                }
            });
        }
    }

    public void getCnAvailable(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_CN_AVAILABLE_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//                        json.put("idSalesman", idSalesman);
                        json.put("idPelanggan", idSalesman);

                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            final String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//                                        alert.showAlert("CN Available null result");
                                        Toast.makeText(getApplicationContext(), "CN Available null result", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            dataBase.deleteCnAvailable();
                                            String hasil = "";
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal;
                                                try {
                                                    Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                    Tanggal = getDate(Tanggal);
                                                } catch (Exception e) {
                                                    Tanggal = "-";
                                                }

                                                String Tipe = jsonArray.getJSONObject(i).getString("Tipe");
                                                String NomorRetur = jsonArray.getJSONObject(i).getString("NomorRetur");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idSalesman = jsonArray.getJSONObject(i).getString("idSalesman");
                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String Jumlah = jsonArray.getJSONObject(i).getString("Jumlah");
                                                String Pakai = jsonArray.getJSONObject(i).getString("Pakai");
                                                String Sisa = jsonArray.getJSONObject(i).getString("Sisa");
                                                hasil = hasil + Jumlah + "," + Pakai + "," + Sisa + "#";
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Tipe", Tipe);
                                                values.put("NomorRetur", NomorRetur);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idSalesman", idSalesman);
                                                values.put("Keterangan", Keterangan);
                                                values.put("Jumlah", Jumlah);
                                                values.put("Pakai", Pakai);
                                                values.put("Sisa", Sisa);
                                                dataBase.insert("cn_available", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "CN Available pelanggan success", Toast.LENGTH_SHORT).show();
//                                            Toast.makeText(getApplicationContext(), hasil, Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), CnAvailableActivity.class);
                                            intent.putExtra("query", idPelanggan);
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("CN Available : " + e.toString() + ", Result :" + arrResult);
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("CN Available : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }

            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("CN Available : " + e.toString());
                }
            });
        }
    }

    public void getSalesReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SALES_REPORT_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "sales report null", Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                        if (isSync == false) {
                                            Toast.makeText(getApplicationContext(), "sales report null", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "sales report null", Toast.LENGTH_LONG).show();
                                            currentSalesTV.setText("Current Sales : Rp.0");
                                            getArReminder(idPelanggan);
                                        }
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSalesReport();
                                            String str = "";
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String Diskon = jsonArray.getJSONObject(i).getString("Diskon");
                                                String Biaya = jsonArray.getJSONObject(i).getString("Biaya");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                Tanggal = getDate(Tanggal);
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("Diskon", Diskon);
                                                values.put("Biaya", Biaya);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("sales_report", values);
//                                                str = str + Total + "#";
                                            }
//                                            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                                            Toast.makeText(getApplicationContext(), "Sales report pelanggan success", Toast.LENGTH_SHORT).show();
                                            getSalesReportDetail(idPelanggan);
//                                            dialog.dismiss();
//                                            if(isFromDate==true&&isToDate==true)
//                                            {
//                                                String fromDateStr = setDateReverse(fromDate.toString());
//                                                String toDateStr = setDateReverse(toDate.toString());
//                                                Intent intent = new Intent(getApplicationContext(), SalesReportActivity.class);
//                                                intent.putExtra("query", "'"+idPelanggan+"'");
//                                                intent.putExtra("fromDate", fromDateStr);
//                                                intent.putExtra("toDate", toDateStr);
//                                                intent.putExtra("isSingle", "1");
//                                                startActivity(intent);
//                                            }
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Sales Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Sales Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Sales Report : " + e.toString());
                }
            });
        }
    }

    public void getSalesReportDetail(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SALES_REPORT_DETAIL_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "sales report detail null", Toast.LENGTH_LONG).show();
//                                        getArDetail(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSalesReportDetail();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String KodeBarang = jsonArray.getJSONObject(i).getString("KodeBarang");
                                                String NamaBarang = jsonArray.getJSONObject(i).getString("NamaBarang");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String Isi = jsonArray.getJSONObject(i).getString("Isi");
                                                String HargaJual = jsonArray.getJSONObject(i).getString("HargaJual");
                                                String Quantity = jsonArray.getJSONObject(i).getString("Quantity");
                                                String Diskon1 = jsonArray.getJSONObject(i).getString("Diskon1");
                                                String Diskon2 = jsonArray.getJSONObject(i).getString("Diskon2");
                                                String Subtotal = jsonArray.getJSONObject(i).getString("Subtotal");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("idBarang", idBarang);
                                                values.put("KodeBarang", KodeBarang);
                                                values.put("NamaBarang", NamaBarang);
                                                values.put("Satuan", Satuan);
                                                values.put("Isi", Isi);
                                                values.put("HargaJual", HargaJual);
                                                values.put("Quantity", Quantity);
                                                values.put("Diskon1", Diskon1);
                                                values.put("Diskon2", Diskon2);
                                                values.put("Subtotal", Subtotal);
                                                values.put("Status", "");
                                                values.put("Keterangan", "");
                                                values.put("Diskon", "");
                                                values.put("DiskonPersen3", "");
                                                values.put("DiskonPersen4", "");
                                                values.put("DiskonPersen5", "");
                                                dataBase.insert("sales_report_detail", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sales report detail success", Toast.LENGTH_SHORT).show();
                                            if (isSync == false) {
                                                type = 1;
                                                Calendar c = Calendar.getInstance();
                                                day = c.get(Calendar.DATE);
                                                month = c.get(Calendar.MONTH);
                                                year = c.get(Calendar.YEAR);
                                                showDialog(12);
                                                dialog.dismiss();
                                            } else {
//                                                Toast.makeText(getApplicationContext(),dateFrom+"#"+dateTo,Toast.LENGTH_LONG).show();
                                                Cursor salesCount = dataBase.countSalesReport(idPelanggan, dateFrom, dateTo);
                                                salesCount.moveToFirst();
                                                jmlSales = salesCount.getInt(0);
//                                                currentSalesTV.setText("Current Sales : " + jmlSales);
                                                currentSalesTV.setText("Current Sales : Rp." + formatter.format(jmlSales));
                                                getArReminder(idPelanggan);
                                            }
//                                            String fromDateStr = setDateReverse(fromDate.toString());
//                                            String toDateStr = setDateReverse(toDate.toString());
//                                            Intent intent = new Intent(getApplicationContext(), SalesReportActivity.class);
//                                            intent.putExtra("query", "'"+idPelanggan+"'");
//                                            intent.putExtra("fromDate", fromDateStr);
//                                            intent.putExtra("toDate", toDateStr);
//                                            intent.putExtra("isSingle", "1");
//                                            startActivity(intent);

                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Sales Report detail : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Sales Report detail : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Sales Report detail : " + e.toString());
                }
            });
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getDate(String dateSource) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            try {
                date = format.parse(dateSource);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dateFormat.format(date);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(DashboardCustomerActivity.this);
            builder.setMessage("Apakah Anda Benar-Benar Ingin Keluar?")
                    .setCancelable(false)
                    .setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    Intent intent = new Intent(DashboardCustomerActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();

                        }
                    }).show();
        }
        return true;
    }

    public void getArReminder(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.AR_REMINDER_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
//                                        Toast.makeText(getApplicationContext(), "ar reminder null", Toast.LENGTH_LONG).show();
//                                        dialog.dismiss();
                                        if (isSync == false) {
                                            Toast.makeText(getApplicationContext(), "ar reminder null", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "ar report null", Toast.LENGTH_LONG).show();
                                            arOutstandingTV.setText("AR Outstanding : Rp.0");
                                            getArDetail(idPelanggan);
                                        }
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteARReminder();
                                            jmlArOutstanding = 0;
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String JatuhTempo = "-";
                                                try {
                                                    JatuhTempo = jsonArray.getJSONObject(i).getJSONObject("JatuhTempo").getString("date");
                                                    JatuhTempo = getDate(JatuhTempo);
                                                } catch (Exception e) {
//													alert.showAlert("AR Reminder : "+e.toString());
                                                    JatuhTempo = "-";
                                                }
                                                Tanggal = getDate(Tanggal);
                                                String Umur = jsonArray.getJSONObject(i).getString("Umur");
                                                String Over = jsonArray.getJSONObject(i).getString("Over");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String Bayar = jsonArray.getJSONObject(i).getString("Bayar");
                                                if (Bayar.equals("null")) {
                                                    Bayar = "0";
                                                }
//												String Potongan = jsonArray.getJSONObject(i).getString("Potongan");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Sisa = jsonArray.getJSONObject(i).getString("Sisa");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("JatuhTempo", JatuhTempo);
                                                values.put("Umur", Umur);
                                                values.put("Over", Over);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("Bayar", Bayar);
//												values.put("Potongan", Potongan);
                                                values.put("Potongan", "0");
                                                values.put("Sisa", Sisa);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("ar_reminder", values);
                                                jmlArOutstanding = jmlArOutstanding + Double.parseDouble(Sisa);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync ar reminder pelanggan success", Toast.LENGTH_SHORT).show();
                                            if (isSync == false) {
                                                dialog.dismiss();
                                                Intent intent = new Intent(getApplicationContext(), ArReminderDetailActivity.class);
                                                intent.putExtra("query", idPelanggan);
                                                startActivity(intent);
                                            } else {
//                                                arOutstandingTV.setText("AR Outstanding : "+String.valueOf(jmlArOutstanding));
                                                arOutstandingTV.setText("AR Outstanding : Rp." + formatter.format(jmlArOutstanding));
                                                getArDetail(idPelanggan);
                                            }

                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("AR Reminder : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("AR Reminder : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("AR Reminder : " + e.toString());
                }
            });
        }
    }

    public void getDeliveryReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_DELIVERY_REPORT_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "delivery report null", Toast.LENGTH_LONG).show();
                                        getReturnReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteDeliveryReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String TanggalTerima;
                                                try {
                                                    TanggalTerima = jsonArray.getJSONObject(i).getJSONObject("TanggalTerima").getString("date");
                                                    TanggalTerima = getDate(TanggalTerima);
                                                } catch (Exception e) {
                                                    TanggalTerima = "-";
                                                }
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("TanggalTerima", TanggalTerima);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("delivery_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync delivery report pelanggan success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), DeliveryActivity.class);
                                            intent.putExtra("query", idPelanggan);
                                            intent.putExtra("fromDate", "");
                                            intent.putExtra("toDate", "");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Delivery Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Delivery Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Delivery Report : " + e.toString());
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();
        int tempday = c.get(Calendar.DATE);
        int tempmonth = c.get(Calendar.MONTH);
        int tempyear = c.get(Calendar.YEAR);
        DatePickerDialog datePick = new DatePickerDialog(this, pickerListener, tempyear, tempmonth, tempday);
        if (type == 1) {
            datePick.setTitle("From Date");
        } else {
            datePick.setTitle("To Date");
        }
        return datePick;
//        return new DatePickerDialog(this, pickerListener, tempyear, tempmonth,tempday);
    }

    private String setDateReverse(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ") && !dateString.equals("") && !dateString.equals(null)) {
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            result = fmtOut.format(date);
        }

        return result;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }

}
