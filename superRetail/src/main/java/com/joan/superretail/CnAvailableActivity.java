package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.CnAvailableAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.util.ArrayList;

public class CnAvailableActivity extends Activity {
    String query;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> tipeList = new ArrayList<String>();
    ArrayList<String> noreturList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> keteranganList = new ArrayList<String>();
    ArrayList<String> jumlahList = new ArrayList<String>();
    ArrayList<String> pakaiList = new ArrayList<String>();
    ArrayList<String> sisaList = new ArrayList<String>();
    ArrayList<String> salesidList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cn_available);
        Bundle extras = getIntent().getExtras();
        query = extras.getString("query");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv10.setTypeface(bold);
        Cursor reminderCsr = dataBase.selectCnAvailable(query);
//        Cursor reminderCsr = dataBase.selectArReminder(query);
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            tipeList.clear();
            noreturList.clear();
            pelangganList.clear();
            keteranganList.clear();
            jumlahList.clear();
            pakaiList.clear();
            sisaList.clear();
            salesidList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String tipe = reminderCsr.getString(2);
                String noretur = reminderCsr.getString(3);
                String pelanggan = reminderCsr.getString(4);
                String keterangan = reminderCsr.getString(5);
                String jumlah = reminderCsr.getString(6);
                String pakai = reminderCsr.getString(7);
                String sisa = reminderCsr.getString(8);
                String salesid = reminderCsr.getString(9);
//                Double.parseDouble(jumlah);
//                Double.parseDouble(pakai);
//                Double.parseDouble(sisa);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                tipeList.add(tipe);
                noreturList.add(noretur);
                pelangganList.add(pelanggan);
                keteranganList.add(keterangan);
                jumlahList.add(jumlah);
                pakaiList.add(pakai);
                sisaList.add(sisa);
                salesidList.add(salesid);
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new CnAvailableAdapter(context, nomorList,
                    tanggalList, tipeList, noreturList, pelangganList,
                    keteranganList, jumlahList, pakaiList, sisaList, salesidList));
        }
    }
}
