package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.joan.superretail.adapter.SOReportDetailAdapter;
import com.joan.superretail.constant.Constant;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.IPManager;
import com.joan.superretail.helper.ProgressDialogManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SOReportDetailActivity extends Activity {

    String Nomor;
    String fromDate;
    String toDate;
    String Pelanggan;
    String Query;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> idList = new ArrayList<String>();
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> quantityKirimList = new ArrayList<String>();
    ArrayList<String> quantitySisaList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> d1List = new ArrayList<String>();
    ArrayList<String> d2List = new ArrayList<String>();
    ArrayList<String> statusList = new ArrayList<String>();
    ArrayList<String> subtotalList = new ArrayList<String>();
    ArrayList<String> statusUpdateList = new ArrayList<String>();
    ArrayList<String> kodeUpdateList = new ArrayList<String>();
    ArrayList<String> nomorUpdateList = new ArrayList<String>();
    ListView arLV;
    String Total;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15;
    Button btnSumbit;
    IPManager ipManager;
    GPSTracker gpsTracker;
    double myLatitude;
    double myLongitude;
    int jmlUpdate = 0;
    int updateKe = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soreport_detail);
        ipManager = new IPManager();
        Bundle extras = getIntent().getExtras();
        Nomor = extras.getString("Nomor");
        Total = extras.getString("Total");
        Pelanggan = extras.getString("Pelanggan");
        Query = extras.getString("query");
        dataBase = DataBaseManager.instance();
        gpsTracker = new GPSTracker(this);
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        tv11 = (TextView) findViewById(R.id.tv11);
        tv12 = (TextView) findViewById(R.id.tv12);
        tv13 = (TextView) findViewById(R.id.tv13);
        tv14 = (TextView) findViewById(R.id.tv14);
        tv15 = (TextView) findViewById(R.id.tv15);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv10.setTypeface(bold);
        tv11.setTypeface(bold);
        tv12.setTypeface(bold);
        tv13.setTypeface(bold);
        tv14.setTypeface(bold);
        tv15.setTypeface(bold);
        btnSumbit = (Button) findViewById(R.id.btnSubmit);
        titleTV.setText("SO Report | " + Nomor + " | " + Pelanggan);
        dataBase.deleteUpdateStatusTemp();
        final Cursor reminderCsr = dataBase.selectSOReportDetail(Nomor);
        if (reminderCsr.getCount() > 0) {
            idList.clear();
            nomorList.clear();
            kodeList.clear();
            namaList.clear();
            quantityList.clear();
            satuanList.clear();
            isiList.clear();
            hargaList.clear();
            subtotalList.clear();
            quantityKirimList.clear();
            quantitySisaList.clear();
            statusList.clear();
            d1List.clear();
            d2List.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String kode = reminderCsr.getString(1);
                String nama = reminderCsr.getString(2);
                String quantity = reminderCsr.getString(3);
                String quantityKirim = reminderCsr.getString(4);
                String quantitySisa = reminderCsr.getString(5);
                String satuan = reminderCsr.getString(6);
                String isi = reminderCsr.getString(7);
                String harga = reminderCsr.getString(8);
                String d1 = reminderCsr.getString(9);
                String d2 = reminderCsr.getString(10);
                String status = reminderCsr.getString(11);
                String subtotal = reminderCsr.getString(12);
                String idBarang = reminderCsr.getString(13);
                quantityKirimList.add(quantityKirim);
                quantitySisaList.add(quantitySisa);
                d1List.add(d1);
                d2List.add(d2);
                statusList.add(status);
                nomorList.add(nomor);
                kodeList.add(kode);
                namaList.add(nama);
                quantityList.add(quantity);
                satuanList.add(satuan);
                isiList.add(isi);
                hargaList.add(harga);
                subtotalList.add(subtotal);
                idList.add(idBarang);
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new SOReportDetailAdapter(context, nomorList, kodeList, namaList, quantityList, quantityKirimList
                    , quantitySisaList, satuanList, isiList, hargaList, d1List, d2List, statusList, subtotalList, idList, Total));


        }

        btnSumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gpsTracker.getIsGPSTrackingEnabled()) {
                    myLatitude = gpsTracker.latitude;
                    myLongitude = gpsTracker.longitude;
                    Cursor csr = dataBase.selectUpdateStatusTemp();
                    if (csr.getCount() > 0) {
                        csr.moveToFirst();
                        kodeUpdateList.clear();
                        statusUpdateList.clear();
                        nomorUpdateList.clear();
                        String a = "";
                        jmlUpdate = 0;
                        while (csr.isAfterLast() == false) {
                            nomorUpdateList.add(csr.getString(0));
                            kodeUpdateList.add(csr.getString(1));
                            statusUpdateList.add(csr.getString(2));
                            a = a + csr.getString(0) + "," + csr.getString(1) + "," + csr.getString(2) + "#";
                            csr.moveToNext();
                            jmlUpdate = jmlUpdate + 1;
                        }
//                        Toast.makeText(getApplicationContext(),a+"->"+String.valueOf(jmlUpdate),Toast.LENGTH_LONG).show();
                        updateKe = 0;
                        dialog.show();
                        updateStatus();
                    }
                } else {
                    gpsTracker.showSettingsAlert();
                }

//                    dataBase.updateSoStatus(
//                            "PENDING",
//                            "SO-16030303",
//                            "PS317");
//                    Toast.makeText(getApplicationContext(),"UPDATE",Toast.LENGTH_LONG).show();
            }
        });

    }

//    public void updateStatus()
////    public void updateStatus(final String Nomor, final String idBarang, final String Status)
//    {
//        try
//        {
//            Thread t = new Thread()
//            {
//                public void run()
//                {
//                    Looper.prepare();
//                    try
//                    {
//                        HttpPost post = new HttpPost(ipManager.getIP()+ Constant.UPDATE_STATUS_ORDER);
//                        StringEntity se;
//                        HttpResponse response;
//                        HttpClient client = new DefaultHttpClient();
//                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
//                        JSONObject json = new JSONObject();
//                        json.put("Nomor",nomorUpdateList.get(updateKe));
//                        json.put("idBarang",kodeUpdateList.get(updateKe));
//                        json.put("Status",statusUpdateList.get(updateKe));
//                        se = new StringEntity( json.toString());
//                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//                        post.setEntity(se);
//                        response = client.execute(post);
//                        StringBuilder sb = new StringBuilder();
//                        InputStream in = response.getEntity().getContent();
//                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
//                        String line;
//                        while ((line = br.readLine()) != null)
//                        {
//                            sb.append(line);
//                        }
//                        String arrResult = sb.toString();
////                        Toast.makeText(getApplicationContext(),arrResult,Toast.LENGTH_LONG).show();
//                        if(updateKe<nomorUpdateList.size()-1){
//                            updateKe = updateKe + 1;
//                            updateStatus();
//                        }else {
//                            Toast.makeText(getApplicationContext(),"Update Order Success",Toast.LENGTH_LONG).show();
//                            Cursor csr = dataBase.selectAccount();
//                            csr.moveToFirst();
//                            String idSalesman = csr.getString(3);
//                            sendLog(idSalesman,String.valueOf(myLatitude),String.valueOf(myLongitude),"UPDATE ORDER");
//                            dialog.dismiss();
//                            updataLocalDb();
//                        }
//                    }
//                    catch(final Exception e)
//                    {
//                        runOnUiThread(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                alert.showAlert("updateStatusOrder : "+ e.toString());
//                            }
//                        });
//                    }
//                    Looper.loop();
//                }
//            };
//            t.start();
//        }
//        catch(final Exception e)
//        {
//            runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    alert.showAlert(e.toString());
//                }
//            });
//        }
//    }

    public void updateStatus() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.UPDATE_STATUS_ORDER);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("Nomor", nomorUpdateList.get(updateKe));
                        json.put("idBarang", kodeUpdateList.get(updateKe));
                        json.put("Status", statusUpdateList.get(updateKe));
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "info salesman null", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                if (updateKe < nomorUpdateList.size() - 1) {
                                    updateKe = updateKe + 1;
                                    updateStatus();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Update Order Success", Toast.LENGTH_LONG).show();
                                    Cursor csr = dataBase.selectAccount();
                                    csr.moveToFirst();
                                    String idSalesman = csr.getString(3);
                                    sendLog(idSalesman, String.valueOf(myLatitude), String.valueOf(myLongitude), "UPDATE ORDER");
                                    dialog.dismiss();
                                    updataLocalDb();
                                }
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Info Salesman : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("AR Reminder : " + e.toString());
                }
            });
        }
    }

    public void sendLog(final String idSalesman, final String Latitude, final String Longitude, final String Remarks) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SEND_LOG);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        json.put("Latitude", Latitude);
                        json.put("Longitude", Longitude);
                        json.put("Remarks", Remarks);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        response = client.execute(post);
                        StringBuilder sb = new StringBuilder();
                        InputStream in = response.getEntity().getContent();
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        String arrResult = sb.toString();
//						Toast.makeText(getApplicationContext(),arrResult,Toast.LENGTH_LONG).show();
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                alert.showAlert("sendlog : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    alert.showAlert(e.toString());
                }
            });
        }
    }

    public void updataLocalDb() {
        for (int i = 0; i < kodeUpdateList.size(); i++) {
            dataBase.updateSoStatus(statusUpdateList.get(i), nomorUpdateList.get(i), kodeUpdateList.get(i));
        }
        Intent intent = new Intent(getApplicationContext(), SOReportActivity.class);
        intent.putExtra("query", Query);
        intent.putExtra("fromDate", "");
        intent.putExtra("toDate", "");
        startActivity(intent);
        finish();
    }
}
