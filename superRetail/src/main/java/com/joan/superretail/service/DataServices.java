package com.joan.superretail.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.widget.Toast;

import com.joan.superretail.constant.Constant;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.IPManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
//import android.widget.Toast;


public class DataServices extends Service {
    DataBaseManager dataBase;
    boolean isSendOnProg = false;
    IPManager ipManager;
    double myLatitude;
    double myLongitude;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        PendingIntent pi;
        AlarmManager am;

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent i) {
                try {
                    dataBase = DataBaseManager.instance();
                    if (Constant.SERVICE) {
                        if (isSendOnProg == false) {
//							Toast.makeText(getApplicationContext(), "isSendProg == false", Toast.LENGTH_LONG).show();
                            checkPendingOrder(c);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isSendOnProg = false;
                }
            }
        };

        registerReceiver(br, new IntentFilter(Constant.INTENT_FILTER_TAG));
        pi = PendingIntent.getBroadcast(this, 0, new Intent(Constant.INTENT_FILTER_TAG), 0);
        am = (AlarmManager) (this.getSystemService(Context.ALARM_SERVICE));
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + Constant.PERIODIC_TIME, Constant.PERIODIC_TIME, pi);
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void checkPendingOrder(Context context) {
        ipManager = new IPManager();
        Cursor salesCsr = dataBase.selectPendingSalesOrder();
        if (salesCsr.getCount() > 0) {
//			Toast.makeText(getApplicationContext(), "Pending sales order > 0", Toast.LENGTH_LONG).show();
            isSendOnProg = true;
            salesCsr.moveToFirst();
            String id = salesCsr.getString(0);
            sendOrderToserver(id);
        } else {
            isSendOnProg = false;
        }
    }

    private void sendOrderToserver(final String id) {
//		GPSTracker gpsTracker = new GPSTracker(this);
//		if (gpsTracker.getIsGPSTrackingEnabled())
//		{
//			myLatitude = gpsTracker.latitude;
//			myLongitude = gpsTracker.longitude;
        Thread t = new Thread() {
            public void run() {
                Looper.prepare();
                try {
                    Cursor csrv = dataBase.selectPendingSalesOrderDetail(id);
                    String srv = "";
                    if (csrv.getCount() > 0) {
                        csrv.moveToFirst();
//							Toast.makeText(getApplicationContext(),csr.getString(0),Toast.LENGTH_LONG).show();
                        if (csrv.getString(7).equals("Sales")) {
//                            srv = Constant.DATA_SERVICE;
                            srv = Constant.NEW_DATA_SERVICE;
                        } else {
//                            srv = Constant.DATA_SERVICE_PRE;
                            srv = Constant.NEW_DATA_SERVICE_PRE;
                        }
                    } else {
                        srv = Constant.NEW_DATA_SERVICE;
                    }
//                    HttpPost post = new HttpPost(ipManager.getIP() + srv);
                    HttpPost post = new HttpPost(Constant.NEW_BASE_URL + srv);
                    StringEntity se;
                    HttpResponse response;
                    HttpClient client = new DefaultHttpClient();
                    HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                    JSONArray array = new JSONArray();
                    Cursor orderCsr = dataBase.selectPendingSalesOrderDetail(id);
                    if (orderCsr.getCount() > 0) {
                        orderCsr.moveToFirst();
                        while (orderCsr.isAfterLast() == false) {
                            String idSalesman = orderCsr.getString(0);
                            String idPelanggan = orderCsr.getString(1);
                            String idBarang = orderCsr.getString(2);
                            String Quantity = orderCsr.getString(3);
                            String Satuan = orderCsr.getString(4);
                            String Isi = orderCsr.getString(5);
                            String Keterangan = orderCsr.getString(6);
                            Quantity = Quantity.replace(",", "");
                            Isi = Isi.replace(",", "");
                            Quantity = Quantity.replace(".", "");
                            Isi = Isi.replace(".", "");
                            JSONObject json = new JSONObject();
                            json.put("idSalesman", idSalesman);
                            json.put("idPelanggan", idPelanggan);
                            json.put("idBarang", idBarang);
                            json.put("Quantity", Quantity);
                            json.put("Satuan", Satuan);
                            json.put("Isi", Isi);
                            json.put("Keterangan", Keterangan);
                            array.put(json);
                            orderCsr.moveToNext();
                        }
                    }
                    se = new StringEntity(array.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    if (post.isAborted()) {
                        isSendOnProg = false;
                    } else {
                        response = client.execute(post);
                        StringBuilder sb = new StringBuilder();
                        InputStream in = response.getEntity().getContent();
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        String arrResult = sb.toString();
//	    				Toast.makeText(getApplicationContext(), arrResult, Toast.LENGTH_LONG).show();
                        if (arrResult.equals("")) {
                            isSendOnProg = false;
                        } else {
                            final JSONObject jsonResult = new JSONObject(arrResult);
                            String Nomor = jsonResult.getString("Nomor");
                            if (!Nomor.equals("0")) {
//	    						Toast.makeText(getApplicationContext(), Nomor, Toast.LENGTH_LONG).show();
                                dataBase.deletePendingSalesOrder(id);
//									Cursor csr = dataBase.selectAccount();
//									csr.moveToFirst();
//									String idSalesman = csr.getString(3);
//									sendLog(idSalesman,String.valueOf(myLatitude),String.valueOf(myLongitude),"Order");
//								Toast.makeText(getApplicationContext(),"SendLog From Order",Toast.LENGTH_LONG).show();
                            }
                            isSendOnProg = false;
                        }
                    }
                } catch (final Exception e) {
                    isSendOnProg = false;
                }
                Looper.loop();
            }
        };
        t.start();
//		}else{
//			gpsTracker.showSettingsAlert();
//		}

    }

    public void sendLog(final String idSalesman, final String Latitude, final String Longitude, final String Remarks) {
//		Toast.makeText(getApplicationContext(),"SendLog Process",Toast.LENGTH_LONG).show();
        try {
            HttpPost post = new HttpPost(ipManager.getIP() + Constant.SEND_LOG);
            StringEntity se;
            HttpResponse response;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
            JSONObject json = new JSONObject();
            json.put("idSalesman", idSalesman);
            json.put("Latitude", Latitude);
            json.put("Longitude", Longitude);
            json.put("Remarks", Remarks);
            se = new StringEntity(json.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            StringBuilder sb = new StringBuilder();
            InputStream in = response.getEntity().getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            String arrResult = sb.toString();
            Toast.makeText(getApplicationContext(), arrResult, Toast.LENGTH_LONG).show();
        } catch (final Exception e) {
//			Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
        Looper.loop();
    }
}