package com.joan.superretail;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.ListBarangAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.model.Barangs;

import java.util.ArrayList;
import java.util.Collections;

public class OrderActivity extends FragmentActivity {

    //	ArrayList<String> idList = new ArrayList<String>();
//    ArrayList<String> nameList = new ArrayList<String>();
    public ArrayList<Barangs> barangs = new ArrayList<Barangs>();
    DataBaseManager dataBase;
    AlertDialogManager alert;
    ListView listBarangLV;
    String idPelanggan;
    String idKategori;
    String levelHarga;
    String perintah;
    String counter;
    String so;
    Context context;
    TextView titleTV, checkOutTV, totalItemTV;
    Typeface bold, regular, medium;
    FontManager fm;
    int totalItemID;
    private ViewPager pager;
    private MyPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Bundle extras = getIntent().getExtras();
        idPelanggan = extras.getString("idPelanggan");
        idKategori = extras.getString("idKategori");
        levelHarga = extras.getString("levelHarga");
        perintah = extras.getString("perintah");
        so = extras.getString("so");
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        counter = extras.getString("counter");
        pager = (ViewPager) findViewById(R.id.pager);
        titleTV = (TextView) findViewById(R.id.titleTV);
        checkOutTV = (TextView) findViewById(R.id.checkOutTV);
        totalItemTV = (TextView) findViewById(R.id.totalItemTV);
        totalItemID = totalItemTV.getId();
        listBarangLV = (ListView) findViewById(R.id.listBarangLV);
        context = this;
        dataBase = DataBaseManager.instance();
        titleTV.setTypeface(bold);
        totalItemTV.setTypeface(bold);
        checkOutTV.setTypeface(regular);
        alert = new AlertDialogManager(this);

        try {
//			idList.clear();
//			nameList.clear();
            barangs.clear();
            Cursor barangCsr;
//			Toast.makeText(getApplicationContext(),perintah,Toast.LENGTH_LONG).show();
            //if (levelHarga.equals("1") || levelHarga.equals("2") || levelHarga.equals("6")) {
            if (so.equalsIgnoreCase("so1")) {
                if (perintah.equals("sales")) {
                    barangCsr = dataBase.selectIdNamaBarangBy12(idKategori, idPelanggan);
                } else {
                    barangCsr = dataBase.selectIdNamaBarangByPreOrder12(idKategori, idPelanggan);
                }
            } else {
                if (perintah.equals("sales")) {
                    barangCsr = dataBase.selectIdNamaBarangBy3(idKategori, idPelanggan);
                } else {
                    if (levelHarga.equals("1") || levelHarga.equals("2") || levelHarga.equals("3")  || levelHarga.equals("6")) {
                        barangCsr = dataBase.selectIdNamaBarangByPreOrder12(idKategori, idPelanggan);
                    } else {
                        barangCsr = dataBase.selectIdNamaBarangByPreOrder3(idKategori, idPelanggan);
                    }
                    //barangCsr = dataBase.selectIdNamaBarangByPreOrder3(idKategori,idPelanggan);
                }
            }
            if (barangCsr.getCount() > 0) {
                barangCsr.moveToFirst();
                while (barangCsr.isAfterLast() == false) {
                    String id = barangCsr.getString(0);
                    String nama = barangCsr.getString(1);
                    String Kode = barangCsr.getString(2);
//					idList.add(id);
//					nameList.add(nama);
                    Barangs barang = new Barangs(id, nama, false, Kode);
                    barangs.add(barang);
                    barangCsr.moveToNext();
                }

                Collections.sort(barangs, Barangs.StuNameComparator);
                listBarangLV.setAdapter(new ListBarangAdapter(context, barangs));
                listBarangLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listBarangLV.setSelection(0);
                listBarangLV.setItemChecked(0, true);
                adapter = new MyPagerAdapter(getSupportFragmentManager());
                listBarangLV.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        pager.setCurrentItem(position);
//						view.setSelected(true);
                    }
                });
                pager.setAdapter(adapter);
                pager.setOnPageChangeListener(new OnPageChangeListener() {

                    // This method will be invoked when a new page becomes selected.
                    @Override
                    public void onPageSelected(int position) {
                        listBarangLV.setItemChecked(position, true);
                        listBarangLV.setSelection(position);
                    }

                    // This method will be invoked when the current page is scrolled
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        // Code goes here
                    }

                    // Called when the scroll state changes:
                    // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
                    @Override
                    public void onPageScrollStateChanged(int state) {
                        // Code goes here
                    }
                });
                checkOutTV.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Cursor count = dataBase.selectOrder();
                        if (count.getCount() > 0) {
                            Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                            intent.putExtra("so", so);
                            startActivityForResult(intent, 1);
                        }
                    }
                });
            } else {

            }
        } catch (Exception e) {
            alert.showAlert(e.toString());
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//		totalTV = (TextView)findViewById(R.id.totalTV);
//		Cursor orderCSR = dataBase.selectOrder();
//		if(orderCSR.getCount()>0)
//		{
//			int count = orderCSR.getCount();
//			totalTV.setText("Total items ("+Integer.toString(count)+")");
//		}
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return Integer.parseInt(counter);
        }

        @Override
        public Fragment getItem(int position) {

//			if(position==Integer.parseInt(counter)-1)
//			{
//				listBarangLV.setItemChecked(position, true);
//				listBarangLV.setSelection(position);
//			}
//			else
//			{
//				listBarangLV.setItemChecked(position-1, true);
//				listBarangLV.setSelection(position-1);
//
//			}
            return CardFragment.newInstance(position, barangs.get(position).getId(), levelHarga
                    , idPelanggan, barangs.get(position).getStatus(), totalItemID);
        }
    }
}