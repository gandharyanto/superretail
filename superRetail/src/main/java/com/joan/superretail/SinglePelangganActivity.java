package com.joan.superretail;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.model.ListPelanggan;

import java.util.ArrayList;
import java.util.Calendar;

public class SinglePelangganActivity extends Activity {

    ArrayList<ListPelanggan> pelangganList = new ArrayList<ListPelanggan>();
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    TextView titleTV;
    MyCustomAdapter dataAdapter = null;
    String intentType;
    LinearLayout dateLL;
    TextView fromDateTV, toDateTV;
    boolean isFromDate = false;
    boolean isToDate = false;
    int type;
    private int year;
    private int month;
    private int day;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            String dayStr = Integer.toString(selectedDay);
            String monthStr = Integer.toString(selectedMonth + 1);
            if (selectedDay < 10) {
                dayStr = "0" + dayStr;
            }
            if (selectedMonth < 10) {
                monthStr = "0" + monthStr;
            }
//        year  = selectedYear;
//        month = selectedMonth;
//        day   = selectedDay;
            String dateStr = Integer.toString(selectedYear) + "-" + monthStr + "-" + dayStr;
            if (type == 1) {
                fromDateTV.setText(dateStr);
                isFromDate = true;
            } else if (type == 2) {
                toDateTV.setText(dateStr);
                isToDate = true;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pelanggan);
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        Calendar c = Calendar.getInstance();
        day = c.get(Calendar.DATE);
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);
        dateLL = (LinearLayout) findViewById(R.id.dateLL);
        fromDateTV = (TextView) findViewById(R.id.fromDateTV);
        toDateTV = (TextView) findViewById(R.id.toDateTV);
        Bundle extras = getIntent().getExtras();
        intentType = extras.getString("intentType");

        fromDateTV.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                type = 1;
                showDialog(12);
            }
        });
        toDateTV.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                type = 2;
                showDialog(12);
            }
        });
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        titleTV.setTypeface(medium);
        Cursor pelangganCsr = dataBase.selectIdNamaPelanggan();
        if (pelangganCsr.getCount() > 0) {
            pelangganList.clear();
            pelangganCsr.moveToFirst();
            while (pelangganCsr.isAfterLast() == false) {
                String id = pelangganCsr.getString(0);
                String name = pelangganCsr.getString(1);
                ListPelanggan pelanggan = new ListPelanggan(id, name, false);
                pelangganList.add(pelanggan);
                pelangganCsr.moveToNext();
            }
            dataAdapter = new MyCustomAdapter(this,
                    R.layout.pelanggan_checkbox, pelangganList);
            ListView listView = (ListView) findViewById(R.id.listKategoriLV);
            // Assign adapter to ListView
            listView.setAdapter(dataAdapter);


            listView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
//					   if(isFromDate==true&&isToDate==true)
//						{
                    String fromDateStr = fromDateTV.getText().toString();
                    String toDateStr = toDateTV.getText().toString();
                    Intent intent = null;
                    if (intentType.equals("1")) {
                        intent = new Intent(getApplicationContext(), ReturnReportActivity.class);

                    } else if (intentType.equals("2")) {
                        intent = new Intent(getApplicationContext(), ARDetailActivity.class);
                    } else if (intentType.equals("3")) {
                        intent = new Intent(getApplicationContext(), SalesReportActivity.class);
                    }
                    intent.putExtra("id", pelangganList.get(position).getId());
                    intent.putExtra("fromDate", fromDateStr);
                    intent.putExtra("toDate", toDateStr);
//							if(intentType.equals("3"))
//							{
//								if(isFromDate==true&&isToDate==true)
//								{
//									startActivity(intent);
//								}
//								else
//								{
//									alert.showAlert("Mohon pilih tanggal !");
//								}
//							}
//							else
//							{
                    startActivity(intent);
//							}

//						}
                }
            });

        } else {
            alert.showAlert("Tidak ada data. Mohon sync data terlebih dahulu");
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, pickerListener, year, month, day);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(SinglePelangganActivity.this, ReportMenuActivity.class);
            startActivity(intent);
            this.finish();
//			AlertDialog.Builder builder = new AlertDialog.Builder(SinglePelangganActivity.this);
//			builder.setMessage("Apakah Anda Benar-Benar Ingin Keluar?")
//					.setCancelable(false)
//					.setPositiveButton("Ya",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//													int id) {
//									finish();
//									System.exit(0);
//								}
//							})
//					.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog,
//											int id) {
//							dialog.cancel();
//
//						}
//					}).show();
        }
        return true;
    }

    private class MyCustomAdapter extends ArrayAdapter<ListPelanggan> {

        private ArrayList<ListPelanggan> countryList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<ListPelanggan> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<ListPelanggan>();
            this.countryList.addAll(countryList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));
            final ListPelanggan country = countryList.get(position);
            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.model_single_pelanggan_list, null);
                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.code.setText(country.getName());
            holder.code.setTypeface(medium);
            return convertView;

        }

        private class ViewHolder {
            TextView code;
        }

    }
}
