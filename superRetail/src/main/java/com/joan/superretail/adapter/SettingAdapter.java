package com.joan.superretail.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joan.superretail.LoginActivity;
import com.joan.superretail.R;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.model.Settings;

import java.util.ArrayList;

public class SettingAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    DataBaseManager dataBase;
    Context context;
    ArrayList<Settings> settingList = new ArrayList<Settings>();

    public SettingAdapter(Context c, ArrayList<Settings> settingList) {
        context = c;
        this.settingList = settingList;
        fm = new FontManager(c);
        dataBase = DataBaseManager.instance();
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
    }

    public int getCount() {

        return settingList.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        final int where = position;
        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_setting, null);
        TextView profileNameTV = (TextView) gridView.findViewById(R.id.profileNameTV);
        profileNameTV.setTypeface(regular);
        profileNameTV.setText(settingList.get(position).getNama());
        TextView profileIPTV = (TextView) gridView.findViewById(R.id.profileIPTV);
        profileIPTV.setTypeface(regular);
        profileIPTV.setText(settingList.get(position).getIP());
        final LinearLayout containerLL = (LinearLayout) gridView.findViewById(R.id.containerLL);
        final LinearLayout useLL = (LinearLayout) gridView.findViewById(R.id.useLL);
        LinearLayout deleteLL = (LinearLayout) gridView.findViewById(R.id.deleteLL);
        if (settingList.get(position).getAktif().equals("1")) {
            //	containerLL.setBackgroundColor(context.getResources().getColor(R.color.accent));
            useLL.setVisibility(LinearLayout.GONE);
        } else {
            useLL.setVisibility(LinearLayout.VISIBLE);
            //	containerLL.setBackgroundColor(Color.TRANSPARENT);
        }
        useLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //		containerLL.setBackgroundColor(context.getResources().getColor(R.color.accent));
                settingList.get(where).setAktif("1");
                dataBase.resetSettingIP(settingList.get(where).getNama());
                dataBase.setIP(settingList.get(where).getNama());
                useLL.setVisibility(LinearLayout.GONE);
                containerLL.setBackgroundColor(Color.TRANSPARENT);
                notifyDataSetChanged();
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();

            }
        });
        deleteLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dataBase.deleteIP(settingList.get(where).getNama());
                settingList.remove(where);
                notifyDataSetChanged();
            }
        });
        return gridView;
    }
}