package com.joan.superretail.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SalesReportDetailAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    double sum = 0;
    double diskon, biaya;
    String grandTotal, Nomor;
    boolean isSumcalc = true;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> subtotalList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public SalesReportDetailAdapter(Context c, ArrayList<String> nomorList,
                                    ArrayList<String> kodeList, ArrayList<String> namaList, ArrayList<String> quantityList
            , ArrayList<String> satuanList, ArrayList<String> isiList, ArrayList<String> hargaList
            , ArrayList<String> subtotalList, String grandTotal, String Nomor) {
        context = c;
        this.nomorList = nomorList;
        this.kodeList = kodeList;
        this.namaList = namaList;
        this.quantityList = quantityList;
        this.satuanList = satuanList;
        this.isiList = isiList;
        this.hargaList = hargaList;
        this.subtotalList = subtotalList;
        this.grandTotal = grandTotal;
        this.Nomor = Nomor;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        dataBase = DataBaseManager.instance();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        Cursor diskCsr = dataBase.selectSalesReportBiayaDisk(Nomor);
        diskCsr.moveToFirst();
        String biayaStr = diskCsr.getString(0);
        String diskonStr = diskCsr.getString(1);
        double biayaDbl = Double.parseDouble(biayaStr);
        biaya = Math.floor(biayaDbl);
        double diskonDbl = Double.parseDouble(diskonStr);
        diskon = Math.floor(diskonDbl);
    }

    public int getCount() {

        return nomorList.size() + 4;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_sales_report_detail, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.kodeTV);
        jatuhTempoTV.setTypeface(regular);
        TextView overTV = (TextView) gridView.findViewById(R.id.quantityTV);
        overTV.setTypeface(regular);
        TextView umurTV = (TextView) gridView.findViewById(R.id.namaTV);
        umurTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.satuanTV);
        pelangganTV.setTypeface(regular);
        TextView bayarTV = (TextView) gridView.findViewById(R.id.isiTV);
        bayarTV.setTypeface(regular);
        TextView totalTV = (TextView) gridView.findViewById(R.id.hargaTV);
        totalTV.setTypeface(regular);
        TextView sisaTV = (TextView) gridView.findViewById(R.id.subtotalTV);
        sisaTV.setTypeface(regular);
        if (position < nomorList.size()) {
            nomorTV.setText(Integer.toString(position + 1));

            jatuhTempoTV.setText(kodeList.get(position));

            umurTV.setText(namaList.get(position));

            double squantityDbl = Double.parseDouble(quantityList.get(position));
            double quantityDblTmp = Math.floor(squantityDbl);
            overTV.setText(formatter.format(quantityDblTmp));

            pelangganTV.setText(satuanList.get(position));

            double totalDbl = Double.parseDouble(hargaList.get(position));
            double ttlDblTmp = Math.floor(totalDbl);
            totalTV.setText(formatter.format(ttlDblTmp));


            double isiDbl = Double.parseDouble(isiList.get(position));
            double isiDblTmp = Math.floor(isiDbl);
            bayarTV.setText(formatter.format(isiDblTmp));


            double sisaDbl = Double.parseDouble(subtotalList.get(position));
            double sisaDblTmp = Math.floor(sisaDbl);
            if (isSumcalc == true) {
                sum = sum + sisaDblTmp;
            }
            sisaTV.setText(formatter.format(sisaDblTmp));
        } else {
            totalTV.setTypeface(bold);
            sisaTV.setTypeface(bold);
            if (position == nomorList.size()) {
                double ttaDbl = Double.parseDouble(grandTotal);
                double sisaDbddlTmp = Math.floor(ttaDbl);
                double biayaDbddlTmp = Math.floor(biaya);
                double diskonDbddlTmp = Math.floor(diskon);
                double result = sisaDbddlTmp - biayaDbddlTmp - diskonDbddlTmp;
                isSumcalc = false;
                totalTV.setText("SUBTOTAL");
                sisaTV.setText(formatter.format(result));
            } else if (position == nomorList.size() + 1) {
                isSumcalc = false;
                totalTV.setText("DISKON");
                sisaTV.setText(formatter.format(diskon));
            } else if (position == nomorList.size() + 2) {
                isSumcalc = false;
                totalTV.setText("BIAYA");
                sisaTV.setText(formatter.format(biaya));
            } else if (position == nomorList.size() + 3) {
                isSumcalc = false;
                totalTV.setText("TOTAL");
                double ttaDbl = Double.parseDouble(grandTotal);
                double sisaDbddlTmp = Math.floor(ttaDbl);
                sisaTV.setText(formatter.format(sisaDbddlTmp));
            }


        }


        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}