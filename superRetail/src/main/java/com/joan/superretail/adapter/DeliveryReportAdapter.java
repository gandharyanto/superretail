package com.joan.superretail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DeliveryReportAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> TanggalTerimaList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public DeliveryReportAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> TanggalTerimaList,
                                 ArrayList<String> pelangganList, ArrayList<String> totalList) {
        context = c;
        this.nomorList = nomorList;
        this.TanggalTerimaList = TanggalTerimaList;
        this.pelangganList = pelangganList;
        this.totalList = totalList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_delivery_report, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(bold);
        nomorTV.setText(nomorList.get(position));
        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTerimaTV);
        tanggalTV.setTypeface(regular);
        tanggalTV.setText(setDate(TanggalTerimaList.get(position)));
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);
        pelangganTV.setText(pelangganList.get(position));
        TextView totalTV = (TextView) gridView.findViewById(R.id.totalTV);
        totalTV.setTypeface(regular);
        double totalDbl = Double.parseDouble(totalList.get(position));
        totalTV.setText(formatter.format(totalDbl));

        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}