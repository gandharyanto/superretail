package com.joan.superretail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.util.ArrayList;

public class ListMerkAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nameList = new ArrayList<String>();
//    ArrayList<Jenis> jenisList = new ArrayList<Jenis>();
//    ArrayList<Pelanggan> pelangganList = new ArrayList<Pelanggan>();

    public ListMerkAdapter(Context c, ArrayList<String> nameList) {
//	public ListMerkAdapter(Context c, ArrayList<String> nameList, ArrayList<Jenis> jenisList){
        context = c;
        this.nameList = nameList;
//		this.jenisList = jenisList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
    }

    public int getCount() {

        return nameList.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_pelanggan_list, null);
        TextView nameTV = (TextView) gridView.findViewById(R.id.nameTV);
        nameTV.setTypeface(medium);
        nameTV.setText(nameList.get(position));
//    			nameTV.setText(jenisList.get(position).getNama());
        return gridView;
    }
}