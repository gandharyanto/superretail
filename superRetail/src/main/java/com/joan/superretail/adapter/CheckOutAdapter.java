package com.joan.superretail.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.model.Orders;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

public class CheckOutAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<Orders> Orders = new ArrayList<Orders>();
    ImageView image;
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;
    TextView totalItemTV, kodeBarangTV, kuantitiBarangTV, namaBarangTV, hargaBarangTV, disk1TV, disk2TV;
    String so;

    //	CheckOutAdapter ctx;
    public CheckOutAdapter(Context c, ArrayList<Orders> Orders, ImageView image, TextView totalItemTV,
                           TextView kodeBarangTV, TextView kuantitiBarangTV, TextView namaBarangTV, TextView hargaBarangTV,
                           TextView disk1TV, TextView disk2TV, String so) {
        context = c;
        this.totalItemTV = totalItemTV;
        this.kodeBarangTV = kodeBarangTV;
        this.kuantitiBarangTV = kuantitiBarangTV;
        this.namaBarangTV = namaBarangTV;
        this.hargaBarangTV = hargaBarangTV;
        this.disk1TV = disk1TV;
        this.disk2TV = disk2TV;
        this.Orders = Orders;
        this.so = so;
        dataBase = DataBaseManager.instance();
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        this.image = image;
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        alert = new AlertDialogManager(context);
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return Orders.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final int where = position;
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_checkoutl, null);
        try {
            TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
            nomorTV.setTypeface(regular);
            nomorTV.setText(Integer.toString(position + 1));
            TextView kodeTV = (TextView) gridView.findViewById(R.id.kodeTV);
            kodeTV.setTypeface(regular);
            if (Orders.get(position).getKode().length() < 25) {
                kodeTV.setText(Orders.get(position).getKode());
            } else {
                kodeTV.setText(Orders.get(position).getKode().substring(0, 24));
            }
            if (position == 0) {
                kodeBarangTV.setText(Orders.get(0).getKode());
                String id = Orders.get(0).getIdBarang();
                Cursor detailCsr = dataBase.selectBarangDetail(id);
                detailCsr.moveToFirst();
                String Satuan = detailCsr.getString(9);
                String IsiBesar = detailCsr.getString(13);
                kuantitiBarangTV.setText(IsiBesar + " " + Satuan);
                namaBarangTV.setText(Orders.get(0).getNama());
                double hargaInt = Orders.get(where).getHarga();
                hargaBarangTV.setText(formatter.format(hargaInt));
                double d1Dbl = Orders.get(0).getD1();
                double d2Dbl = Orders.get(0).getD2();
                disk1TV.setText(Double.toString(d1Dbl));
                disk2TV.setText(Double.toString(d2Dbl));
            }
            final EditText qtyET = (EditText) gridView.findViewById(R.id.qtyET);
            qtyET.setTypeface(regular);
            ImageView deleteIV = (ImageView) gridView.findViewById(R.id.deleteIV);
            deleteIV.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dataBase.deleteOrderTempbyRowid(Orders.get(where).getRowid());
                    Orders.remove(where);
                    totalItemTV.setText("Total Items (" + Integer.toString(Orders.size()) + ")");
                    notifyDataSetChanged();
                }
            });
            if (Orders.get(position).getQty() > 0) {
                qtyET.setText(Integer.toString(Orders.get(position).getQty()));
            }
            qtyET.addTextChangedListener(new TextWatcher() {


                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() != 0) {
                        String qtyStr = qtyET.getText().toString();
                        qtyStr = qtyStr.replaceAll("[^0-9]", "");
                        Orders.get(where).setQty(Integer.parseInt(qtyStr));
                    }
                }
            });
            final Spinner pilihSatuanSPN = (Spinner) gridView.findViewById(R.id.pilihSatuanSPN);
            String[] items;
            String idPelanggan = Orders.get(position).getIdPelanggan();
            Cursor levelCsr = dataBase.selectLevelPelanggan(idPelanggan);
            levelCsr.moveToFirst();
            String LevelHarga = levelCsr.getString(0);
            boolean isLevel = false;
            //if (LevelHarga.equals("1") || LevelHarga.equals("6")) {
            if (so.equalsIgnoreCase("so1")) {
                isLevel = true;
                pilihSatuanSPN.setEnabled(true);

                if(Orders.get(position).getSatuanSedang().length()>0)
                {
                    items = new String[]{Orders.get(position).getSatuanSedang()+" ("+(int)Orders.get(position).getIsiSedang()+")"
                            , Orders.get(position).getSatuanBesar()+" ("+(int)Orders.get(position).getIsiBesar()+")"};
//					items = new String[]{Orders.get(position).getSatuan()
//							, Orders.get(position).getSatuanSedang(), Orders.get(position).getSatuanBesar()};
                }
                else
                {
                    items = new String[]{Orders.get(position).getSatuanBesar() + " (" + (int) Orders.get(position).getIsiBesar() + ")"};
                    double isiBesar = Orders.get(position).getIsiBesar();
                    Orders.get(position).setIsi(isiBesar);
                    Orders.get(position).setSatuanTerpilih(Orders.get(position).getSatuanBesar());
                }
            } else if (so.equalsIgnoreCase("pre")) {
                if (LevelHarga.equals("1") || LevelHarga.equals("2")|| LevelHarga.equals("6")) {
                    isLevel = true;
                    pilihSatuanSPN.setEnabled(false);
                    items = new String[]{Orders.get(position).getSatuanBesar() + " (" + (int) Orders.get(position).getIsiBesar() + ")"};
                    double isiBesar = Orders.get(position).getIsiBesar();
                    Orders.get(position).setIsi(isiBesar);
                    Orders.get(position).setSatuanTerpilih(Orders.get(position).getSatuanBesar());
                }else{
                    items = new String[]{Orders.get(position).getSatuan() + " (" + (int) 1 + ")"
                            , Orders.get(position).getSatuanBesar() + " (" + (int) Orders.get(position).getIsiBesar() + ")"};
                }
            }else{
				if(Orders.get(position).getSatuanSedang().length()>0)
				{
					items = new String[]{Orders.get(position).getSatuanSedang()+" ("+(int)Orders.get(position).getIsiSedang()+")"
							, Orders.get(position).getSatuanBesar()+" ("+(int)Orders.get(position).getIsiBesar()+")"};
//					items = new String[]{Orders.get(position).getSatuan()
//							, Orders.get(position).getSatuanSedang(), Orders.get(position).getSatuanBesar()};
				}
				else
				{
                items = new String[]{Orders.get(position).getSatuan() + " (" + (int) 1 + ")"
                        , Orders.get(position).getSatuanBesar() + " (" + (int) Orders.get(position).getIsiBesar() + ")"};
				}
            }
            final boolean isLevelFinal = isLevel;
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
            pilihSatuanSPN.setAdapter(adapter);
            final int adapterCount = adapter.getCount();
            final TextView isiTV = (TextView) gridView.findViewById(R.id.isiTV);
            isiTV.setTypeface(regular);

            if (isLevel == false) {
                String pilihh = Orders.get(position).getSatuanTerpilih();

                if (adapter.getCount() == 3) {
                    String satuanKecil = Orders.get(position).getSatuan();
                    String satuanSedang = Orders.get(position).getSatuanSedang();
                    String satuanBesar = Orders.get(position).getSatuanBesar();
//					if(Orders.get(position).getSatuanTerpilih().equals(satuanKecil))
                    if (pilihh.equals(satuanKecil)) {
                        Orders.get(position).setIsi(1);
                        pilihSatuanSPN.setSelection(0);
                        isiTV.setText("1");
                    } else if (pilihh.equals(satuanSedang))
//					else if(Orders.get(position).getSatuanTerpilih().equals(satuanSedang))
                    {

                        Orders.get(position).setIsi(Orders.get(position).getIsiSedang());
                        pilihSatuanSPN.setSelection(1);
                        isiTV.setText(Integer.toString((int) Orders.get(position).getIsiSedang()));

                    } else if (pilihh.equals(satuanBesar))
//					else if(Orders.get(position).getSatuanTerpilih().equals(satuanBesar))
                    {
                        Orders.get(position).setIsi(Orders.get(position).getIsiBesar());
                        pilihSatuanSPN.setSelection(2);
                        isiTV.setText(Integer.toString((int) Orders.get(position).getIsiBesar()));
                    }
                } else {
                    if (pilihh.equals(Orders.get(position).getSatuan()))
//					if(Orders.get(position).getSatuanTerpilih().equals(Orders.get(position).getSatuan()))
                    {
                        Orders.get(position).setIsi(1);
                        pilihSatuanSPN.setSelection(0);
                        isiTV.setText("1");
                    } else if (pilihh.equals(Orders.get(position).getSatuanBesar()))
//					else if(Orders.get(position).getSatuanTerpilih().equals(Orders.get(position).getSatuanBesar()))
                    {
                        Orders.get(position).setIsi(Orders.get(position).getIsiBesar());
                        pilihSatuanSPN.setSelection(1);
                        isiTV.setText(Integer.toString((int) Orders.get(position).getIsiBesar()));
                    }
                }
            } else {
                Orders.get(position).setIsi(Orders.get(position).getIsiBesar());
                isiTV.setText(Integer.toString((int) Orders.get(position).getIsiBesar()));
            }


            pilihSatuanSPN.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    try {
//						if(isLevelFinal==false)
//						{
                        String hasilClick = pilihSatuanSPN.getSelectedItem().toString();
                        int pos = 0;
                        pos = hasilClick.indexOf(" (");
                        if (pos > 0) {
                            hasilClick = hasilClick.substring(0, pos);
                            hasilClick = hasilClick.trim();
//								Toast.makeText(context,hasilClick,Toast.LENGTH_LONG).show();
                        }
                        Orders.get(where).setSatuanTerpilih(hasilClick);
                        pilihSatuanSPN.setSelection(position);
                        if (hasilClick.equals(Orders.get(where).getSatuan())) {
                            Orders.get(where).setIsi(1);
                            isiTV.setText("1");
                        } else if (hasilClick.equals(Orders.get(where).getSatuanBesar())) {
                            Orders.get(where).setIsi(Orders.get(where).getIsiBesar());
                            isiTV.setText(Integer.toString((int) Orders.get(where).getIsiBesar()));
                        } else if (hasilClick.equals(Orders.get(where).getSatuanSedang())) {
                            Orders.get(where).setIsi(Orders.get(where).getIsiSedang());
                            isiTV.setText(Integer.toString((int) Orders.get(where).getIsiSedang()));
                        }
//							if(pilihSatuanSPN.getSelectedItem().toString().equals(Orders.get(where).getSatuan()))
//							{
//								String satuanTerpilihTemp = Orders.get(where).getSatuan();
//								Orders.get(where).setSatuanTerpilih(satuanTerpilihTemp);
//								pilihSatuanSPN.setSelection(0);
//							}
//							else if(pilihSatuanSPN.getSelectedItem().toString().equals(Orders.get(where).getSatuanSedang()))
//							{
//								String satuanTerpilihTemp = Orders.get(position).getSatuanSedang();
//								Orders.get(where).setSatuanTerpilih(satuanTerpilihTemp);
//								pilihSatuanSPN.setSelection(1);
//							}
//							else if(pilihSatuanSPN.getSelectedItem().toString().equals(Orders.get(position).getSatuanBesar()))
//							{
//								String satuanTerpilihTemp = Orders.get(position).getSatuanBesar();
//								Orders.get(position).setSatuanTerpilih(satuanTerpilihTemp);
//								if(adapterCount==3)
//								{
//									pilihSatuanSPN.setSelection(2);
//								}
//								else
//								{
//									pilihSatuanSPN.setSelection(1);
//								}
//							}
//						}
                    } catch (Exception e) {
                        alert.showAlert(e.toString());
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });


            TextView hargaTV = (TextView) gridView.findViewById(R.id.hargaTV);
            hargaTV.setTypeface(regular);
            hargaTV.setText(formatter.format(Orders.get(position).getHarga()));

            TextView d1TV = (TextView) gridView.findViewById(R.id.d1TV);
            d1TV.setTypeface(regular);
            double d1DBL = Orders.get(where).getD1();
            d1TV.setText(Double.toString(d1DBL));
            TextView d2TV = (TextView) gridView.findViewById(R.id.d2TV);
            d2TV.setTypeface(regular);
            double d2DBL = Orders.get(where).getD2();
            d2TV.setText(Double.toString(d2DBL));

            TextView subtotalTV = (TextView) gridView.findViewById(R.id.subtotalTV);
            subtotalTV.setTypeface(regular);
            if (Orders.get(position).getSubtotal() > 0) {
                subtotalTV.setText(formatter.format(Orders.get(position).getSubtotal()));
            } else {
                subtotalTV.setText("0");
            }
            gridView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
//					File imageFile = new  File(Environment.getExternalStorageDirectory().getPath() +"/Super Retail/"+Orders.get(where).getKode()+".jpg"); 
                    File imageFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + Orders.get(where).getKode() + ".jpg");
                    Bitmap bitmap;
                    if (imageFile.exists()) {
                        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

                    } else {
                        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.nopict);
                        image.setImageResource(R.drawable.nopict);
                    }
                    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                    int width = displayMetrics.widthPixels;
                    int height = (int) (((float) width / (float) bitmap.getWidth()) * (float) bitmap.getHeight());
                    Matrix m = new Matrix();
                    m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
                    Bitmap newBitmapHeadline = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                    image.setImageBitmap(newBitmapHeadline);
                    PhotoViewAttacher mAttacher = new PhotoViewAttacher(image);
                    kodeBarangTV.setText(Orders.get(where).getKode());
                    String id = Orders.get(where).getIdBarang();
                    Cursor detailCsr = dataBase.selectBarangDetail(id);
                    detailCsr.moveToFirst();
                    String Satuan = detailCsr.getString(9);
                    String IsiBesar = detailCsr.getString(13);
                    kuantitiBarangTV.setText(IsiBesar + " " + Satuan);
                    namaBarangTV.setText(Orders.get(where).getNama());
                    double hargaInt = Orders.get(where).getHarga();
                    hargaBarangTV.setText(formatter.format(hargaInt));
                    disk1TV.setText(Double.toString(Orders.get(where).getD1()));
                    disk2TV.setText(Double.toString(Orders.get(where).getD2()));
                }
            });
            qtyET.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
//					File imageFile = new  File(Environment.getExternalStorageDirectory().getPath() +"/Super Retail/"+Orders.get(where).getKode()+".jpg"); 
                    File imageFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + Orders.get(where).getKode() + ".jpg");
                    Bitmap bitmap;
                    if (imageFile.exists()) {
                        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

                    } else {
                        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.nopict);
                    }
                    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                    int width = displayMetrics.widthPixels;
                    int height = (int) (((float) width / (float) bitmap.getWidth()) * (float) bitmap.getHeight());
                    Matrix m = new Matrix();
                    m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
                    Bitmap newBitmapHeadline = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                    image.setImageBitmap(newBitmapHeadline);
                    PhotoViewAttacher mAttacher = new PhotoViewAttacher(image);
                    kodeBarangTV.setText(Orders.get(where).getKode());
                    String id = Orders.get(where).getIdBarang();
                    Cursor detailCsr = dataBase.selectBarangDetail(id);
                    detailCsr.moveToFirst();
                    String Satuan = detailCsr.getString(9);
                    String IsiBesar = detailCsr.getString(13);
                    kuantitiBarangTV.setText(IsiBesar + " " + Satuan);
                    namaBarangTV.setText(Orders.get(where).getNama());
                    double hargaInt = Orders.get(where).getHarga();
                    hargaBarangTV.setText(formatter.format(hargaInt));
                    double d1dbl = Orders.get(where).getD1();
                    double d2dbl = Orders.get(where).getD2();
                    disk1TV.setText(Double.toString(d1dbl));
                    disk2TV.setText(Double.toString(d2dbl));
                    return false;
                }
            });
        } catch (Exception e) {
            alert.showAlert(e.toString());
        }
        return gridView;
    }
}