package com.joan.superretail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ARReminderAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> jatuhTempoList = new ArrayList<String>();
    ArrayList<String> umurList = new ArrayList<String>();
    ArrayList<String> overList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> bayarList = new ArrayList<String>();
    ArrayList<String> potonganList = new ArrayList<String>();
    ArrayList<String> sisaList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;
    double sum;
    ;
    boolean isSumcalc = true;

    public ARReminderAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
                             ArrayList<String> jatuhTempoList, ArrayList<String> umurList, ArrayList<String> overList
            , ArrayList<String> pelangganList, ArrayList<String> totalList, ArrayList<String> bayarList
            , ArrayList<String> potonganList, ArrayList<String> sisaList) {
        context = c;
        this.nomorList = nomorList;
        this.tanggalList = tanggalList;
        this.jatuhTempoList = jatuhTempoList;
        this.umurList = umurList;
        this.overList = overList;
        this.pelangganList = pelangganList;
        this.totalList = totalList;
        this.bayarList = bayarList;
        this.potonganList = potonganList;
        this.sisaList = sisaList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        sum = 0;
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_ar_reminder_detail, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(regular);
        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTV);
        tanggalTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.jatuhTempoTV);
        jatuhTempoTV.setTypeface(regular);
        TextView umurTV = (TextView) gridView.findViewById(R.id.umurTV);
        umurTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);
        TextView overTV = (TextView) gridView.findViewById(R.id.overTV);
        overTV.setTypeface(regular);
        TextView totalTV = (TextView) gridView.findViewById(R.id.totalTV);
        totalTV.setTypeface(regular);
        TextView bayarTV = (TextView) gridView.findViewById(R.id.bayarTV);
        bayarTV.setTypeface(regular);
        TextView potonganTV = (TextView) gridView.findViewById(R.id.potonganTV);
        potonganTV.setTypeface(regular);
        TextView sisaTV = (TextView) gridView.findViewById(R.id.sisaTV);
        sisaTV.setTypeface(regular);
        if (position < nomorList.size()) {

            nomorTV.setText(nomorList.get(position));

            tanggalTV.setText(setDate(tanggalList.get(position)));

            jatuhTempoTV.setText(setDate(jatuhTempoList.get(position)));

            umurTV.setText(umurList.get(position));

            overTV.setText(overList.get(position));

            pelangganTV.setText(pelangganList.get(position));

            double totalDbl = Double.parseDouble(totalList.get(position));
            double totalDblTmp = Math.floor(totalDbl);
            totalTV.setText(formatter.format(totalDblTmp));

            double bayarDbl = Double.parseDouble(bayarList.get(position));
            double bayarDblTmp = Math.floor(bayarDbl);
            bayarTV.setText(formatter.format(bayarDblTmp));

            potonganTV.setText(potonganList.get(position));

            double sisaDbl = Double.parseDouble(sisaList.get(position));
            double sisaDblTmp = Math.floor(sisaDbl);
            if (isSumcalc == true) {
                sum = sum + sisaDblTmp;
            }

            sisaTV.setText(formatter.format(sisaDblTmp));
        } else {
            bayarTV.setTypeface(bold);
            sisaTV.setTypeface(bold);
            isSumcalc = false;
            bayarTV.setText("TOTAL");
            sisaTV.setText(formatter.format(sum));
        }
        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}