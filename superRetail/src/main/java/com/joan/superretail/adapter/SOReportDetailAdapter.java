package com.joan.superretail.adapter;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SOReportDetailAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> quantityKirimList = new ArrayList<String>();
    ArrayList<String> quantitySisaList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> d1List = new ArrayList<String>();
    ArrayList<String> d2List = new ArrayList<String>();
    ArrayList<String> statusList = new ArrayList<String>();
    ArrayList<String> idList = new ArrayList<String>();
    String Total;
    ArrayList<String> subtotalList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public SOReportDetailAdapter(Context c, ArrayList<String> nomorList,
                                 ArrayList<String> kodeList, ArrayList<String> namaList, ArrayList<String> quantityList, ArrayList<String> quantityKirimList
            , ArrayList<String> quantitySisaList, ArrayList<String> satuanList, ArrayList<String> isiList, ArrayList<String> hargaList
            , ArrayList<String> d1List, ArrayList<String> d2List, ArrayList<String> statusList, ArrayList<String> subtotalList, ArrayList<String> idList, String Total) {
        context = c;
        this.nomorList = nomorList;
        this.kodeList = kodeList;
        this.namaList = namaList;
        this.quantityList = quantityList;
        this.satuanList = satuanList;
        this.isiList = isiList;
        this.hargaList = hargaList;
        this.subtotalList = subtotalList;
        this.quantityKirimList = quantityKirimList;
        this.quantitySisaList = quantitySisaList;
        this.d1List = d1List;
        this.d2List = d2List;
        this.statusList = statusList;
        this.idList = idList;
        this.Total = Total;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_so_report_detail, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.kodeTV);
        jatuhTempoTV.setTypeface(regular);
        TextView umurTV = (TextView) gridView.findViewById(R.id.namaTV);
        umurTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.satuanTV);
        pelangganTV.setTypeface(regular);
        TextView totalTV = (TextView) gridView.findViewById(R.id.hargaTV);
        totalTV.setTypeface(regular);
        TextView overTV = (TextView) gridView.findViewById(R.id.quantityTV);
        overTV.setTypeface(regular);
        TextView bayarTV = (TextView) gridView.findViewById(R.id.isiTV);
        bayarTV.setTypeface(regular);
        TextView sisaTV = (TextView) gridView.findViewById(R.id.subtotalTV);
        sisaTV.setTypeface(regular);
        TextView qtySisaTV = (TextView) gridView.findViewById(R.id.quantitySisaTV);
        qtySisaTV.setTypeface(regular);
        TextView d1TV = (TextView) gridView.findViewById(R.id.d1TV);
        d1TV.setTypeface(regular);
        TextView d2TV = (TextView) gridView.findViewById(R.id.d2TV);
        d2TV.setTypeface(regular);
        TextView statusTV = (TextView) gridView.findViewById(R.id.statusTV);
        statusTV.setTypeface(regular);
//				Button btnStatus = (Button)gridView.findViewById(R.id.btnStatusSO);
//				btnStatus.setTypeface(regular);
        final Spinner pilihStatus = (Spinner) gridView.findViewById(R.id.pilihStatus);
        TextView qtyKirimTV = (TextView) gridView.findViewById(R.id.quantityKirimTV);
        qtyKirimTV.setTypeface(regular);
        String[] items;
        items = new String[]{"PENDING", "CANCEL"};
        dataBase = DataBaseManager.instance();

        if (position < nomorList.size()) {
            umurTV.setText(namaList.get(position));
            nomorTV.setText(Integer.toString(position + 1));
            jatuhTempoTV.setText(kodeList.get(position));
            pelangganTV.setText(satuanList.get(position));
            double totalDbl = Double.parseDouble(hargaList.get(position));
            double totalDblTmp = Math.floor(totalDbl);
            totalTV.setText(formatter.format(totalDblTmp));
            double qtyDbl = Double.parseDouble(quantityList.get(position));
            double qtyDblTmp = Math.floor(qtyDbl);
            overTV.setText(formatter.format(qtyDblTmp));
            double isiDbl = Double.parseDouble(isiList.get(position));
            double isiDblTmp = Math.floor(isiDbl);
            bayarTV.setText(formatter.format(isiDblTmp));
            double sisaDbl = Double.parseDouble(subtotalList.get(position));
            double sisaDblTmp = Math.floor(sisaDbl);
            sisaTV.setText(formatter.format(sisaDblTmp));
            double qtySisaDbl = Double.parseDouble(quantitySisaList.get(position));
            double qtySisaDblTmp = Math.floor(qtySisaDbl);
            qtySisaTV.setText(formatter.format(qtySisaDblTmp));
            double qtyKirimDbl = Double.parseDouble(quantityKirimList.get(position));
            double qtyKirimTmp = Math.floor(qtyKirimDbl);
            qtyKirimTV.setText(formatter.format(qtyKirimTmp));
            statusTV.setText(statusList.get(position));
//					btnStatus.setText(statusList.get(position));

            if ((statusList.get(position).equals("DONE")) || (statusList.get(position).equals("CANCEL"))) {
                pilihStatus.setVisibility(View.GONE);
                statusTV.setVisibility(View.VISIBLE);
            } else {
                pilihStatus.setVisibility(View.VISIBLE);
                statusTV.setVisibility(View.GONE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                pilihStatus.setAdapter(adapter);
                // Set initial selection
                pilihStatus.setSelection(position);
                if ((statusList.get(position).equals("PENDING")) || (statusList.get(position).equals(""))) {
                    pilihStatus.setSelection(0);
//							pilihStatus.setBackground(context.getResources().getDrawable(R.drawable.button_green_selector));
                } else {
                    pilihStatus.setSelection(1);
//							pilihStatus.setBackground(context.getResources().getDrawable(R.drawable.button_red_selector));
                }
            }
            double d1Dbl = Double.parseDouble(d1List.get(position));
            double d1DblTmp = Math.floor(d1Dbl);
            d1TV.setText(formatter.format(d1DblTmp));
            double d2Dbl = Double.parseDouble(d2List.get(position));
            double d2DblTmp = Math.floor(d2Dbl);
            d2TV.setText(formatter.format(d2DblTmp));
        } else {
            d2TV.setTypeface(bold);
            sisaTV.setTypeface(bold);
            d2TV.setText("Total");
            double d2Dbl = Double.parseDouble(Total);
            double d2DblTmp = Math.floor(d2Dbl);
            sisaTV.setText(formatter.format(d2DblTmp));
        }


        // Post to avoid initial invocation
        pilihStatus.post(new Runnable() {
            @Override
            public void run() {
                pilihStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        // Only called when the user changes the selection
                        String a = pilihStatus.getSelectedItem().toString();
                        String b = nomorList.get(position).toString();
                        String c = idList.get(position).toString();
//						Toast.makeText(context,a+"#"+b+"#"+c, Toast.LENGTH_SHORT).show();
                        if (a.equals("CANCEL")) {
                            pilihStatus.setBackground(context.getResources().getDrawable(R.drawable.button_red_selector));
                        } else {
                            pilihStatus.setBackground(context.getResources().getDrawable(R.drawable.button_green_selector));
                        }
                        Cursor csr = dataBase.selectUpdateStatusTempByKodeBarang(c);
                        if (csr.getCount() > 0) {
//							csr.moveToFirst();
                            dataBase.updateStatusTemp(a, b, c);
//							Toast.makeText(context,"Update "+a+"#"+b+"#"+c, Toast.LENGTH_SHORT).show();
                        } else {
                            ContentValues values = new ContentValues();
                            values.put("Nomor", b);
                            values.put("KodeBarang", c);
                            values.put("Status", a);
                            dataBase.insert("update_sales_temp", values);
//							Toast.makeText(context,"Insert "+a+"#"+b+"#"+c, Toast.LENGTH_SHORT).show();
                        }
//						dataBase.updateSoStatus(a,b,c);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        });

//		pilihStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//				Toast.makeText(context,String.valueOf(pos),Toast.LENGTH_SHORT).show();
//					dataBase.updateSoStatus(
//							pilihStatus.getSelectedItem().toString(),
//							nomorList.get(position).toString(),
//							kodeList.get(position).toString());
//							"CANCEL",
//							"SO-16030303",
//							"PS317");

//				dataBase.updateSoStatus(
//						"PENDING",
//						nomorList.get(position).toString(),
//						kodeList.get(position).toString());
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//
//			}
//		});

        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}