package com.joan.superretail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joan.superretail.R;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SOReportAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    double sum = 0;
    boolean isCalc = false;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> TanggalTerimaList = new ArrayList<String>();
    ArrayList<String> JatuhTempoList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public SOReportAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> TanggalTerimaList, ArrayList<String> JatuhTempoList,
                           ArrayList<String> pelangganList, ArrayList<String> totalList) {
        context = c;
        this.nomorList = nomorList;
        this.TanggalTerimaList = TanggalTerimaList;
        this.JatuhTempoList = JatuhTempoList;
        this.pelangganList = pelangganList;
        this.totalList = totalList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_so_report, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(bold);

        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTerimaTV);
        tanggalTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.jatuhTempoTV);
        jatuhTempoTV.setTypeface(regular);


        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);

        TextView totalTV = (TextView) gridView.findViewById(R.id.totalTV);
        totalTV.setTypeface(regular);
        if (position < nomorList.size()) {
            pelangganTV.setText(pelangganList.get(position));
            double totalDbl = Double.parseDouble(totalList.get(position));
            double d2DblTmp = Math.floor(totalDbl);
            totalTV.setText(formatter.format(d2DblTmp));
            tanggalTV.setText(setDate(TanggalTerimaList.get(position)));
            nomorTV.setText(nomorList.get(position));
//        			jatuhTempoTV.setText(setDate(JatuhTempoList.get(position)));
            if (isCalc == false) {
                sum = sum + d2DblTmp;
            }

        } else {
            isCalc = true;
            totalTV.setTypeface(bold);
            tanggalTV.setTypeface(bold);
            tanggalTV.setText("Grand Total");
            double d2DblTmp = Math.floor(sum);
            totalTV.setText(formatter.format(d2DblTmp));
        }


        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}