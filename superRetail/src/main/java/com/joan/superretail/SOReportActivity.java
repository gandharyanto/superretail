package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.SOReportAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.util.ArrayList;

public class SOReportActivity extends Activity {

    String query;
    String fromDate;
    String toDate;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> TanggalTerimaList = new ArrayList<String>();
    ArrayList<String> JatuhTempoList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soreport);
        Bundle extras = getIntent().getExtras();
        query = extras.getString("query");
        fromDate = extras.getString("fromDate");
        toDate = extras.getString("toDate");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        Cursor reminderCsr = dataBase.selectSOReport(query, fromDate, toDate);
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            TanggalTerimaList.clear();
//        	JatuhTempoList.clear();
            pelangganList.clear();
            totalList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(2);
//        		String jatuhTempo = reminderCsr.getString(3);
                String pelanggan = reminderCsr.getString(1);
                String total = reminderCsr.getString(3);
                nomorList.add(nomor);
                TanggalTerimaList.add(tanggal);
                pelangganList.add(pelanggan);
                totalList.add(total);
                JatuhTempoList.add("");
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new SOReportAdapter(context, nomorList, TanggalTerimaList, JatuhTempoList, pelangganList, totalList));
            arLV.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if (position < nomorList.size()) {
                        Intent intent = new Intent(getApplicationContext(), SOReportDetailActivity.class);
                        intent.putExtra("Nomor", nomorList.get(position));
                        intent.putExtra("Total", totalList.get(position));
                        intent.putExtra("Pelanggan", pelangganList.get(position));
                        intent.putExtra("query", query);
                        startActivity(intent);
                    }
                }
            });
        }
    }
}
