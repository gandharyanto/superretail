package com.joan.superretail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.joan.superretail.adapter.ARReminderAdapter;
import com.joan.superretail.constant.Constant;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.IPManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.service.DataServices;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ReportMenuActivity extends Activity {

    //	private static final String URL = "http://www.joanriffaldy.esy.es/srdonline";
//	private static final String SYNC_BARANG = URL+"/sync_barang.php";
//	private static final String SYNC_PELANGGAN = URL+"/sync_pelanggan.php";
//	private static final String SYNC_KATEGORI = URL+"/sync_kategori.php";
//	private static final String SYNC_DISCOUNT = URL+"/sync_discount.php";
//	private static final String AR_REMINDER = URL+"/ar_reminder.php";
//	private static final String PICTURE_FOLDER = URL+"/gambar/";
    LinearLayout salesOrderLL, arReminderLL, syncDataLL, syncReportLL, salesReportLL, deliveryReportLL, arDetailLL, soReportLL, returnReportLL, logoutLL, syncPictureLL, cnAvailableLL;
    TextView salesOrderTV, arReminderTV, syncDataTV, syncReportTV, lastSyncPictTV, salesReportTV, syncPictureTV, deliveryReportTV, arDetailTV, soReportTV, returnReportTV, logoutTV, titleArReminderTV, lastSyncDataTV, lastSyncReportTV, passwordTV, setPathTV;
    EditText passwordET;
    boolean isMenuVisible = true;
    Cursor pictCsr;
    int totalImage;
    SimpleDateFormat dateFormat;
    IPManager ipManager;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> jatuhTempoList = new ArrayList<String>();
    ArrayList<String> umurList = new ArrayList<String>();
    ArrayList<String> overList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> bayarList = new ArrayList<String>();
    ArrayList<String> potonganList = new ArrayList<String>();
    ArrayList<String> sisaList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;
    TextView lastSyncArReminder;
    Button btnDetail;
    String idSalesman;
    TextView namaSales, currentSales, salesTarget, achievement;
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_menu);
        dataBase = DataBaseManager.instance();
        context = this;
        ipManager = new IPManager();
        if (Constant.SERVICE) {
            context.startService(new Intent(context, DataServices.class));
        }
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        salesOrderLL = (LinearLayout) findViewById(R.id.salesOrderLL);
        arReminderLL = (LinearLayout) findViewById(R.id.arReminderLL);
        syncDataLL = (LinearLayout) findViewById(R.id.syncDataLL);
        syncPictureLL = (LinearLayout) findViewById(R.id.syncPictureLL);
        syncReportLL = (LinearLayout) findViewById(R.id.syncReportLL);
        salesReportLL = (LinearLayout) findViewById(R.id.salesReportLL);
        deliveryReportLL = (LinearLayout) findViewById(R.id.deliveryReportLL);
        arDetailLL = (LinearLayout) findViewById(R.id.arDetailLL);
        soReportLL = (LinearLayout) findViewById(R.id.soReportLL);
        returnReportLL = (LinearLayout) findViewById(R.id.returnReportLL);
        logoutLL = (LinearLayout) findViewById(R.id.logoutLL);
        cnAvailableLL = (LinearLayout) findViewById(R.id.cnAvailableLL);
        setPathTV = (TextView) findViewById(R.id.setPathTV);
        salesOrderTV = (TextView) findViewById(R.id.salesOrderTV);
        arReminderTV = (TextView) findViewById(R.id.arReminderTV);
        syncDataTV = (TextView) findViewById(R.id.syncDataTV);
        syncPictureTV = (TextView) findViewById(R.id.syncPictureTV);
        syncReportTV = (TextView) findViewById(R.id.syncReportTV);
        salesReportTV = (TextView) findViewById(R.id.salesReportTV);
        deliveryReportTV = (TextView) findViewById(R.id.deliveryReportTV);
        arDetailTV = (TextView) findViewById(R.id.arDetailTV);
        soReportTV = (TextView) findViewById(R.id.soReportTV);
        returnReportTV = (TextView) findViewById(R.id.returnReportTV);
        logoutTV = (TextView) findViewById(R.id.logoutTV);
        titleTV = (TextView) findViewById(R.id.titleTV);
        lastSyncPictTV = (TextView) findViewById(R.id.lastSyncPictTV);
        lastSyncDataTV = (TextView) findViewById(R.id.lastSyncDataTV);
        lastSyncReportTV = (TextView) findViewById(R.id.lastSyncReportTV);
        passwordTV = (TextView) findViewById(R.id.passwordTV);
        passwordET = (EditText) findViewById(R.id.passwordET);
        lastSyncArReminder = (TextView) findViewById(R.id.lastSyncArReminder);
        namaSales = (TextView) findViewById(R.id.reportNamaSalesmanTV);
        currentSales = (TextView) findViewById(R.id.reportCurrentSalesTV);
        salesTarget = (TextView) findViewById(R.id.reportSalesTargetTV);
        achievement = (TextView) findViewById(R.id.reportAchievementTV);

        salesOrderTV.setTypeface(bold);
        setPathTV.setTypeface(bold);
        arReminderTV.setTypeface(bold);
        syncDataTV.setTypeface(bold);
        syncPictureTV.setTypeface(bold);
        syncReportTV.setTypeface(bold);
        salesReportTV.setTypeface(bold);
        deliveryReportTV.setTypeface(bold);
        arDetailTV.setTypeface(bold);
        soReportTV.setTypeface(bold);
        returnReportTV.setTypeface(bold);
        logoutTV.setTypeface(bold);
        titleTV.setTypeface(bold);
        passwordET.setTypeface(bold);
        passwordTV.setTypeface(bold);
        lastSyncPictTV.setTypeface(regular);
        lastSyncDataTV.setTypeface(regular);
        lastSyncReportTV.setTypeface(regular);
        passwordTV.setText("Hide");
        arLV = (ListView) findViewById(R.id.arLV);
        titleArReminderTV = (TextView) findViewById(R.id.titleArRemindeTV);
        btnDetail = (Button) findViewById(R.id.btnDetail);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv10.setTypeface(bold);

        Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
        if (lastSyncCsr.getCount() > 0) {
            lastSyncCsr.moveToFirst();
            String dataSyncTime = lastSyncCsr.getString(0);
            String pictureSyncTime = lastSyncCsr.getString(1);
            String reportSyncTime = lastSyncCsr.getString(2);
            if (dataSyncTime != null && dataSyncTime.length() > 0) {
                lastSyncDataTV.setText("Last Sync : " + dataSyncTime);
            } else {
                lastSyncDataTV.setText("No data sync yet");
            }
            if (pictureSyncTime != null && pictureSyncTime.length() > 0) {
                lastSyncPictTV.setText("Last Sync : " + pictureSyncTime);
            } else {
                lastSyncPictTV.setText("No data sync yet");
            }
            if (reportSyncTime != null && reportSyncTime.length() > 0) {
                lastSyncReportTV.setText("Last Sync : " + reportSyncTime);
                lastSyncArReminder.setText("Last Sync : " + reportSyncTime);
            } else {
                lastSyncReportTV.setText("No data sync yet");
                lastSyncArReminder.setText("No data sync yet");
            }
        } else {
            lastSyncDataTV.setText("No data sync yet");
            lastSyncPictTV.setText("No data sync yet");
            lastSyncReportTV.setText("No data sync yet");
        }

        Cursor reminderCsr = dataBase.selectAllArReminder();
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            jatuhTempoList.clear();
            umurList.clear();
            overList.clear();
            potonganList.clear();
            pelangganList.clear();
            totalList.clear();
            bayarList.clear();
            sisaList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String jatuhTempo = reminderCsr.getString(2);
                String umur = reminderCsr.getString(3);
                String over = reminderCsr.getString(4);
                String pelanggan = reminderCsr.getString(5);
                String total = reminderCsr.getString(6);
                String bayar = reminderCsr.getString(7);
                String potongan = reminderCsr.getString(8);
                String sisa = reminderCsr.getString(9);
                Double.parseDouble(total);
                Double.parseDouble(sisa);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                jatuhTempoList.add(jatuhTempo);
                umurList.add(umur);
                overList.add(over);
                pelangganList.add(pelanggan);
                totalList.add(total);
                bayarList.add(bayar);
                potonganList.add(potongan);
                sisaList.add(sisa);
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new ARReminderAdapter(context, nomorList,
                    tanggalList, jatuhTempoList, umurList, overList,
                    pelangganList, totalList, bayarList, potonganList, sisaList));
        }

        Cursor infoSales = dataBase.selectInfoSalesman();
        if (infoSales.getCount() > 0) {
            infoSales.moveToFirst();
            namaSales.setText(infoSales.getString(0));
            String a = formatter.format(Double.parseDouble(infoSales.getString(1)));
            String b = formatter.format(Double.parseDouble(infoSales.getString(2)));
            a = a.replace(".", ",");
            b = b.replace(".", ",");
            Double c = Double.parseDouble(infoSales.getString(3));
            salesTarget.setText("Sales Target : Rp." + a);
            currentSales.setText("Current Sales : Rp." + b);
            achievement.setText("Achievement % : " + String.format("%.2f", c));
//			achievement.setText("Achievement % : "+infoSales.getString(3));
        } else {
            namaSales.setText("Nama Salesman");
            currentSales.setText("Current Sales :");
            salesTarget.setText("Sales Target :");
            achievement.setText("Achievement % :");
        }

        cnAvailableLL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                idSalesman = csr.getString(3);
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
                builder.setMessage("Apakah anda ingin melakukan sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getCnAvailable(idSalesman);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                intent.putExtra("intentType", "5");
                                startActivity(intent);
                            }
                        }).show();
            }
        });
        salesOrderLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				Intent intent = new Intent(getApplicationContext(), ListPelangganActivity.class);
//				startActivity(intent);
                Intent intent = new Intent(getApplicationContext(), InputPasswordActivity.class);
                startActivityForResult(intent, 11);
            }
        });
        btnDetail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                intent.putExtra("intentType", "1");
                startActivity(intent);
            }
        });
        arReminderLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                intent.putExtra("intentType", "1");
                startActivity(intent);
            }
        });
        syncDataLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.show();
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                String idSalesman = csr.getString(3);
                getPelanggan(idSalesman);
            }
        });
        syncReportLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.show();
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                String idSalesman = csr.getString(3);
                getArReminder(idSalesman);
//				getArDetail(idSalesman);
            }
        });

        syncPictureLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				syncPict();
                downloadFileWithDownloadManager();
            }
        });
        salesReportLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                idSalesman = csr.getString(3);
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
                builder.setMessage("Apakah anda ingin melakukan sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getSalesReport(idSalesman);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                intent.putExtra("intentType", "4");
                                startActivity(intent);
                            }
                        }).show();

            }
        });
        deliveryReportLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                idSalesman = csr.getString(3);
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
                builder.setMessage("Apakah anda ingin melakukan sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getDeliveryReport(idSalesman);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                intent.putExtra("intentType", "3");
                                startActivity(intent);
                            }
                        }).show();

            }
        });
        arDetailLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                idSalesman = csr.getString(3);
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
                builder.setMessage("Apakah anda ingin melakukan sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getArDetail(idSalesman);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), SinglePelangganActivity.class);
                                intent.putExtra("intentType", "2");
                                startActivity(intent);
                            }
                        }).show();
            }
        });
        soReportLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                intent.putExtra("intentType", "2");
                startActivity(intent);
            }
        });
        returnReportLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                idSalesman = csr.getString(3);
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
                builder.setMessage("Apakah anda ingin melakukan sync?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dg,
                                                        int id) {
                                        dialog.show();
                                        getReturnReport(idSalesman);
                                    }
                                })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                Intent intent = new Intent(getApplicationContext(), SinglePelangganActivity.class);
                                intent.putExtra("intentType", "1");
                                startActivity(intent);
                            }
                        }).show();
            }
        });
        logoutLL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dataBase.deleteAccount();
                finish();

            }
        });
        passwordTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (passwordET.getText().toString().length() > 0) {
                    Cursor csr = dataBase.selectAccount();
                    csr.moveToFirst();
                    String pass = csr.getString(1);
                    if (passwordET.getText().toString().equals(pass)) {
                        setVisibility();
                    } else {
                        Toast.makeText(getApplicationContext(), "Wrong password !", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
//        setVisibility();

    }

    public void getKategori() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_KATEGORI);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "kategori null", Toast.LENGTH_SHORT).show();
//										getArReminder();
                                        getJenis();
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "kategori null result", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteKategori();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String id = jsonArray.getJSONObject(i).getString("id");
                                                String Nama = jsonArray.getJSONObject(i).getString("Nama");

                                                ContentValues values = new ContentValues();
                                                values.put("id", id);
                                                values.put("Nama", Nama);
                                                dataBase.insert("kategori", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync kategori success", Toast.LENGTH_SHORT).show();
//   											getArReminder();
                                            getJenis();
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_kategori : " + e.toString());
                                            e.printStackTrace();
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_kategori : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert(e.toString());
                }
            });
        }
    }

    public void getJenis() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_JENIS);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "jenis null", Toast.LENGTH_SHORT).show();
//										getArReminder();
                                        getBarang();
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
//   			    		            	Toast.makeText(getApplicationContext(), "jenis null result", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteJenis();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String id = jsonArray.getJSONObject(i).getString("id");
                                                String Nama = jsonArray.getJSONObject(i).getString("Nama");

                                                ContentValues values = new ContentValues();
                                                values.put("id", id);
                                                values.put("Nama", Nama);
                                                dataBase.insert("jenis", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync jenis success", Toast.LENGTH_SHORT).show();
//   											getArReminder();
                                            getBarang();
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_kategori : " + e.toString());
                                            e.printStackTrace();
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_kategori : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert(e.toString());
                }
            });
        }
    }

    public void getPelanggan(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_PELANGGAN);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "Pelanggan null", Toast.LENGTH_SHORT).show();
                                        getBlacklist();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deletePelanggan();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String id = jsonArray.getJSONObject(i).getString("id");
                                                String idDepartment = jsonArray.getJSONObject(i).getString("idDepartment");
                                                String idArea = jsonArray.getJSONObject(i).getString("idArea");
                                                String Nama = jsonArray.getJSONObject(i).getString("Nama");
                                                String LevelHarga = jsonArray.getJSONObject(i).getString("LevelHarga");
                                                String Alamat = jsonArray.getJSONObject(i).getString("Alamat");
                                                String Telepon1 = jsonArray.getJSONObject(i).getString("Telepon1");
                                                String Telepon2 = jsonArray.getJSONObject(i).getString("Telepon2");
                                                String Tempo = jsonArray.getJSONObject(i).getString("Tempo");
                                                String Aktif = jsonArray.getJSONObject(i).getString("Aktif");
                                                String Kode = jsonArray.getJSONObject(i).getString("Kode");
                                                String Poin = jsonArray.getJSONObject(i).getString("Poin");
                                                String LimitPiutang = jsonArray.getJSONObject(i).getString("LimitPiutang");
                                                String ContactPerson = jsonArray.getJSONObject(i).getString("ContactPerson");
                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String TanggalLahir = jsonArray.getJSONObject(i).getString("TanggalLahir");
                                                String Title = jsonArray.getJSONObject(i).getString("Title");
                                                String TipeFaktur = jsonArray.getJSONObject(i).getString("TipeFaktur");
                                                String idSalesman = jsonArray.getJSONObject(i).getString("idSalesman");
                                                String idGrupPelanggan = jsonArray.getJSONObject(i).getString("idGrupPelanggan");
                                                String Department = jsonArray.getJSONObject(i).getString("Department");
                                                String Area = jsonArray.getJSONObject(i).getString("Area");
                                                String Salesman = jsonArray.getJSONObject(i).getString("Salesman");
                                                String GrupPelanggan = jsonArray.getJSONObject(i).getString("GrupPelanggan");


                                                ContentValues values = new ContentValues();
                                                values.put("id", id);
                                                values.put("idDepartment", idDepartment);
                                                values.put("idArea", idArea);
                                                values.put("Nama", Nama);
                                                values.put("LevelHarga", LevelHarga);
                                                values.put("Alamat", Alamat);
                                                values.put("Telepon1", Telepon1);
                                                values.put("Telepon2", Telepon2);
                                                values.put("Tempo", Tempo);
                                                values.put("Aktif", Aktif);
                                                values.put("Kode", Kode);
                                                values.put("Poin", Poin);
                                                values.put("LimitPiutang", LimitPiutang);
                                                values.put("ContactPerson", ContactPerson);
                                                values.put("Keterangan", Keterangan);
                                                values.put("TanggalLahir", TanggalLahir);
                                                values.put("Title", Title);
                                                values.put("TipeFaktur", TipeFaktur);
                                                values.put("idSalesman", idSalesman);
                                                values.put("idGrupPelanggan", idGrupPelanggan);
                                                values.put("Department", Department);
                                                values.put("Area", Area);
                                                values.put("Salesman", Salesman);
                                                values.put("GrupPelanggan", GrupPelanggan);
                                                dataBase.insert("pelanggan", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync Pelanggan success", Toast.LENGTH_SHORT).show();
                                            getBlacklist();

                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_pelanggan : " + e.toString());
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_pelanggan : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("sync_pelanggan : " + e.toString());
                }
            });
        }
    }

    public void getBlacklist() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_BLACKLIST);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "Blacklist null", Toast.LENGTH_SHORT).show();
                                        getKategori();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteBlacklist();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");


                                                ContentValues values = new ContentValues();
                                                values.put("idBarang", idBarang);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("blacklist", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync Blacklist success", Toast.LENGTH_SHORT).show();
                                            getKategori();

                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_blacklist : " + e.toString());
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_blacklist : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("sync_blacklist : " + e.toString());
                }
            });
        }
    }

    public void getBarang() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_BARANG);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "barang null", Toast.LENGTH_SHORT).show();
                                        getDiscount();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteBarang();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String id = jsonArray.getJSONObject(i).getString("id");
                                                String idKategori = jsonArray.getJSONObject(i).getString("idKategori");
                                                String idJenis = jsonArray.getJSONObject(i).getString("idJenis");
                                                String idMerk = jsonArray.getJSONObject(i).getString("idMerk");
                                                String idGrup = jsonArray.getJSONObject(i).getString("idGrup");
                                                String idPemasok = jsonArray.getJSONObject(i).getString("idPemasok");
                                                String Kode = jsonArray.getJSONObject(i).getString("Kode");
                                                String Nama = jsonArray.getJSONObject(i).getString("Nama");
                                                String Kategori = jsonArray.getJSONObject(i).getString("Kategori");
                                                String Jenis = jsonArray.getJSONObject(i).getString("Jenis");
                                                String Merk = jsonArray.getJSONObject(i).getString("Merk");
                                                String Grup = jsonArray.getJSONObject(i).getString("Grup");
                                                String Pemasok = jsonArray.getJSONObject(i).getString("Pemasok");
                                                String Barcode = jsonArray.getJSONObject(i).getString("Barcode");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String SatuanSedang = jsonArray.getJSONObject(i).getString("SatuanSedang");
                                                String SatuanBesar = jsonArray.getJSONObject(i).getString("SatuanBesar");
                                                double IsiSedang = jsonArray.getJSONObject(i).getDouble("IsiSedang");
                                                double IsiBesar = jsonArray.getJSONObject(i).getDouble("IsiBesar");
                                                String HargaBeli = jsonArray.getJSONObject(i).getString("HargaBeli");
                                                String HargaBeliGross = jsonArray.getJSONObject(i).getString("HargaBeliGross");
                                                String HargaJual1 = jsonArray.getJSONObject(i).getString("HargaJual1");
                                                String HargaJual2 = jsonArray.getJSONObject(i).getString("HargaJual2");
                                                String HargaJual3 = jsonArray.getJSONObject(i).getString("HargaJual3");
                                                String HargaJual4 = jsonArray.getJSONObject(i).getString("HargaJual4");
                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String DateModified = jsonArray.getJSONObject(i).getString("DateModified");
                                                String MinStok = jsonArray.getJSONObject(i).getString("MinStok");
                                                String MaxStok = jsonArray.getJSONObject(i).getString("MaxStok");
                                                double Stok = jsonArray.getJSONObject(i).getDouble("Stok");
                                                String Stok2 = jsonArray.getJSONObject(i).getString("Stok2");
                                                String Custom1 = "";
                                                String Custom2 = "";
                                                String Custom3 = "";
//												String Custom1 = jsonArray.getJSONObject(i).getString("Custom1");
//												String Custom2 = jsonArray.getJSONObject(i).getString("Custom2");
//												String Custom3 = jsonArray.getJSONObject(i).getString("Custom3");
                                                String D1 = jsonArray.getJSONObject(i).getString("D1");
                                                String D2 = jsonArray.getJSONObject(i).getString("D2");
                                                String D3 = jsonArray.getJSONObject(i).getString("D3");
                                                String D4 = jsonArray.getJSONObject(i).getString("D4");
                                                String D5 = jsonArray.getJSONObject(i).getString("D5");
                                                String Tipe = jsonArray.getJSONObject(i).getString("Tipe");
                                                String Berat = jsonArray.getJSONObject(i).getString("Berat");
                                                String UniqueKey = jsonArray.getJSONObject(i).getString("UniqueKey");

//												IsiSedang = IsiSedang.replace(".0000", "");
//												IsiBesar = IsiBesar.replace(".0000", "");
//												Stok = Stok.replace(".0000", "");

                                                ContentValues values = new ContentValues();
                                                values.put("id", id);
                                                values.put("idKategori", idKategori);
                                                values.put("idJenis", idJenis);
                                                values.put("idMerk", idMerk);
                                                values.put("idGrup", idGrup);
                                                values.put("idPemasok", idPemasok);
                                                values.put("Kode", Kode);
                                                values.put("Nama", Nama);
                                                values.put("Kategori", Kategori);
                                                values.put("Jenis", Jenis);
                                                values.put("Merk", Merk);
                                                values.put("Grup", Grup);
                                                values.put("Pemasok", Pemasok);
                                                values.put("Barcode", Barcode);
                                                values.put("Satuan", Satuan);
                                                values.put("SatuanSedang", SatuanSedang);
                                                values.put("SatuanBesar", SatuanBesar);
                                                values.put("IsiSedang", (int) IsiSedang);
                                                values.put("IsiBesar", (int) IsiBesar);
                                                values.put("Stok", (int) Stok);
                                                values.put("HargaBeli", HargaBeli);
                                                values.put("HargaBeliGross", HargaBeliGross);
                                                values.put("HargaJual1", HargaJual1);
                                                values.put("HargaJual2", HargaJual2);
                                                values.put("HargaJual3", HargaJual3);
                                                values.put("HargaJual4", HargaJual4);
                                                values.put("Keterangan", Keterangan);
                                                values.put("DateModified", DateModified);
                                                values.put("MinStok", MinStok);
                                                values.put("MaxStok", MaxStok);

                                                values.put("Stok2", Stok2);
                                                values.put("Custom1", Custom1);
                                                values.put("Custom2", Custom2);
                                                values.put("Custom3", Custom3);
                                                values.put("D1", D1);
                                                values.put("D2", D2);
                                                values.put("D3", D3);
                                                values.put("D4", D4);
                                                values.put("D5", D5);
                                                values.put("Tipe", Tipe);
                                                values.put("Berat", Berat);
                                                values.put("UniqueKey", UniqueKey);
                                                dataBase.insert("barang", values);
                                            }


                                            Toast.makeText(getApplicationContext(), "Sync barang success", Toast.LENGTH_SHORT).show();
                                            getDiscount();
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_barang : " + e.toString());
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_barang : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("sync_barang : " + e.toString());
                }
            });
        }
    }

    public void getDiscount() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_DISCOUNT);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        ContentValues dataValues = new ContentValues();
                                        dataValues.put("data", getDateTime());
                                        Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
                                        if (lastSyncCsr.getCount() > 0) {
                                            lastSyncCsr.moveToFirst();
                                            String dataSyncTime = lastSyncCsr.getString(1);
                                            if (dataSyncTime.length() > 0) {
                                                dataValues.put("picture", dataSyncTime);
                                                lastSyncPictTV.setText("Last Sync : " + dataSyncTime);
                                            } else {
                                                dataValues.put("picture", "");
                                                lastSyncPictTV.setText("No data sync yet");
                                            }

                                        } else {
                                            dataValues.put("picture", "");
                                            lastSyncPictTV.setText("No data sync yet");
                                        }
                                        dataBase.deleteDataSync();
                                        dataBase.insert("data_sync", dataValues);
                                        lastSyncDataTV.setText("Last Sync : " + getDateTime());
                                        Toast.makeText(getApplicationContext(), "discount null", Toast.LENGTH_SHORT).show();
                                        dataBase.pushDatabase(context);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteDiscount();
                                            DecimalFormat precision = new DecimalFormat("0.0");
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
//												String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Diskon1 = jsonArray.getJSONObject(i).getString("Diskon1");
                                                String Diskon2 = jsonArray.getJSONObject(i).getString("Diskon2");
                                                String LevelHarga = jsonArray.getJSONObject(i).getString("LevelHarga");
                                                double d1D = Double.parseDouble(Diskon1);
                                                double d2D = Double.parseDouble(Diskon2);

                                                ContentValues values = new ContentValues();
                                                values.put("idBarang", idBarang);
//												values.put("idPelanggan", idPelanggan);
                                                values.put("LevelHarga", LevelHarga);
                                                values.put("Diskon1", precision.format(d1D));
                                                values.put("Diskon2", precision.format(d2D));
                                                dataBase.insert("discount", values);
                                            }
                                            dialog.dismiss();
                                            ContentValues dataValues = new ContentValues();
                                            dataValues.put("data", getDateTime());
                                            Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
                                            if (lastSyncCsr.getCount() > 0) {
                                                lastSyncCsr.moveToFirst();
                                                String dataSyncTime = lastSyncCsr.getString(1);
                                                String reportSyncTime = lastSyncCsr.getString(2);
                                                if (dataSyncTime.length() > 0) {
                                                    dataValues.put("picture", dataSyncTime);
                                                    lastSyncPictTV.setText("Last Sync : " + dataSyncTime);
                                                } else {
                                                    dataValues.put("picture", "");
                                                    lastSyncPictTV.setText("No data sync yet");
                                                }
                                                if (reportSyncTime.length() > 0) {
                                                    dataValues.put("report", reportSyncTime);
                                                    lastSyncReportTV.setText("Last Sync : " + reportSyncTime);
                                                } else {
                                                    dataValues.put("report", "");
                                                    lastSyncReportTV.setText("No data sync yet");
                                                }

                                            } else {
                                                dataValues.put("picture", "");
                                                lastSyncPictTV.setText("No data sync yet");
                                                dataValues.put("report", "");
                                                lastSyncReportTV.setText("No data sync yet");
                                            }
                                            dataBase.deleteDataSync();
                                            dataBase.insert("data_sync", dataValues);
                                            lastSyncDataTV.setText("Last Sync : " + getDateTime());
                                            Toast.makeText(getApplicationContext(), "Sync discount success", Toast.LENGTH_SHORT).show();
                                            dataBase.pushDatabase(context);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("sync_discount : " + e.toString());
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("sync_discount : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("sync_discount : " + e.toString());
                }
            });
        }
    }

    public void getArReminder(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_ARREMINDER);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//			    		            	dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "ar reminder null", Toast.LENGTH_LONG).show();
                                        getDeliveryReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteARReminder();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String JatuhTempo = "-";
                                                try {
                                                    JatuhTempo = jsonArray.getJSONObject(i).getJSONObject("JatuhTempo").getString("date");
                                                    JatuhTempo = getDate(JatuhTempo);
                                                } catch (Exception e) {
//													alert.showAlert("AR Reminder : "+e.toString());
                                                    JatuhTempo = "-";
                                                }
                                                Tanggal = getDate(Tanggal);
                                                String Umur = jsonArray.getJSONObject(i).getString("Umur");
                                                String Over = jsonArray.getJSONObject(i).getString("Over");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String Bayar = jsonArray.getJSONObject(i).getString("Bayar");
                                                if (Bayar.equals("null")) {
                                                    Bayar = "0";
                                                }
//												String Potongan = jsonArray.getJSONObject(i).getString("Potongan");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Sisa = jsonArray.getJSONObject(i).getString("Sisa");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("JatuhTempo", JatuhTempo);
                                                values.put("Umur", Umur);
                                                values.put("Over", Over);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("Bayar", Bayar);
//												values.put("Potongan", Potongan);
                                                values.put("Potongan", "0");
                                                values.put("Sisa", Sisa);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("ar_reminder", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync ar reminder success", Toast.LENGTH_SHORT).show();
                                            getDeliveryReport(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("AR Reminder : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("AR Reminder : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("AR Reminder : " + e.toString());
                }
            });
        }
    }

    public void getDeliveryReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_DELIVERY_REPORT);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//		    			json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "delivery report null", Toast.LENGTH_LONG).show();
//			    		            	getReturnReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteDeliveryReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String TanggalTerima;
                                                try {
                                                    TanggalTerima = jsonArray.getJSONObject(i).getJSONObject("TanggalTerima").getString("date");
                                                    TanggalTerima = getDate(TanggalTerima);
                                                } catch (Exception e) {
                                                    TanggalTerima = "-";
                                                }
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("TanggalTerima", TanggalTerima);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("delivery_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync delivery report success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                            intent.putExtra("intentType", "3");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Delivery Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Delivery Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Delivery Report : " + e.toString());
                }
            });
        }
    }

    public void getReturnReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_RETURN_REPORT);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//		    			json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "return report null", Toast.LENGTH_LONG).show();
//			    		            	getSOReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteReturnReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String KodeBarang = jsonArray.getJSONObject(i).getString("KodeBarang");
                                                String NamaBarang = jsonArray.getJSONObject(i).getString("NamaBarang");
                                                String Quantity = jsonArray.getJSONObject(i).getString("Quantity");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String Isi = jsonArray.getJSONObject(i).getString("Isi");
                                                String HargaJual = jsonArray.getJSONObject(i).getString("HargaJual");
                                                String Subtotal = jsonArray.getJSONObject(i).getString("Subtotal");

                                                Tanggal = getDate(Tanggal);

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("idBarang", idBarang);
                                                values.put("KodeBarang", KodeBarang);
                                                values.put("NamaBarang", NamaBarang);
                                                values.put("Quantity", Quantity);
                                                values.put("Satuan", Satuan);
                                                values.put("Isi", Isi);
                                                values.put("HargaJual", HargaJual);
                                                values.put("Subtotal", Subtotal);
                                                dataBase.insert("return_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync return report success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), SinglePelangganActivity.class);
                                            intent.putExtra("intentType", "1");
                                            startActivity(intent);
//											getSOReport(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Return Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Return Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Return Report : " + e.toString());
                }
            });
        }
    }

    public void getSOReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SO_REPORT);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//		    			json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "so report null", Toast.LENGTH_LONG).show();
                                        getSalesReport(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSOReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {

                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String TanggalTerima = jsonArray.getJSONObject(i).getJSONObject("TanggalTerima").getString("date");
//												String TanggalFaktur = jsonArray.getJSONObject(i).getJSONObject("TanggalFaktur").getString("date");
                                                String JatuhTempo = jsonArray.getJSONObject(i).getJSONObject("JatuhTempo").getString("date");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");

                                                TanggalTerima = getDate(TanggalTerima);
                                                JatuhTempo = getDate(JatuhTempo);
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("TanggalTerima", TanggalTerima);
//												values.put("TanggalFaktur", "");
                                                values.put("JatuhTempo", JatuhTempo);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("Total", Total);
                                                dataBase.insert("so_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync so report success", Toast.LENGTH_SHORT).show();
                                            getSalesReport(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("SO Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("SO Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("SO Report : " + e.toString());
                }
            });
        }
    }

    public void getSalesReport(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SALES_REPORT);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//   		    			json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//   			    		            	dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "sales report null", Toast.LENGTH_LONG).show();
                                        getSalesReportDetail(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSalesReport();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String Total = jsonArray.getJSONObject(i).getString("Total");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                Tanggal = getDate(Tanggal);
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("Total", Total);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("sales_report", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync sales report success", Toast.LENGTH_SHORT).show();
                                            getSalesReportDetail(idSalesman);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Sales Report : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Sales Report : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Sales Report : " + e.toString());
                }
            });
        }
    }

    public void getSalesReportDetail(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_SALES_REPORT_DETAIL);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
//   			    	    				alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                        Toast.makeText(getApplicationContext(), "sales report detail null", Toast.LENGTH_LONG).show();
//										getArDetail(idSalesman);
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteSalesReportDetail();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String idBarang = jsonArray.getJSONObject(i).getString("idBarang");
                                                String KodeBarang = jsonArray.getJSONObject(i).getString("KodeBarang");
                                                String NamaBarang = jsonArray.getJSONObject(i).getString("NamaBarang");
                                                String Satuan = jsonArray.getJSONObject(i).getString("Satuan");
                                                String Isi = jsonArray.getJSONObject(i).getString("Isi");
                                                String HargaJual = jsonArray.getJSONObject(i).getString("HargaJual");
                                                String Quantity = jsonArray.getJSONObject(i).getString("Quantity");
                                                String Diskon1 = jsonArray.getJSONObject(i).getString("Diskon1");
                                                String Diskon2 = jsonArray.getJSONObject(i).getString("Diskon2");
                                                String Subtotal = jsonArray.getJSONObject(i).getString("Subtotal");
//   												String Status = jsonArray.getJSONObject(i).getString("Status");
//   												String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
//   												String Diskon = jsonArray.getJSONObject(i).getString("Diskon");
//   												String DiskonPersen3 = jsonArray.getJSONObject(i).getString("DiskonPersen3");
//   												String DiskonPersen4 = jsonArray.getJSONObject(i).getString("DiskonPersen4");
//   												String DiskonPersen5 = jsonArray.getJSONObject(i).getString("DiskonPersen5");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("idBarang", idBarang);
                                                values.put("KodeBarang", KodeBarang);
                                                values.put("NamaBarang", NamaBarang);
                                                values.put("Satuan", Satuan);
                                                values.put("Isi", Isi);
                                                values.put("HargaJual", HargaJual);
                                                values.put("Quantity", Quantity);
                                                values.put("Diskon1", Diskon1);
                                                values.put("Diskon2", Diskon2);
                                                values.put("Subtotal", Subtotal);
                                                values.put("Status", "");
                                                values.put("Keterangan", "");
                                                values.put("Diskon", "");
                                                values.put("DiskonPersen3", "");
                                                values.put("DiskonPersen4", "");
                                                values.put("DiskonPersen5", "");
                                                dataBase.insert("sales_report_detail", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync sales report detail success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                            intent.putExtra("intentType", "4");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("Sales Report detail : " + e.toString());
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Sales Report detail : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Sales Report detail : " + e.toString());
                }
            });
        }
    }

    public void getArDetail(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_ARDETAIL);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            final String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        alert.showAlert("AR Detail null result");
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            dataBase.deleteARDetail();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal;
                                                try {
                                                    Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                    Tanggal = getDate(Tanggal);
                                                } catch (Exception e) {
                                                    Tanggal = "-";
                                                }

                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String Debit = jsonArray.getJSONObject(i).getString("Debit");
                                                String Kredit = jsonArray.getJSONObject(i).getString("Kredit");
                                                String Saldo = jsonArray.getJSONObject(i).getString("Saldo");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");

                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Keterangan", Keterangan);
                                                values.put("Debit", Debit);
                                                values.put("Kredit", Kredit);
                                                values.put("Saldo", Saldo);
                                                values.put("idPelanggan", idPelanggan);
                                                dataBase.insert("ar_detail", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "Sync ar detail success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), SinglePelangganActivity.class);
                                            intent.putExtra("intentType", "2");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("AR Detail : " + e.toString() + ", Result :" + arrResult);
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("AR Detail : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }

            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("AR Detail : " + e.toString());
                }
            });
        }
    }

    public void getCnAvailable(final String idSalesman) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SYNC_CN_AVAILABLE);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
//                        json.put("idSalesman", idSalesman);
                        json.put("idSalesman", idSalesman);

                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            final String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "CN Available null result", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONArray jsonArray = new JSONArray(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            dataBase.deleteCnAvailable();
                                            String hasil = "";
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                String Nomor = jsonArray.getJSONObject(i).getString("Nomor");
                                                String Tanggal;
                                                try {
                                                    Tanggal = jsonArray.getJSONObject(i).getJSONObject("Tanggal").getString("date");
                                                    Tanggal = getDate(Tanggal);
                                                } catch (Exception e) {
                                                    Tanggal = "-";
                                                }

                                                String Tipe = jsonArray.getJSONObject(i).getString("Tipe");
                                                String NomorRetur = jsonArray.getJSONObject(i).getString("NomorRetur");
                                                String idPelanggan = jsonArray.getJSONObject(i).getString("idPelanggan");
                                                String Pelanggan = jsonArray.getJSONObject(i).getString("Pelanggan");
                                                String idSalesman = jsonArray.getJSONObject(i).getString("idSalesman");
                                                String Keterangan = jsonArray.getJSONObject(i).getString("Keterangan");
                                                String Jumlah = jsonArray.getJSONObject(i).getString("Jumlah");
                                                String Pakai = jsonArray.getJSONObject(i).getString("Pakai");
                                                String Sisa = jsonArray.getJSONObject(i).getString("Sisa");
                                                hasil = hasil + Jumlah + "," + Pakai + "," + Sisa + "#";
                                                ContentValues values = new ContentValues();
                                                values.put("Nomor", Nomor);
                                                values.put("Tanggal", Tanggal);
                                                values.put("Tipe", Tipe);
                                                values.put("NomorRetur", NomorRetur);
                                                values.put("idPelanggan", idPelanggan);
                                                values.put("Pelanggan", Pelanggan);
                                                values.put("idSalesman", idSalesman);
                                                values.put("Keterangan", Keterangan);
                                                values.put("Jumlah", Jumlah);
                                                values.put("Pakai", Pakai);
                                                values.put("Sisa", Sisa);
                                                dataBase.insert("cn_available", values);
                                            }
                                            Toast.makeText(getApplicationContext(), "CN Available pelanggan success", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), MultiPelangganListActivity.class);
                                            intent.putExtra("intentType", "5");
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            dialog.dismiss();
                                            alert.showAlert("CN Available : " + e.toString() + ", Result :" + arrResult);
                                        }
                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("CN Available : " + e.toString());
                            }
                        });
                    }
                    Looper.loop();
                }

            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("CN Available : " + e.toString());
                }
            });
        }
    }

    private String getDateTime() {
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        String dateStr;
        String monthStr = null;
        if (date < 10) {
            dateStr = "0" + Integer.toString(date);
        } else {
            dateStr = Integer.toString(date);
        }
        if (month < 10) {
            monthStr = "0" + Integer.toString(month);
        } else {
            monthStr = Integer.toString(month);
        }

        String phoneDate = Integer.toString(year) + "-" + monthStr + "-" + dateStr;
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        String hourStr;
        String minuteStr = null;
        if (hour < 10) {
            hourStr = "0" + Integer.toString(hour);
        } else {
            hourStr = Integer.toString(hour);
        }
        if (minute < 10) {
            minuteStr = "0" + Integer.toString(minute);
        } else {
            minuteStr = Integer.toString(minute);
        }
        String timeResult = hourStr + ":" + minuteStr;
        return phoneDate + " " + timeResult;
    }

    private void setVisibility() {
        if (isMenuVisible == true) {
            arReminderLL.setVisibility(LinearLayout.GONE);
            salesReportLL.setVisibility(LinearLayout.GONE);
            deliveryReportLL.setVisibility(LinearLayout.GONE);
            arDetailLL.setVisibility(LinearLayout.GONE);
            soReportLL.setVisibility(LinearLayout.GONE);
            returnReportLL.setVisibility(LinearLayout.GONE);
            syncReportLL.setVisibility(LinearLayout.GONE);
//            Toast.makeText(getApplicationContext(), "Menu Hide !", Toast.LENGTH_LONG).show();
            isMenuVisible = false;
            passwordTV.setText("Show");
            passwordET.getText().clear();
        } else {
            arReminderLL.setVisibility(LinearLayout.VISIBLE);
            salesReportLL.setVisibility(LinearLayout.VISIBLE);
            deliveryReportLL.setVisibility(LinearLayout.VISIBLE);
            arDetailLL.setVisibility(LinearLayout.VISIBLE);
            soReportLL.setVisibility(LinearLayout.VISIBLE);
            returnReportLL.setVisibility(LinearLayout.VISIBLE);
            syncReportLL.setVisibility(LinearLayout.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu Show !", Toast.LENGTH_LONG).show();
            isMenuVisible = true;
            passwordTV.setText("Hide");
            passwordET.getText().clear();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 11) {
            if (resultCode == RESULT_OK) {
//				Intent intentList = new Intent(getApplicationContext(), ListPelangganActivity.class);
//				startActivity(intentList);
                getAccess();
            }
        }
    }

    public void getAccess() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost("http://www.aspirasi.co/srdonline/access.php");
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONObject jsonArray = new JSONObject(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        try {
                                            String allowed = jsonArray.getString("allowed");
                                            if (allowed.equals("true")) {
                                                Intent intentList = new Intent(getApplicationContext(), ListPelangganActivity.class);
                                                startActivity(intentList);
                                            } else {
                                                dialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "You dant have access !!", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }


                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void downloadFile(String url, String dest_file_path) {
        try {
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + "/Super Retail");
            if (!directory.exists())
                directory.mkdir();
            File dest_file = new File(Environment.getExternalStorageDirectory().getPath() + "/Super Retail/" + dest_file_path + ".jpg");
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();
            DataInputStream stream = new DataInputStream(u.openStream());
            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();
            DataOutputStream fos = new DataOutputStream(new FileOutputStream(dest_file));
            fos.write(buffer);
            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {
//            hideProgressIndicator();
            return;
        } catch (IOException e) {
//            hideProgressIndicator();
            return;
        }
    }

    public void saveImage(String imageUrl, String destinationFile) throws IOException {
        try {
            URL u = new URL(imageUrl);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());

            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(destinationFile));
            fos.write(buffer);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            return; // swallow a 404
        } catch (IOException e) {
            return; // swallow a 404
        }
    }

    private void syny_pict(int count) {
        Thread t = new Thread() {
            public void run() {
                Looper.prepare();
                try {

                    while (pictCsr.isAfterLast() == false) {
                        final String kode = pictCsr.getString(0);
                        final String url = Constant.PICTURE_FOLDER + kode + ".jpg";
                        saveImage(url, Environment.getExternalStorageDirectory().getPath() + "/Download/" + kode + ".jpg");
                        pictCsr.moveToNext();
                    }
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            ContentValues dataValues = new ContentValues();
                            dataValues.put("picture", getDateTime());
                            Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
                            if (lastSyncCsr.getCount() > 0) {
                                lastSyncCsr.moveToFirst();
                                String dataSyncTime = lastSyncCsr.getString(0);
                                String reportSyncTime = lastSyncCsr.getString(2);
                                if (dataSyncTime.length() > 0) {
                                    dataValues.put("data", dataSyncTime);
                                    lastSyncDataTV.setText("Last Sync : " + dataSyncTime);
                                } else {
                                    dataValues.put("data", "");
                                    lastSyncDataTV.setText("No data sync yet");
                                }
                                if (reportSyncTime.length() > 0) {
                                    dataValues.put("report", reportSyncTime);
                                    lastSyncReportTV.setText("Last Sync : " + reportSyncTime);
                                } else {
                                    dataValues.put("report", "");
                                    lastSyncReportTV.setText("No data sync yet");
                                }
                            } else {
                                dataValues.put("data", "");
                                lastSyncDataTV.setText("No data sync yet");
                                dataValues.put("report", "");
                                lastSyncReportTV.setText("No data sync yet");
                            }
                            dataBase.deleteDataSync();
                            dataBase.insert("data_sync", dataValues);
                            lastSyncPictTV.setText("Last Sync : " + getDateTime());
                            dialog.dismiss();
                        }
                    });
                } catch (final IOException e) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            dialog.dismiss();
                            alert.showAlert("Sync picture failed : " + e.toString());
                        }
                    });
                }

                Looper.loop();
            }
        };
        t.start();


    }

    @SuppressLint("SimpleDateFormat")
    private String getDate(String dateSource) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            try {
                date = format.parse(dateSource);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dateFormat.format(date);
    }

    /**
     * private void loadBitmapWithPicasso()
     * {
     * Picasso.with(this).load(url).into(new Target() {
     *
     * @Override public void onPrepareLoad(Drawable arg0) {
     * // TODO Auto-generated method stub
     * <p>
     * }
     * @Override public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
     * <p>
     * File file = new File(Environment.getExternalStorageDirectory().getPath() +"/Super Retail/"+kode+".jpg");
     * try
     * {
     * file.createNewFile();
     * FileOutputStream ostream = new FileOutputStream(file);
     * bitmap.compress(CompressFormat.JPEG, 75, ostream);
     * ostream.close();
     * pictCsr.moveToNext();
     * syny_pict(newCount);
     * }
     * catch (Exception e)
     * {
     * pictCsr.moveToNext();
     * syny_pict(newCount);
     * e.printStackTrace();
     * }
     * }
     * @Override public void onBitmapFailed(Drawable arg0) {
     * pictCsr.moveToNext();
     * syny_pict(newCount);
     * }
     * <p>
     * <p>
     * });
     * }
     **/
    private boolean downloadManager(String kode, int current, int sum) {
        boolean result = true;
        try {
            File direct = new File(Environment.getExternalStorageDirectory().getPath()
                    + "/Download");

            if (!direct.exists()) {
                direct.mkdirs();
            }

            DownloadManager mgr = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(Constant.PICTURE_FOLDER + kode + ".jpg");
            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);

            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI
                            | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false).setTitle(kode)
                    .setDescription("Downloading : " + Integer.toString(current) + " / " + Integer.toString(sum))
                    .setDestinationInExternalPublicDir("/Download", kode + ".jpg");

            mgr.enqueue(request);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    private void downloadFileWithDownloadManager() {

        Cursor pictKodeCsr = dataBase.selectPictureKode();
        int totalPictCount = pictKodeCsr.getCount();
        pictKodeCsr.moveToFirst();
        for (int i = 0; i < totalPictCount; i++) {
            String kode = pictKodeCsr.getString(0);
            boolean isDownloadSuccess = downloadManager(kode, i, totalPictCount);
            if (isDownloadSuccess == false) {
                Toast.makeText(getApplicationContext(), kode + ".jpg NOT FOUND", Toast.LENGTH_SHORT).show();
            }
            pictKodeCsr.moveToNext();
        }
        ContentValues dataValues = new ContentValues();
        dataValues.put("picture", getDateTime());
        Cursor lastSyncCsr = dataBase.selectLastSyncDateTime();
        if (lastSyncCsr.getCount() > 0) {
            lastSyncCsr.moveToFirst();
            String dataSyncTime = lastSyncCsr.getString(0);
            String reportSyncTime = lastSyncCsr.getString(2);
            if (dataSyncTime.length() > 0) {
                dataValues.put("data", dataSyncTime);
                lastSyncDataTV.setText("Last Sync : " + dataSyncTime);
            } else {
                dataValues.put("data", "");
                lastSyncDataTV.setText("No data sync yet");
            }
            if (reportSyncTime.length() > 0) {
                dataValues.put("report", reportSyncTime);
                lastSyncReportTV.setText("Last Sync : " + reportSyncTime);
            } else {
                dataValues.put("report", "");
                lastSyncReportTV.setText("No data sync yet");
            }
        } else {
            dataValues.put("data", "");
            lastSyncDataTV.setText("No data sync yet");
            dataValues.put("report", "");
            lastSyncReportTV.setText("No data sync yet");
        }
        dataBase.deleteDataSync();
        dataBase.insert("data_sync", dataValues);
        lastSyncPictTV.setText("Last Sync : " + getDateTime());
    }

    private void syncPict() {
        pictCsr = dataBase.selectPictureKode();
        if (pictCsr.getCount() > 0) {
            dialog.show();
            try {
                totalImage = pictCsr.getCount();
                pictCsr.moveToFirst();
                syny_pict(1);
            } catch (Exception e) {
                alert.showAlert(e.toString());
                dialog.dismiss();
                alert.showAlert(e.toString());
            }

        } else {
            dialog.dismiss();
            alert.showAlert("Tidak ada data barang, mohon sync data terlebih dahulu");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(ReportMenuActivity.this);
            builder.setMessage("Apakah Anda Benar-Benar Ingin Keluar Dashboard Report ?")
                    .setCancelable(false)
                    .setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
//	            	 ReportMenuActivity.this.finish();
                                    Intent intent = new Intent(ReportMenuActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();

                        }
                    }).show();
        }
        return true;
    }
}
