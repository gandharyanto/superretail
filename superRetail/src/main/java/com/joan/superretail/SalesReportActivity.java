package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.SalesReportAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.util.ArrayList;

public class SalesReportActivity extends Activity {

    String query;
    String fromDate;
    String toDate;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv5, fromDateTV, namaTV;
    String grandTotal;
    double sum = 0;
    String isSingle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_report);
        Bundle extras = getIntent().getExtras();
        query = extras.getString("query");
        fromDate = extras.getString("fromDate");
        toDate = extras.getString("toDate");
        isSingle = extras.getString("isSingle");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        fromDateTV = (TextView) findViewById(R.id.fromDateTV);
        namaTV = (TextView) findViewById(R.id.namaTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv5 = (TextView) findViewById(R.id.tv5);
        fromDateTV.setTypeface(medium);
        namaTV.setTypeface(bold);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv5.setTypeface(bold);
        fromDateTV.setText(fromDate + " to " + toDate);
//        Cursor namaCSr = dataBase.selectIdNamaPelangganById(id);
//        namaCSr.moveToFirst();
//        String namaStr = namaCSr.getString(0);
//        namaTV.setText("Pelanggan : "+namaStr);

        Cursor reminderCsr;
//        if(isSingle.equals("1"))
//        {
//        	query = query.replace("'", "");
//        	reminderCsr = dataBase.selectSalesReportSingle(query, fromDate, toDate);
//        }
//        else
//        {
        reminderCsr = dataBase.selectSalesReport(query, fromDate, toDate);
//        }

        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            totalList.clear();
            pelangganList.clear();

            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String total = reminderCsr.getString(2);
                String pelanggan = reminderCsr.getString(3);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                totalList.add(total);
                pelangganList.add(pelanggan);
                sum = sum + Double.parseDouble(total);
                reminderCsr.moveToNext();
            }
            grandTotal = Double.toString(sum);
            arLV.setAdapter(new SalesReportAdapter(context, nomorList, tanggalList, totalList, pelangganList, grandTotal));
            arLV.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if (position < nomorList.size()) {
                        Intent intent = new Intent(getApplicationContext(), SalesReportDetailActivity.class);
                        intent.putExtra("Nomor", nomorList.get(position));
                        intent.putExtra("Total", totalList.get(position));
                        intent.putExtra("Pelanggan", pelangganList.get(position));
                        startActivity(intent);
                    }

                }
            });
        }
    }
}
