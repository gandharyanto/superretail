package com.joan.superretail.receiver;

/**
 * Receiver when boot complete
 * Start SystemService
 *
 * @author Jouhan Riffaldy
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.joan.superretail.constant.Constant;
import com.joan.superretail.service.DataServices;

public class BootReceiver extends BroadcastReceiver {
    final static String TAG = "BootCompletedReceiver";

    @Override
    public void onReceive(Context context, Intent arg1) {
        if (Constant.SERVICE) {
            context.startService(new Intent(context, DataServices.class));
        }
    }
}