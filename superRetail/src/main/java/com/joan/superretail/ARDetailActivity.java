package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.ARDetailAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ARDetailActivity extends Activity {

    String id;
    String fromDate;
    String toDate;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> keteranganList = new ArrayList<String>();
    ArrayList<String> debitList = new ArrayList<String>();
    ArrayList<String> kreditList = new ArrayList<String>();
    ArrayList<String> saldoList = new ArrayList<String>();
    double sumKredit = 0.0;
    double sumDebit = 0.0;
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, fromDateTV, namaTV, kreditTV, debitTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ardetail);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        fromDate = extras.getString("fromDate");
        toDate = extras.getString("toDate");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        fromDateTV = (TextView) findViewById(R.id.fromDateTV);
        namaTV = (TextView) findViewById(R.id.namaTV);
        kreditTV = (TextView) findViewById(R.id.kreditTV);
        debitTV = (TextView) findViewById(R.id.debitTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        fromDateTV.setTypeface(medium);
        namaTV.setTypeface(bold);
        kreditTV.setTypeface(medium);
        debitTV.setTypeface(medium);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        fromDateTV.setText(fromDate + " to " + toDate);
        Cursor namaCSr = dataBase.selectIdNamaPelangganById(id);
        namaCSr.moveToFirst();
        String namaStr = namaCSr.getString(0);
        namaTV.setText("Pelanggan : " + namaStr);

        Cursor reminderCsr = dataBase.selectARDetail(id, fromDate, toDate);
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            keteranganList.clear();
            debitList.clear();
            kreditList.clear();
            saldoList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String keterangan = reminderCsr.getString(2);
                String debit = reminderCsr.getString(3);
                String kredit = reminderCsr.getString(4);
                String saldo = reminderCsr.getString(5);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                keteranganList.add(keterangan);
                debitList.add(debit);
                kreditList.add(kredit);
                saldoList.add(saldo);

                double debitTemp = Double.parseDouble(debit);
                double kreditTemp = Double.parseDouble(kredit);
                sumDebit = sumDebit + debitTemp;
                sumKredit = sumKredit + kreditTemp;

                reminderCsr.moveToNext();
            }
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

            symbols.setGroupingSeparator(',');
            kreditTV.setText("Subtotal Kredit : Rp. " + formatter.format(sumKredit));
            debitTV.setText("Subtotal Debit  : Rp. " + formatter.format(sumDebit));
            arLV.setAdapter(new ARDetailAdapter(context, nomorList, tanggalList, keteranganList, debitList, kreditList, saldoList));
        }
    }
}
