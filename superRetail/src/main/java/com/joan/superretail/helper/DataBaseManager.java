package com.joan.superretail.helper;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.joan.superretail.ApplicationContextProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;


public class DataBaseManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    @SuppressLint("SdCardPath")
    private static String DB_PATH = "/data/data/com.joan.superretail/databases/";
    private static String DB_NAME = "database.sqlite";
    private static SQLiteDatabase mDataBase;
    private static DataBaseManager sInstance = null;

    private DataBaseManager() {
        super(ApplicationContextProvider.getContext(), DB_NAME, null, DATABASE_VERSION);

        try {
            createDataBase();
            openDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static DataBaseManager instance() {
        if (sInstance == null) {
            sInstance = new DataBaseManager();
        }
        return sInstance;
    }

    @SuppressLint("SdCardPath")
    public static void pushDatabase(Context c) {
        String databasePath = "/data/data/com.joan.superretail/databases/database.sqlite";
        File f = new File(databasePath);
        OutputStream myOutput = null;
        InputStream myInput = null;


        if (f.exists()) {
            try {

                File directory = new File("/mnt/sdcard/Super Retail");
                if (!directory.exists())
                    directory.mkdir();

                myOutput = new FileOutputStream(directory.getAbsolutePath()
                        + "/" + "database.sqlite");
                myInput = new FileInputStream(databasePath);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }

                myOutput.flush();
            } catch (Exception e) {
            } finally {
                try {
                    if (myOutput != null) {
                        myOutput.close();
                        myOutput = null;
                    }
                    if (myInput != null) {
                        myInput.close();
                        myInput = null;
                    }
                } catch (Exception e) {
//	                	   errorHandle.write(e.toString());
                }
            }
        }
    }

    public static void exportDB() {
        // TODO Auto-generated method stub

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.joan.superretail"
                        + "//databases//" + "database.sqlite";
                String backupDBPath = "//Download//SUPERRETAIL.txt";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
            }
        } catch (Exception e) {
            //Toast.makeText(act, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void checkAndWriteTable() {
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'ip' ('name' TEXT,'ip' TEXT,'aktif' TEXT)");

        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'account' ('username' TEXT,'password' TEXT,'isLogin' TEXT, 'idSalesman' TEXT, 'NamaSalesman' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'data_sync' ('data' TEXT,'picture' TEXT,'report' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'kategori' ('id' TEXT, 'Nama' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'jenis' ('id' TEXT, 'Nama' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'pelanggan' ('id' TEXT, 'idDepartment' TEXT, 'idArea' TEXT, 'Nama' TEXT, 'LevelHarga' TEXT, 'Alamat' TEXT" +
                ", 'Telepon1' TEXT, 'Telepon2' TEXT, 'Tempo' TEXT, 'Aktif' TEXT, 'Kode' TEXT" +
                ", 'Poin' TEXT, 'LimitPiutang' TEXT, 'ContactPerson' TEXT, 'Keterangan' TEXT, 'TanggalLahir' TEXT" +
                ", 'Title' TEXT, 'idSalesman' TEXT, 'TipeFaktur' TEXT, 'idGrupPelanggan' TEXT, 'Department' TEXT, 'Area' TEXT, 'Salesman' TEXT, 'GrupPelanggan' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'barang' ('id' TEXT, 'idKategori' TEXT, 'idJenis' TEXT, 'idMerk' TEXT" +
                ",'idGrup' TEXT, 'idPemasok' TEXT, 'Kode' TEXT, 'Nama' TEXT,'Kategori' TEXT, 'Jenis' TEXT, 'Merk' TEXT, 'Grup' TEXT" +
                ",'Pemasok' TEXT, 'Barcode' TEXT, 'Satuan' TEXT, 'SatuanSedang' TEXT,'SatuanBesar' TEXT, 'IsiSedang' INTEGER, 'IsiBesar' INTEGER, 'HargaBeli' TEXT" +
                ",'HargaBeliGross' TEXT, 'HargaJual1' TEXT, 'HargaJual2' TEXT, 'HargaJual3' TEXT,'HargaJual4' TEXT, 'HargaJual5' TEXT, 'Keterangan' TEXT, 'DateModified' TEXT" +
                ",'MinStok' TEXT, 'MaxStok' TEXT, 'Stok' INTEGER, 'Stok2' TEXT,'Custom1' TEXT, 'Custom2' TEXT, 'Custom3' TEXT, 'D1' TEXT" +
                ",'D2' TEXT, 'D3' TEXT, 'D4' TEXT, 'D5' TEXT,'Tipe' TEXT, 'Berat' TEXT, 'UniqueKey' TEXT, 'del' TEXT, 'HargaJual6' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'ar_reminder' ('Nomor' TEXT, 'Tanggal' DATETIME" +
                ",'JatuhTempo' DATETIME, 'Umur' TEXT,'Over' TEXT, 'Pelanggan' TEXT,'Total' TEXT, 'Bayar' TEXT" +
                ", 'Potongan' TEXT,'Sisa' TEXT, 'idPelanggan' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'order_temp' ('idSalesman' TEXT,'idPelanggan' TEXT," +
                "'idBarang' TEXT,'Kode' TEXT,'Nama' TEXT,'Harga' TEXT,'SatuanSedang' TEXT,'SatuanBesar' TEXT" +
                ",'IsiSedang' TEXT,'IsiBesar' TEXT,'D1' TEXT,'D2' TEXT,'Satuan' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'discount' ('idBarang' TEXT," +
                "'Diskon1' TEXT,'Diskon2' TEXT,'LevelHarga' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'sales_order' ('idSalesman' TEXT,'idPelanggan' TEXT," +
                "'idBarang' TEXT,'Kode' TEXT,'Nama' TEXT,'Harga' TEXT,'SatuanSedang' TEXT,'SatuanBesar' TEXT" +
                ",'IsiSedang' TEXT,'IsiBesar' TEXT,'D1' TEXT,'D2' TEXT,'Satuan' TEXT" +
                ",'SatuanTerpilih' TEXT,'qty' TEXT,'subtotal' TEXT,'Keterangan' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'pending_sales_order' ('id' TEXT,'status' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'pending_sales_order_detail' ('id' TEXT,'idBarang' TEXT,'idPelanggan' TEXT," +
                "'idSalesman' TEXT,'Quantity' TEXT,'Satuan' TEXT,'Isi' TEXT,'Keterangan' TEXT,'Jenis' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'blacklist' ('idBarang' TEXT,'idPelanggan' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'ar_detail' ('Nomor' TEXT,'idPelanggan' TEXT,'Tanggal' DATETIME" +
                ",'Keterangan' TEXT,'Debit' TEXT,'Kredit' TEXT,'Saldo' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'sales_report' ('Nomor' TEXT,'idPelanggan' TEXT" +
                ",'Pelanggan' TEXT,'Tanggal' DATETIME,'Total' TEXT,'Diskon' TEXT,'Biaya' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'delivery_report' ('Nomor' TEXT,'idPelanggan' TEXT" +
                ",'Pelanggan' TEXT,'TanggalTerima' DATETIME,'Total' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'return_report' ('Nomor' TEXT,'idPelanggan' TEXT" +
                ",'Pelanggan' TEXT,'Tanggal' DATETIME,'Subtotal' TEXT,'idBarang' TEXT,'KodeBarang' TEXT,'NamaBarang' TEXT" +
                ",'Quantity' TEXT,'Satuan' TEXT,'Isi' TEXT,'HargaJual' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'so_report' ('Nomor' TEXT,'idPelanggan' TEXT" +
                ",'Pelanggan' TEXT,'Tanggal' DATETIME,'Total' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'sales_report_detail' ('Nomor' TEXT,'idBarang' TEXT" +
                ",'KodeBarang' TEXT,'NamaBarang' TEXT,'Quantity' TEXT,'Satuan' TEXT,'Isi' TEXT,'HargaJual' TEXT" +
                ",'Diskon1' TEXT,'Diskon2' TEXT,'Subtotal' TEXT,'Status' TEXT,'Keterangan' TEXT,'Diskon' TEXT,'DiskonPersen3' TEXT" +
                ",'DiskonPersen4' TEXT,'DiskonPersen5' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'so_report_detail' ('Nomor' TEXT,'idBarang' TEXT" +
                ",'KodeBarang' TEXT,'NamaBarang' TEXT,'Quantity' TEXT,'QuantityKirim' TEXT,'QuantitySisa' TEXT" +
                ",'Satuan' TEXT,'Isi' TEXT,'HargaJual' TEXT,'Diskon1' TEXT,'Diskon2' TEXT" +
                ",'Subtotal' TEXT,'Status' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'cn_available' ('Nomor' TEXT, 'Tanggal' DATETIME" +
                ",'Tipe' TEXT,'NomorRetur' TEXT,'idPelanggan' TEXT,'Pelanggan' TEXT,'idSalesman' TEXT,'Keterangan' TEXT" +
                ",'Jumlah' TEXT,'Pakai' TEXT,'Sisa' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'sync_temp' ('currentsales' TEXT, 'arout' TEXT, 'totalar' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'jns_order' ('jns_order' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'infosalesman' ('nama' TEXT, 'salestarget' TEXT, 'salesomset' TEXT, 'achievement' TEXT)");
        mDataBase.execSQL("CREATE  TABLE IF NOT EXISTS  'update_sales_temp' ('Nomor' TEXT, 'KodeBarang' TEXT, 'Status' TEXT)");
        sInstance = new DataBaseManager();
    }

    private void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {

        } else {
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    public void copyDataBase() throws IOException {
        InputStream myInput = ApplicationContextProvider.getContext().getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    private void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }

    public Cursor select(String query) throws SQLException {
        return mDataBase.rawQuery(query, null);
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {


    }

    public void insert(String table, ContentValues values) throws SQLException {
        mDataBase.insert(table, null, values);
    }

    // start of account table
    public void updateLoginStatus(String username, String password, String isLogin) throws SQLException {
        mDataBase.execSQL("UPDATE account SET username = '" + username + "'");
        mDataBase.execSQL("UPDATE account SET password = '" + password + "'");
        mDataBase.execSQL("UPDATE account SET isLogin = '" + isLogin + "'");
    }

    public void changePassword(String password) throws SQLException {
        mDataBase.execSQL("UPDATE account SET password = '" + password + "'");
    }

    public Cursor selectAccount() throws SQLException {
        String command = "SELECT username, password, isLogin, idSalesman, NamaSalesman FROM account";
        return mDataBase.rawQuery(command, null);
    }

    public void deleteAccount() throws SQLException {
        mDataBase.execSQL("DELETE FROM account");
    }

    public void deleteUpdateStatusTemp() throws SQLException {
        mDataBase.execSQL("DELETE FROM update_sales_temp");
    }

    public void deleteJnsOrder() throws SQLException {
        mDataBase.execSQL("DELETE FROM jns_order");
    }

    public void deleteSyncTemp() throws SQLException {
        mDataBase.execSQL("DELETE FROM sync_temp");
    }

    public void deleteInfoSalesman() throws SQLException {
        mDataBase.execSQL("DELETE FROM infosalesman");
    }

    public void deletePelanggan() throws SQLException {
        mDataBase.execSQL("DELETE FROM pelanggan");
    }

    public void deleteBlacklist() throws SQLException {
        mDataBase.execSQL("DELETE FROM blacklist");
    }

    public void deleteBarang() throws SQLException {
        mDataBase.execSQL("DELETE FROM barang");
    }

    public void deleteBarang1(String del) throws SQLException {
        mDataBase.execSQL("DELETE FROM barang WHERE del = " + del);
    }

    public void deleteKategori() throws SQLException {
        mDataBase.execSQL("DELETE FROM kategori");
    }

    public void deleteDataSync() throws SQLException {
        mDataBase.execSQL("DELETE FROM data_sync");
    }

    public void deleteARReminder() throws SQLException {
        mDataBase.execSQL("DELETE FROM ar_reminder");
    }

    public void deleteSalesReport() throws SQLException {
        mDataBase.execSQL("DELETE FROM sales_report");
    }

    public void deleteSalesReportDetail() throws SQLException {
        mDataBase.execSQL("DELETE FROM sales_report_detail");
    }

    public void deleteDeliveryReport() throws SQLException {
        mDataBase.execSQL("DELETE FROM delivery_report");
    }

    public void deleteReturnReport() throws SQLException {
        mDataBase.execSQL("DELETE FROM return_report");
    }

    public void deleteARDetail() throws SQLException {
        mDataBase.execSQL("DELETE FROM ar_detail");
    }

    public void deleteCnAvailable() throws SQLException {
        mDataBase.execSQL("DELETE FROM cn_available");
    }

    public void deleteSOReport() throws SQLException {
        mDataBase.execSQL("DELETE FROM so_report");
    }

    public void deleteSOReportDetail() throws SQLException {
        mDataBase.execSQL("DELETE FROM so_report_detail");
    }

    public void deleteOrderTemp() throws SQLException {
        mDataBase.execSQL("DELETE FROM order_temp");
    }

    public void deleteOrderTempbyRowid(String rowid) throws SQLException {
        mDataBase.execSQL("DELETE FROM order_temp WHERE rowid ='" + rowid + "'");
    }

    public void deletePendingSalesOrder(String id) throws SQLException {
        mDataBase.execSQL("DELETE FROM pending_sales_order_detail WHERE id ='" + id + "'");
        mDataBase.execSQL("DELETE FROM pending_sales_order WHERE id ='" + id + "'");
    }

    public void deleteDiscount() throws SQLException {
        mDataBase.execSQL("DELETE FROM discount");
    }

    public void deleteSalesOrder() throws SQLException {
        mDataBase.execSQL("DELETE FROM sales_order");
    }

    public void deleteJenis() throws SQLException {
        mDataBase.execSQL("DELETE FROM jenis");
    }

    public Cursor selectPelangganIdName() throws SQLException {
        String command = "SELECT id, Nama, LevelHarga, Alamat, Telepon1, Telepon2, Aktif FROM pelanggan ORDER BY Nama ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectLastSyncDateTime() throws SQLException {
        String command = "SELECT data, picture,report FROM data_sync";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectUpdateStatusTemp() throws SQLException {
        String command = "SELECT * FROM update_sales_temp";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSyncTemp() throws SQLException {
        String command = "SELECT * FROM sync_temp";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectJnsOrder() throws SQLException {
        String command = "SELECT * FROM jns_order";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategori() throws SQLException {
        String command = "SELECT id, Nama FROM kategori";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectJenis() throws SQLException {
        String command = "SELECT id, Nama FROM jenis";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectInfoSalesman() throws SQLException {
        String command = "SELECT nama, salestarget ,salesomset, achievement FROM infosalesman";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectMerk() throws SQLException {
        String command = "SELECT DISTINCT Merk FROM barang";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectPictureKode() throws SQLException {
        String command = "SELECT Kode FROM barang ORDER BY Kode ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaPelanggan() throws SQLException {
        String command = "SELECT id, Nama FROM pelanggan ORDER BY Nama ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaPelangganById(String id) throws SQLException {
        String command = "SELECT Nama, LevelHarga, Alamat, Telepon1, Telepon2, Aktif FROM pelanggan WHERE id ='" + id + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectUpdateStatusTempByKodeBarang(String id) throws SQLException {
        String command = "SELECT * FROM update_sales_temp WHERE KodeBarang ='" + id + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaBarangByKategori(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT id, Nama, Kode FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "')";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaBarangBy12(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT id, Nama, Kode FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok >= '1') AND del = 0";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaBarangBy3(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT id, Nama, Kode FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND Stok2 >= '1' AND del = 0";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaBarangByPreOrder12(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT id, Nama, Kode FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok = '0') AND (Grup = 'CATALOG-1' OR Grup = 'CATALOG-ALL') AND del = 1";
//                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok < IsiBesar) AND (Grup = 'CATALOG-1' OR Grup = 'CATALOG-ALL') AND del = 1";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIdNamaBarangByPreOrder3(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT id, Nama, Kode FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok2 = '0') AND (Grup = 'CATALOG-2' OR Grup = 'CATALOG-ALL') AND del = 1";
//                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok2 < '1') AND (Grup = 'CATALOG-2' OR Grup = 'CATALOG-ALL') AND del = 1";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategoriCount(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT COUNT(*) FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "')";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategoriCount12(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT COUNT(*) FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok >= '1') AND del = 0";
//                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok >= IsiBesar) AND del = 0";
//				" idBarang FROM blacklist WHERE idPelanggan = '"+idPelanggan+"') AND (Grup = 'CATALOG' OR (Stok >= IsiBesar ))";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategoriCount3(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT COUNT(*) FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND Stok2 >= '1' AND del = 0";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategoriPreOrderCount12(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT COUNT(*) FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok = '0') AND (Grup = 'CATALOG-1' OR Grup = 'CATALOG-ALL') AND del = 1";
//				" idBarang FROM blacklist WHERE idPelanggan = '"+idPelanggan+"') AND (Grup = 'CATALOG' OR (Stok >= IsiBesar ))";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectKategoriPreOrderCount3(String idKategori, String idPelanggan) throws SQLException {
        String command = "SELECT COUNT(*) FROM barang WHERE idJenis = '" + idKategori + "' AND id NOT IN (SELECT" +
                " idBarang FROM blacklist WHERE idPelanggan = '" + idPelanggan + "') AND (Stok = '0') AND (Grup = 'CATALOG-2' OR Grup = 'CATALOG-ALL') AND del = 1";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectOrder() throws SQLException {
        String command = "SELECT idSalesman, idPelanggan, idBarang, Kode, Nama, Satuan, SatuanSedang, SatuanBesar," +
                "IsiSedang, IsiBesar, Harga, D1, D2, rowid FROM order_temp";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSalesOrder() throws SQLException {
        String command = "SELECT idSalesman, idPelanggan, idBarang, Kode, Nama, Satuan, SatuanSedang, SatuanBesar," +
                "IsiSedang, IsiBesar, Harga, D1, D2, SatuanTerpilih,qty,subtotal, Keterangan FROM sales_order";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectBarangDetail(String id) throws SQLException {
        String command = "SELECT id, Nama, Satuan, HargaJual1, HargaJual2, HargaJual3, HargaJual4" +
                ", HargaJual5, Kode, Satuan, SatuanSedang, SatuanBesar, IsiSedang, IsiBesar, HargaBeliGross, HargaJual6 FROM barang WHERE id = '" + id + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectArReminder(String query) throws SQLException {
        String command = "SELECT Nomor, Tanggal, JatuhTempo, Umur, Over, Pelanggan, Total, Bayar" +
                ", Potongan, Sisa FROM ar_reminder WHERE idPelanggan IN(" + query + ") ORDER BY Pelanggan ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectCnAvailable(String query) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Tipe, NomorRetur, Pelanggan, Keterangan, Jumlah" +
                ", Pakai, Sisa, idSalesman FROM cn_available WHERE idPelanggan IN(" + query + ") ORDER BY Pelanggan ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectAllArReminder() throws SQLException {
        String command = "SELECT Nomor, Tanggal, JatuhTempo, Umur, Over, Pelanggan, Total, Bayar" +
                ", Potongan, Sisa FROM ar_reminder ORDER BY JatuhTempo ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectDiscount1(String idBarang, String LevelHarga) throws SQLException {
        String command = "SELECT Diskon1 FROM discount WHERE idBarang = '" + idBarang + "' AND LevelHarga ='" + LevelHarga + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectDiscount2(String idBarang, String LevelHarga) throws SQLException {
        String command = "SELECT Diskon2 FROM discount WHERE idBarang = '" + idBarang + "' AND LevelHarga ='" + LevelHarga + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectPendingSalesOrder() throws SQLException {
        String command = "SELECT id FROM pending_sales_order WHERE status = '0' LIMIT 1";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectLevelPelanggan(String id) throws SQLException {
        String command = "SELECT LevelHarga FROM pelanggan WHERE id = '" + id + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectPendingSalesOrderDetail(String id) throws SQLException {
        String command = "SELECT idSalesman, idPelanggan, idBarang, Quantity, Satuan, Isi, Keterangan, Jenis FROM pending_sales_order_detail WHERE id = '" + id + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSOReport(String query, String fromDate, String toDate) throws SQLException {
//		String command = "SELECT Nomor, Pelanggan, Tanggal, Total FROM so_report WHERE idPelanggan IN("+query+") " +
//				" GROUP BY idPelanggan ORDER BY Tanggal DESC";
        String command = "SELECT Nomor, Pelanggan, Tanggal, Total FROM so_report WHERE idPelanggan IN(" + query + ") " +
                " ORDER BY Tanggal DESC";
        return mDataBase.rawQuery(command, null);
    }

    //	public Cursor selectSOReport(String query, String fromDate, String toDate) throws SQLException
//	{
//		String command = "SELECT Nomor, Pelanggan, TanggalTerima, JatuhTempo, Total FROM so_report WHERE idPelanggan IN("+query+") " +
//				" GROUP BY idPelanggan ORDER BY Nomor ASC";
//	    return mDataBase.rawQuery(command, null);
//	}
    public Cursor selectDeliveryReport(String query, String fromDate, String toDate) throws SQLException {
        String command = "SELECT Nomor, Pelanggan, TanggalTerima, Total FROM delivery_report WHERE idPelanggan IN(" + query + ")" +
                " ORDER BY Nomor DESC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectReturnReport(String id, String fromDate, String toDate) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Kodebarang, NamaBarang, Quantity, Satuan, Isi, HargaJual, Subtotal, SUM(Subtotal), Pelanggan  FROM return_report WHERE idPelanggan = '" + id +
                "' GROUP BY Nomor ORDER BY Tanggal ASC";
        return mDataBase.rawQuery(command, null);
    }

    //	public Cursor selectReturnReport(String id, String fromDate, String toDate) throws SQLException
//	{
//		String command = "SELECT Nomor, Tanggal, Kodebarang, NamaBarang, Quantity, Satuan, Isi, HargaJual, Subtotal FROM return_report WHERE idPelanggan = '"+id+
//				"' AND Tanggal BETWEEN '"+fromDate+"' AND '"+toDate+"' GROUP BY Nomor ORDER BY Tanggal ASC";
//	    return mDataBase.rawQuery(command, null);
//	}
    public Cursor selectReturnReportDetail(String Nomor) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Kodebarang, NamaBarang, Quantity, Satuan, Isi, HargaJual, Subtotal FROM return_report WHERE Nomor ='" + Nomor + "' ORDER BY Tanggal ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSalesReportDetail(String Nomor) throws SQLException {
        String command = "SELECT Nomor, Kodebarang, NamaBarang, Quantity, Satuan, Isi, HargaJual, " +
                "Subtotal FROM sales_report_detail WHERE Nomor ='" + Nomor + "' ORDER BY Nomor ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSOReportDetail(String Nomor) throws SQLException {
        String command = "SELECT Nomor, Kodebarang, NamaBarang, Quantity, QuantityKirim, QuantitySisa, Satuan, Isi, HargaJual, Diskon1," +
                " Diskon2, Status, Subtotal, idBarang FROM so_report_detail WHERE Nomor ='" + Nomor + "' ORDER BY Status DESC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor checkShoppingCart(String idBarang, String idSalesman, String idPelanggan) throws SQLException {
        String command = "SELECT * FROM order_temp WHERE idBarang ='" + idBarang + "' AND idSalesman = '" + idSalesman + "' AND idPelanggan = '" + idPelanggan + "'";
        return mDataBase.rawQuery(command, null);
    }

    //	public Cursor selectARDetail(String idPelanggan, String fromDate, String toDate) throws SQLException
//	{
//		String command = "SELECT Nomor, Tanggal, Keterangan, Debit, Kredit, Saldo FROM ar_detail WHERE idPelanggan ='" +idPelanggan+"'"+
//				"AND Tanggal BETWEEN '"+fromDate+"' AND '"+toDate+"' ORDER BY Tanggal DESC";
//	    return mDataBase.rawQuery(command, null);
//	}
    public Cursor selectARDetail(String idPelanggan, String fromDate, String toDate) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Keterangan, Debit, Kredit, Saldo FROM ar_detail WHERE idPelanggan ='" + idPelanggan + "'" +
                " ORDER BY Tanggal ASC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSalesReport(String query, String fromDate, String toDate) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Total, Pelanggan FROM sales_report WHERE idPelanggan IN(" + query + ") AND " +
                "  Tanggal BETWEEN '" + fromDate + "' AND '" + toDate + "' ORDER BY Tanggal DESC";
//		String command = "SELECT Nomor, Tanggal, Total, Pelanggan FROM sales_report WHERE idPelanggan = "+query+""+
//				" AND "+
//				"  Tanggal BETWEEN '"+fromDate+"' AND '"+toDate+"' ORDER BY Tanggal DESC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor countSalesReport(String query, String fromDate, String toDate) throws SQLException {
        String command = "SELECT SUM(Total) FROM sales_report WHERE idPelanggan IN(" + query + ") AND " +
                "  Tanggal BETWEEN '" + fromDate + "' AND '" + toDate + "' ORDER BY Tanggal DESC";
//		String command = "SELECT COUNT(*) FROM sales_report WHERE idPelanggan IN("+query+") AND "+
//				"  Tanggal BETWEEN '"+fromDate+"' AND '"+"2016-03-27"+"' ORDER BY Tanggal DESC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSalesReportSingle(String query, String fromDate, String toDate) throws SQLException {
        String command = "SELECT Nomor, Tanggal, Total, Pelanggan FROM sales_report WHERE idPelanggan = '" + query + "' AND " +
                "  Tanggal BETWEEN '" + fromDate + "' AND '" + toDate + "' ORDER BY Tanggal DESC";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectSalesReportBiayaDisk(String Nomor) throws SQLException {
        String command = "SELECT Biaya, Diskon FROM sales_report WHERE Nomor = '" + Nomor + "'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIP() throws SQLException {
        String command = "SELECT ip FROM ip WHERE aktif = '1'";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor selectIPList() throws SQLException {
        String command = "SELECT name, ip, aktif FROM ip";
        return mDataBase.rawQuery(command, null);
    }

    public Cursor updateIP(String ip) throws SQLException {
        String command = "UPDATE ip SET ip = '" + ip + "'";
        return mDataBase.rawQuery(command, null);
    }

    public void updateSoStatus(String status, String Nomor, String idBarang) throws SQLException {
        String command = "UPDATE so_report_detail SET Status = '" + status + "' " +
                "WHERE Nomor = '" + Nomor + "' AND idBarang = '" + idBarang + "'";
        mDataBase.execSQL(command);
    }

    public void updateStatusTemp(String status, String Nomor, String idBarang) throws SQLException {
        String command = "UPDATE update_sales_temp SET Status = '" + status + "' " +
                "WHERE Nomor = '" + Nomor + "' AND KodeBarang = '" + idBarang + "'";
        mDataBase.execSQL(command);
    }

    public void resetSettingIP(String destName) throws SQLException {
        String command = "UPDATE ip SET aktif = '0' WHERE aktif = '1'";
        mDataBase.execSQL(command);
    }

    public void setIP(String destName) throws SQLException {
        String command1 = "UPDATE ip SET aktif = '1' WHERE name = '" + destName + "'";
        mDataBase.execSQL(command1);
    }

    public void deleteIP() throws SQLException {
        mDataBase.execSQL("DELETE FROM ip");
    }

    public void deleteIP(String name) throws SQLException {
        mDataBase.execSQL("DELETE FROM ip WHERE name = '" + name + "'");
    }
//	public Cursor selectSalesReport(String idPelanggan, String fromDate, String toDate) throws SQLException
//	{
//		String command = "SELECT Nomor, Tanggal, Total FROM sales_report WHERE idPelanggan ='" +idPelanggan+"'"+
//				"AND Tanggal BETWEEN '"+fromDate+"' AND '"+toDate+"' ORDER BY Tanggal DESC";
//	    return mDataBase.rawQuery(command, null);
//	}

}