package com.joan.superretail.helper;

import android.content.Context;
import android.graphics.Typeface;

public class FontManager {

    Context context;
    Typeface font;

    public FontManager(Context context) {
        this.context = context;
    }

    public Typeface getMediumTypeface() {
        font = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-Medium.ttf");
        return font;

    }

    public Typeface getBoldTypeface() {
        font = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-BoldCondensed.ttf");
        return font;

    }

    public Typeface getRegularTypeface() {
        font = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-Regular.ttf");
        return font;

    }
}
