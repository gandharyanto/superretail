package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.ReturnReportDetailAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import java.util.ArrayList;

public class ReturnReportDetailActivity extends Activity {

    String Nomor;
    String fromDate;
    String toDate;
    String Pelanggan;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> subtotalList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_report_detail);
        Bundle extras = getIntent().getExtras();
        Nomor = extras.getString("Nomor");
        Pelanggan = extras.getString("Pelanggan");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv10.setTypeface(bold);
        titleTV.setText("Return Report | " + Nomor + " | " + Pelanggan);
        Cursor reminderCsr = dataBase.selectReturnReportDetail(Nomor);
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            kodeList.clear();
            namaList.clear();
            quantityList.clear();
            satuanList.clear();
            isiList.clear();
            hargaList.clear();
            subtotalList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String kode = reminderCsr.getString(2);
                String nama = reminderCsr.getString(3);
                String quantity = reminderCsr.getString(4);
                String satuan = reminderCsr.getString(5);
                String isi = reminderCsr.getString(6);
                String harga = reminderCsr.getString(7);
                String subtotal = reminderCsr.getString(8);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                kodeList.add(kode);
                namaList.add(nama);
                quantityList.add(quantity);
                satuanList.add(satuan);
                isiList.add(isi);
                hargaList.add(harga);
                subtotalList.add(subtotal);
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new ReturnReportDetailAdapter(context, nomorList, tanggalList, kodeList, namaList, quantityList, satuanList, isiList, hargaList, subtotalList));
        }
    }
}
