package com.joan.superretail;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.joan.superretail.adapter.SettingAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.IPManager;
import com.joan.superretail.model.Settings;

import java.util.ArrayList;

public class ChangeIpActivity extends Activity {

    DataBaseManager dataBase;
    IPManager ipManager;
    TextView passwordTV;
    EditText passwordET, nameET;
    ListView settingLV;
    String ip;
    Context c;
    AlertDialogManager alert;
    ArrayList<Settings> settingList = new ArrayList<Settings>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_change_ip);
        dataBase = DataBaseManager.instance();
        settingLV = (ListView) findViewById(R.id.settingLV);
        c = this;
        ipManager = new IPManager();
        ip = ipManager.getIP();
        alert = new AlertDialogManager(this);
        passwordTV = (TextView) findViewById(R.id.passwordTV);
        nameET = (EditText) findViewById(R.id.nameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
//		passwordET.setText(ip);
        initList();
        passwordTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (passwordET.getText().toString().length() > 0 && nameET.getText().toString().length() > 0) {
                    //		dataBase.deleteIP();
                    String name = nameET.getText().toString();
                    String ip = passwordET.getText().toString();
                    dataBase.resetSettingIP(name);
                    ContentValues values = new ContentValues();
                    values.put("name", name);
                    values.put("ip", ip);
                    values.put("aktif", "1");
                    dataBase.insert("ip", values);

                    initList();
                    Toast.makeText(getApplicationContext(), "IP changed success !", Toast.LENGTH_LONG).show();
//					dataBase.updateIP(passwordET.getText().toString());
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Please fill profile name and internet protocol !", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initList() {
        settingList.clear();
        Cursor settingCsr = dataBase.selectIPList();
        if (settingCsr.getCount() > 0) {
            settingCsr.moveToFirst();
            while (settingCsr.isAfterLast() == false) {
                String name = settingCsr.getString(0);
                String ip = settingCsr.getString(1);
                String aktif = settingCsr.getString(2);
//				Settings sett = new Settings(name, ip, aktif);
                Settings sett = new Settings(name, ip, aktif);
                settingList.add(sett);
                settingCsr.moveToNext();
            }
            settingLV.setAdapter(new SettingAdapter(c, settingList));

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

}
