package com.joan.superretail;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.joan.superretail.adapter.CheckOutAdapter;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;
import com.joan.superretail.model.Orders;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

public class CheckOutActivity extends Activity {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, totalTV, calculateTV, totalItemTV,
            kodeTV, kuantitiTV, d1Title, d2Title, namaTV, hargaTV, d1TV, d2TV, previewTV;
    ArrayList<Orders> Orders = new ArrayList<Orders>();
    CheckOutAdapter adapter;
    ImageView image;
    EditText keteranganET;
    boolean isCalculate = false;
    String JnsOrder;
    String so;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        Bundle extras = getIntent().getExtras();
        so = extras.getString("so");
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        final DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        keteranganET = (EditText) findViewById(R.id.keteranganET);
        symbols.setGroupingSeparator('.');

        titleTV = (TextView) findViewById(R.id.titleTV);
//		View newview =  LayoutInflater.from(this).inflate(
//	            R.layout.checkout_header, null, false); 

        kodeTV = (TextView) findViewById(R.id.kodeTV);
        kuantitiTV = (TextView) findViewById(R.id.kuantitiTV);
        d1Title = (TextView) findViewById(R.id.d1Title);
        d2Title = (TextView) findViewById(R.id.d2Title);
        namaTV = (TextView) findViewById(R.id.namaTV);
        hargaTV = (TextView) findViewById(R.id.hargaTV);
        d1TV = (TextView) findViewById(R.id.d1TV);
        d2TV = (TextView) findViewById(R.id.d2TV);
        previewTV = (TextView) findViewById(R.id.previewTV);

        totalItemTV = (TextView) findViewById(R.id.totalItemTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        totalTV = (TextView) findViewById(R.id.totalTV);
        titleTV.setTypeface(medium);
        previewTV.setTypeface(medium);
        tv10 = (TextView) findViewById(R.id.tv10);
        tv11 = (TextView) findViewById(R.id.tv11);
        tv12 = (TextView) findViewById(R.id.tv12);
        image = (ImageView) findViewById(R.id.barangIV);
        tv10.setTypeface(medium);
        kodeTV.setTypeface(bold);
        kuantitiTV.setTypeface(bold);
        d1Title.setTypeface(bold);
        d2Title.setTypeface(bold);
        namaTV.setTypeface(bold);
        hargaTV.setTypeface(bold);
        d1TV.setTypeface(bold);
        d2TV.setTypeface(bold);
        calculateTV = (TextView) findViewById(R.id.calculateTV);
        calculateTV.setTypeface(medium);
        totalTV.setTypeface(bold);
        totalItemTV.setTypeface(bold);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv11.setTypeface(bold);
        tv12.setTypeface(bold);
        Cursor csr = dataBase.selectJnsOrder();
        if (csr.getCount() > 0) {
            csr.moveToFirst();
            JnsOrder = csr.getString(0);
        }
        try {
            Cursor checkoutCsr = dataBase.selectOrder();
            if (checkoutCsr.getCount() > 0) {
                totalItemTV.setText("Total Items (" + Integer.toString(checkoutCsr.getCount()) + ")");
                checkoutCsr.moveToFirst();
                Orders.clear();

                while (checkoutCsr.isAfterLast() == false) {
                    String idSalesman = checkoutCsr.getString(0);
                    String idPelanggan = checkoutCsr.getString(1);
                    String idBarang = checkoutCsr.getString(2);
                    String Kode = checkoutCsr.getString(3);
                    String Nama = checkoutCsr.getString(4);
                    String Satuan = checkoutCsr.getString(5);
                    String SatuanSedang = checkoutCsr.getString(6);
                    String SatuanBesar = checkoutCsr.getString(7);
                    String IsiSedangStr = checkoutCsr.getString(8);
                    String IsiBesarStr = checkoutCsr.getString(9);
                    String HargaStr = checkoutCsr.getString(10);
//                	int IsiSedang = Integer.parseInt(IsiSedangStr);
//                	int IsiBesar = Integer.parseInt(IsiBesarStr);
//                	int Harga = Integer.parseInt(HargaStr);
//                	int D1 = Integer.parseInt(checkoutCsr.getString(11).replaceAll("[^\\d]", ""));
//                	int D2 = Integer.parseInt(checkoutCsr.getString(12).replaceAll("[^\\d]", ""));
                    double IsiSedang = 0;
                    if (IsiSedangStr.length() > 0) {
                        IsiSedang = Double.parseDouble(IsiSedangStr);
                    }
                    double IsiBesar = Double.parseDouble(IsiBesarStr);
                    double Harga = Double.parseDouble(HargaStr);
//                	double D1 = Double.parseDouble(checkoutCsr.getString(11).replaceAll("[^\\d]", ""));
//                	double D2 = Double.parseDouble(checkoutCsr.getString(12).replaceAll("[^\\d]", ""));

                    double D1 = Double.parseDouble(checkoutCsr.getString(11).replaceAll(",", "."));
                    double D2 = Double.parseDouble(checkoutCsr.getString(12).replaceAll(",", "."));
                    String rowid = checkoutCsr.getString(13);
                    Orders order = new Orders(idBarang, idPelanggan, idSalesman, Kode, Nama, Harga, Satuan
                            , SatuanSedang, SatuanBesar, IsiSedang, IsiBesar, 0, 0, D1, D2, 1, Satuan, rowid);
//                	int sub = Integer.parseInt(Harga.replaceAll("[^\\d]", ""))*Integer.parseInt(Isi.replaceAll("[^\\d]", ""))*Integer.parseInt(Quantity.replaceAll("[^\\d]", ""));
//                	sum = sum + sub;
//                	subtotalList.add(Integer.toString(sub));
                    Orders.add(order);
                    checkoutCsr.moveToNext();
                }
//                arLV.addHeaderView(newview, null, false);
                adapter = new CheckOutAdapter(context, Orders, image, totalItemTV, kodeTV, kuantitiTV, namaTV
                        , hargaTV, d1TV, d2TV, so);
                arLV.setAdapter(adapter);
//                arLV.setOnItemClickListener(new OnItemClickListener() {
                //
//    				@Override
//    				public void onItemClick(AdapterView<?> parent, View view,
//    						int position, long id) {
//    					setImage(position-1);
//    				}
//    			});
//                arLV.setOnItemSelectedListener(new OnItemSelectedListener() {
                //
//    				@Override
//    				public void onItemSelected(AdapterView<?> parent, View view,
//    						int position, long id) {
//    					setImage(position-1);
//    				}
                //
//    				@Override
//    				public void onNothingSelected(AdapterView<?> parent) {
//    					// TODO Auto-generated method stub
//    					
//    				}
//    			});
//                arLV.setOnTouchListener(new OnTouchListener() {
//    				
//    				@Override
//    				public boolean onTouch(View v, MotionEvent event) {
//    					int pos = v.getId();
//    					setImage(pos-1);
//    					return false;
//    				}
//    			});
                setImage(0);
                totalTV.setText("Grand Total : --");
                titleTV.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        GPSTracker gpsTracker = new GPSTracker(CheckOutActivity.this);
                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                            String keterangan = "";
                            String idPelanggan = "";
                            if (isCalculate == true) {
                                dataBase.deleteSalesOrder();
                                if (keteranganET.getText().toString().length() > 0) {
                                    keterangan = keteranganET.getText().toString();
                                }
                                String idPendingSalesOrder = getId();
                                boolean isOK = false;
                                for (int i = 0; i < Orders.size(); i++) {
                                    Orders order = Orders.get(i);
                                    int qty = order.getQty();
                                    if (qty > 0) {
                                        isOK = true;
                                        ContentValues values = new ContentValues();
                                        values.put("idSalesman", order.getIdSalesman());
                                        values.put("idPelanggan", order.getIdPelanggan());
                                        values.put("idBarang", order.getIdBarang());
                                        values.put("Harga", Double.toString(order.getHarga()));
                                        values.put("Kode", order.getKode());
                                        values.put("Satuan", order.getSatuan());
                                        values.put("Nama", order.getNama());
                                        values.put("SatuanSedang", order.getSatuanSedang());
                                        values.put("SatuanBesar", order.getSatuanBesar());
                                        values.put("IsiSedang", Double.toString(order.getIsiSedang()));
                                        values.put("IsiBesar", Double.toString(order.getIsiBesar()));
                                        values.put("D1", Double.toString(order.getD1()));
                                        values.put("D2", Double.toString(order.getD2()));
                                        values.put("SatuanTerpilih", order.getSatuanTerpilih());
                                        values.put("qty", Integer.toString(qty));
                                        double subTemp = order.getSubtotal();
                                        values.put("subtotal", Double.toString(subTemp));
                                        values.put("Keterangan", keterangan);
                                        dataBase.insert("sales_order", values);

                                        ContentValues values1 = new ContentValues();
                                        values1.put("id", idPendingSalesOrder);
                                        values1.put("idBarang", order.getIdBarang());
                                        values1.put("idPelanggan", order.getIdPelanggan());
                                        values1.put("idSalesman", order.getIdSalesman());
                                        values1.put("Quantity", Integer.toString(qty));
                                        values1.put("Satuan", order.getSatuanTerpilih());
                                        values1.put("Keterangan", keterangan);
                                        values1.put("Jenis", JnsOrder);
                                        idPelanggan = order.getIdPelanggan();
                                        double isi = 1;
                                        if (order.getSatuanTerpilih().equals(order.getSatuanSedang())) {
                                            isi = order.getIsiSedang();
                                        } else if (order.getSatuanTerpilih().equals(order.getSatuanBesar())) {
                                            isi = order.getIsiBesar();
                                        }
                                        values1.put("Isi", formatter.format(isi));
                                        dataBase.insert("pending_sales_order_detail", values1);

                                    }

                                }
                                if (isOK == true) {
//									Toast.makeText(getApplicationContext(),idPelanggan,Toast.LENGTH_LONG).show();
                                    ContentValues values = new ContentValues();
                                    values.put("id", idPendingSalesOrder);
                                    values.put("status", "0");
                                    dataBase.insert("pending_sales_order", values);
                                    Intent intent = new Intent(getApplicationContext(), InvoiceActivity.class);
                                    intent.putExtra("Keterangan", keterangan);
                                    intent.putExtra("idPelanggan", idPelanggan);
                                    startActivityForResult(intent, 1);
                                }
                            } else {
                                alert.showAlert("Mohon calculate terlebih dahulu !");
                            }
                        } else {
                            gpsTracker.showSettingsAlert();
                        }

                    }
                });
                previewTV.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String keterangan = "";

                        if (isCalculate == true) {
                            dataBase.deleteSalesOrder();
                            if (keteranganET.getText().toString().length() > 0) {
                                keterangan = keteranganET.getText().toString();
                            }
                            String idPendingSalesOrder = getId();
                            dataBase.deleteSalesOrder();
                            boolean isOK = false;
                            for (int i = 0; i < Orders.size(); i++) {
                                Orders order = Orders.get(i);
                                int qty = order.getQty();
                                if (qty > 0) {
                                    isOK = true;
                                    ContentValues values = new ContentValues();
                                    values.put("idSalesman", order.getIdSalesman());
                                    values.put("idPelanggan", order.getIdPelanggan());
                                    values.put("idBarang", order.getIdBarang());
                                    values.put("Harga", Double.toString(order.getHarga()));
                                    values.put("Kode", order.getKode());
                                    values.put("Satuan", order.getSatuan());
                                    values.put("Nama", order.getNama());
                                    values.put("SatuanSedang", order.getSatuanSedang());
                                    values.put("SatuanBesar", order.getSatuanBesar());
                                    values.put("IsiSedang", Double.toString(order.getIsiSedang()));
                                    values.put("IsiBesar", Double.toString(order.getIsiBesar()));
                                    values.put("D1", Double.toString(order.getD1()));
                                    values.put("D2", Double.toString(order.getD2()));
                                    values.put("SatuanTerpilih", order.getSatuanTerpilih());
                                    values.put("qty", Integer.toString(qty));
                                    double subTemp = order.getSubtotal();
                                    values.put("subtotal", Double.toString(subTemp));
                                    values.put("Keterangan", keterangan);
                                    dataBase.insert("sales_order", values);
                                }

                            }
                            if (isOK == true) {
                                Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
                                intent.putExtra("Keterangan", keterangan);
                                startActivityForResult(intent, 1);
                            }
                        } else {
                            alert.showAlert("Mohon calculate terlebih dahulu !");
                        }
                    }
                });
                calculateTV.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        isCalculate = true;
                        double sum = 0;
                        for (int i = 0; i < Orders.size(); i++) {
                            int qty = Orders.get(i).getQty();
                            if (qty > 0) {
                                double tempPrice = 0;
                                String satuanterpilih = Orders.get(i).getSatuanTerpilih();
                                String satuanbesar = Orders.get(i).getSatuanBesar();
                                if (satuanterpilih.equals(satuanbesar)) {
                                    tempPrice = qty * Orders.get(i).getIsi() * Orders.get(i).getHarga();
                                    if (Orders.get(i).getD1() > 0) {
                                        tempPrice = tempPrice - (((Orders.get(i).getD1() * Orders.get(i).getHarga()) / 100) * qty * Orders.get(i).getIsi());
                                    }
                                    if (Orders.get(i).getD2() > 0) {
                                        double hargaTemp = tempPrice;
                                        tempPrice = tempPrice - ((Orders.get(i).getD2() / 100) * hargaTemp);
                                    }
                                    Orders.get(i).setSubtotal(tempPrice);
                                } else {
                                    tempPrice = qty * Orders.get(i).getIsi() * Orders.get(i).getHarga();
                                    if (Orders.get(i).getD1() > 0) {
                                        tempPrice = tempPrice - (((Orders.get(i).getD1() * Orders.get(i).getHarga()) / 100) * qty * Orders.get(i).getIsi());
                                    }
                                    Orders.get(i).setSubtotal(tempPrice);
                                }
                            } else {
                                Orders.get(i).setSubtotal(0);
                            }
                            sum = sum + Orders.get(i).getSubtotal();
                        }
                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

                        symbols.setGroupingSeparator(',');
                        adapter.notifyDataSetChanged();
                        totalTV.setText("Rp. " + formatter.format(sum));
                    }
                });
            }
        } catch (Exception e) {
            alert.showAlert(e.toString());
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    private void setImage(int position) {
//		File imageFile = new  File(Environment.getExternalStorageDirectory().getPath() +"/Super Retail/"+Orders.get(position).getKode()+".jpg"); 
        File imageFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + Orders.get(position).getKode() + ".jpg");
        Bitmap bitmap;
        if (imageFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

        } else {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.nopict);
//	    	image.setImageResource(R.drawable.nopict);
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = (int) (((float) width / (float) bitmap.getWidth()) * (float) bitmap.getHeight());
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
        Bitmap newBitmapHeadline = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
        image.setImageBitmap(newBitmapHeadline);
        PhotoViewAttacher mAttacher = new PhotoViewAttacher(image);
        mAttacher.setAllowParentInterceptOnEdge(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(CheckOutActivity.this);
            builder.setMessage("Apakah Anda Ingin Kembali Ke Halaman Sebelumnya ?")
                    .setCancelable(false)
                    .setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    CheckOutActivity.this.finish();
                                }
                            })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();

                        }
                    }).show();
        }
        return true;
    }


    private String getId() {
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.MILLISECOND);

        return Integer.toString(year) + Integer.toString(month) + Integer.toString(date) + Integer.toString(hour)
                + Integer.toString(minute) + Integer.toString(second);
    }
}
