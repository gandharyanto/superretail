package com.joan.superretail;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.ProgressDialogManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CardFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    private static final String ARG_ID = "idBarang";
    private static final String ARG_LEVEL = "levelHarga";
    private static final String ARG_ID_PELANGGAN = "idPelanggan";
    private static final String ARG_STATUS = "status";
    private static final String ARG_TOTAL_ITEM_ID = "totalItemID";
    private static final String URL = "http://www.joanriffaldy.esy.es/srdonline";
    private static final String GET_SALES_PRICE = URL + "/get_sales_price.php";
    final int duration = 300;
    boolean status;
    Typeface bold, regular, medium;
    FontManager fm;
    List<String> idList = new ArrayList<String>();
    List<String> nameList = new ArrayList<String>();
    List<String> urlList = new ArrayList<String>();
    List<String> hashList = new ArrayList<String>();
    DataBaseManager dataBase;
    double sizeDouble = 0.8;
    float size = (float) sizeDouble;
    int totalItemID;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    private int position;
    private String idBarang;
    private String idPelanggan;
    private String levelHarga;

    public static CardFragment newInstance(int position, String idBarang, String levelHarga, String idPelanggan,
                                           boolean status, int totalItemID) {
        CardFragment f = new CardFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        b.putString(ARG_ID, idBarang);
        b.putString(ARG_ID_PELANGGAN, idPelanggan);
        b.putString(ARG_LEVEL, levelHarga);
        b.putBoolean(ARG_STATUS, status);
        b.putInt(ARG_TOTAL_ITEM_ID, totalItemID);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt(ARG_POSITION);
        idBarang = getArguments().getString(ARG_ID);
        idPelanggan = getArguments().getString(ARG_ID_PELANGGAN);
        levelHarga = getArguments().getString(ARG_LEVEL);
        status = getArguments().getBoolean(ARG_STATUS);
        totalItemID = getArguments().getInt(ARG_TOTAL_ITEM_ID);
        fm = new FontManager(getActivity());
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        alert = new AlertDialogManager(getActivity());
        dialog = new ProgressDialogManager(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//		final int where = position;
        View gridView = inflater.inflate(R.layout.detail_barang, null);
        dataBase = DataBaseManager.instance();
        try {
            LinearLayout barangLL = (LinearLayout) gridView.findViewById(R.id.barangLL);
            ImageView image = (ImageView) gridView.findViewById(R.id.barangIV);
            TextView kodeTV = (TextView) gridView.findViewById(R.id.kodeTV);
            TextView namaTV = (TextView) gridView.findViewById(R.id.namaTV);
            final TextView kuantitiTV = (TextView) gridView.findViewById(R.id.kuantitiTV);
            TextView hargaTV = (TextView) gridView.findViewById(R.id.hargaTV);
            final TextView orderTV = (TextView) gridView.findViewById(R.id.orderTV);
            TextView d1Title = (TextView) gridView.findViewById(R.id.d1Title);
            TextView d2Title = (TextView) gridView.findViewById(R.id.d2Title);
            TextView d1TV = (TextView) gridView.findViewById(R.id.d1TV);
            TextView d2TV = (TextView) gridView.findViewById(R.id.d2TV);

            Cursor accs = dataBase.selectAccount();
            accs.moveToFirst();
            String idSalesman = accs.getString(3);
            Cursor checkBarang = dataBase.checkShoppingCart(idBarang, idSalesman, idPelanggan);
            if (checkBarang.getCount() > 0) {
                status = true;
            }
            Cursor detailCsr = dataBase.selectBarangDetail(idBarang);
            kodeTV.setTypeface(bold);
            namaTV.setTypeface(bold);
            kuantitiTV.setTypeface(medium);
            hargaTV.setTypeface(medium);
            orderTV.setTypeface(medium);
            d1Title.setTypeface(medium);
            d2Title.setTypeface(medium);
            d1TV.setTypeface(medium);
            d2TV.setTypeface(medium);

            detailCsr.moveToFirst();
            String id = detailCsr.getString(0);
            final String nama = detailCsr.getString(1);
            final String satuan = detailCsr.getString(2);
            String HargaJual1 = detailCsr.getString(3);
            String HargaJual2 = detailCsr.getString(4);
            String HargaJual3 = detailCsr.getString(5);
            String HargaJual4 = detailCsr.getString(6);
//			String HargaJual5 = detailCsr.getString(7);
            final String Kode = detailCsr.getString(8);
            final String Satuan = detailCsr.getString(9);
            final String SatuanSedang = detailCsr.getString(10);
            final String SatuanBesar = detailCsr.getString(11);
            final String IsiSedang = detailCsr.getString(12);
            final String IsiBesar = detailCsr.getString(13);
            String HargaJual6 = detailCsr.getString(14);
//			String[] items = new String[]{Satuan, SatuanSedang, SatuanBesar};
//			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, items);
//			pilihSatuanSPN.setAdapter(adapter);
//			File imageFile = new  File(Environment.getExternalStorageDirectory().getPath() +"/Super Retail/"+Kode+".jpg"); 
            File imageFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + Kode + ".jpg");
            Bitmap bitmap;
            if (imageFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            } else {
                bitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.nopict);
//		    	image.setImageResource(R.drawable.nopict);
            }
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = (int) (((float) width / (float) bitmap.getWidth()) * (float) bitmap.getHeight());
            Matrix m = new Matrix();
            m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
            Bitmap newBitmapHeadline = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
            image.setImageBitmap(newBitmapHeadline);
//			PhotoViewAttacher mAttacher = new PhotoViewAttacher(image);
//	    	mAttacher.setAllowParentInterceptOnEdge(false);
            kodeTV.setText(Kode);
            namaTV.setText(nama);
            String harga = null;

            if (levelHarga.equals("1")) {
                harga = HargaJual1;
            } else if (levelHarga.equals("2")) {
                harga = HargaJual2;
            } else if (levelHarga.equals("3")) {
                harga = HargaJual3;
            } else if (levelHarga.equals("4")) {
                harga = HargaJual4;
            } else if (levelHarga.equals("6")) {
                harga = HargaJual6;
            }
//		    else if(levelHarga.equals("5"))
//		    {
//		    	harga = HargaJual5;
//		    }
            final String hargaString = harga;
            String disc1 = "0";
            String disc2 = "0";
            Cursor d1Csr = dataBase.selectDiscount1(idBarang, levelHarga);
//		    double disDouble1 = 0;
//		    double disDouble2 = 0;
            if (d1Csr.getCount() > 0) {
                d1Csr.moveToFirst();
                disc1 = d1Csr.getString(0);
//		    	disDouble1 = Double.parseDouble(disc1);


            }
            Cursor d2Csr = dataBase.selectDiscount2(idBarang, levelHarga);
            if (d2Csr.getCount() > 0) {
                d2Csr.moveToFirst();
                disc2 = d2Csr.getString(0);
//		    	if(!levelHarga.equals("1")&&!levelHarga.equals("2"))
//		    	{
//		    		disc2 = d2Csr.getString(0);
//		    	}
//		    	disDouble2 = Double.parseDouble(disc2);
            }
//		    final String D1 = Integer.toString((int)disDouble1);
//		    final String D2 = Integer.toString((int)disDouble2);
            final String D1 = disc1;
            final String D2 = disc2;
            d1TV.setText(disc1 + "%");
            d2TV.setText(disc2 + "%");
//		    int hargaInt = Integer.parseInt(harga.replaceAll("[^\\d]", ""));
            harga = harga.substring(0, harga.length() - 5);
            int hargaInt = Integer.parseInt(harga);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            final TextView totalItemTV = (TextView) getActivity().findViewById(R.id.totalItemTV);
            symbols.setGroupingSeparator(',');
            hargaTV.setText(formatter.format(hargaInt));
//		    hargaTV.setText(harga);
            kuantitiTV.setText(IsiBesar + " " + satuan);
            if (status == true) {
                orderTV.setBackgroundResource(R.drawable.button_orange_normal);
                orderTV.setEnabled(false);
            } else {
                orderTV.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        orderTV.setBackgroundResource(R.drawable.button_orange_normal);
                        Cursor acc = dataBase.selectAccount();
                        acc.moveToFirst();
                        String idSalesman = acc.getString(3);
                        ContentValues values = new ContentValues();
                        values.put("idSalesman", idSalesman);
                        values.put("idPelanggan", idPelanggan);
                        values.put("idBarang", idBarang);
                        values.put("Harga", hargaString);
                        values.put("Kode", Kode);
                        values.put("Satuan", Satuan);
                        values.put("Nama", nama);
                        values.put("SatuanSedang", SatuanSedang);
                        values.put("SatuanBesar", SatuanBesar);
                        values.put("IsiSedang", IsiSedang);
                        values.put("IsiBesar", IsiBesar);
                        values.put("D1", D1);
                        values.put("D2", D2);
                        dataBase.insert("order_temp", values);
                        orderTV.setEnabled(false);
                        status = true;
                        Cursor orderCSr = dataBase.selectOrder();
                        if (orderCSr.getCount() > 0) {
                            int totalOrderCount = orderCSr.getCount();
                            totalItemTV.setText("Total Items (" + Integer.toString(totalOrderCount) + ")");
                        }
                    }
                });

            }


        } catch (Exception e) {
            alert.showAlert(e.toString());
        }
        return gridView;
    }

    public void checkDiscount(final String idPelanggan, final String idBarang, final String Quantity, final String Satuan, final String Isi) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(GET_SALES_PRICE);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idPelanggan", idPelanggan);
                        json.put("idBarang", idBarang);
                        json.put("Quantity", Quantity);
                        json.put("Satuan", Satuan);
                        json.put("Isi", Isi);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("")) {
                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                    }
                                });
                            } else {
                                final JSONObject jsonArray = new JSONObject(arrResult);
                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        dialog.dismiss();

                                        try {
                                            String harga = jsonArray.getString("HargaJual");
                                            alert.showAlert("Harga : " + harga);
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert(e.toString() + e.getMessage());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert(e.toString());
                }
            });
        }
    }

}