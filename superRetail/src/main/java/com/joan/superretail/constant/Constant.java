package com.joan.superretail.constant;

public class Constant {

    public static final boolean SERVICE = true;
    //	public  String URL = "http://www.joanriffaldy.esy.es/srdonline";
//	public  String BASE_URL = "http://36.80.54.74:8080";
    public static final String BASE_URL = "";
    //	public static final String URL = BASE_URL+"/srdonline";
    public static final String URL = BASE_URL;//"/srdonline";

    public static final String SYNC_BARANG = URL + "/sync_barang.php";
    public static final String SYNC_BARANG_V3 = URL + "/sync_barang_v3.php";
    public static final String SYNC_BLACKLIST = URL + "/sync_blacklist.php";
    public static final String SYNC_PELANGGAN = URL + "/sync_pelanggan.php";
    public static final String SYNC_KATEGORI = URL + "/sync_kategori.php";
    public static final String SEND_LOG = URL + "/send_location.php";
    public static final String SYNC_JENIS = URL + "/sync_jenis.php";
    public static final String SYNC_DISCOUNT = URL + "/sync_discount.php";
    public static final String SYNC_ARREMINDER = URL + "/sync_arreminder.php";
    public static final String SYNC_INFOSALESMAN = URL + "/salesman_info.php";
    public static final String SYNC_ARDETAIL = URL + "/ar_detail.php";
    public static final String SYNC_ARDETAIL_PELANGGAN = URL + "/ar_detail_pelanggan.php";
    public static final String SYNC_SALES_REPORT = URL + "/sales_report.php";
    public static final String SYNC_SALES_REPORT_PELANGGAN = URL + "/sales_report_pelanggan.php";
    public static final String SYNC_SALES_REPORT_DETAIL = URL + "/sales_detail_report.php";
    public static final String SYNC_SALES_REPORT_DETAIL_PELANGGAN = URL + "/sales_detail_report_pelanggan.php";
    public static final String SYNC_DELIVERY_REPORT = URL + "/delivery_report.php";
    public static final String SYNC_DELIVERY_REPORT_PELANGGAN = URL + "/delivery_report_pelanggan.php";
    public static final String SYNC_SO_REPORT = URL + "/so_report.php";
    public static final String SYNC_SO_REPORT_PELANGGAN = URL + "/so_report_pelanggan.php";
    public static final String SYNC_SO_REPORT_DETAIL = URL + "/so_detail_report.php";
    public static final String SYNC_SO_REPORT_DETAIL_PELANGGAN = URL + "/so_detail_report_pelanggan.php";
    public static final String SYNC_RETURN_REPORT = URL + "/return_report.php";
    public static final String SYNC_RETURN_REPORT_PELANGGAN = URL + "/return_report_pelanggan.php";
    public static final String SYNC_CN_AVAILABLE = URL + "/sync_cn.php";
    public static final String SYNC_CN_AVAILABLE_PELANGGAN = URL + "/sync_cn_pelanggan.php";
    public static final String UPDATE_STATUS_ORDER = URL + "/update_status_sales_order.php";
    public static final String AR_REMINDER = URL + "/ar_reminder.php";
    public static final String AR_REMINDER_PELANGGAN = URL + "/ar_reminder_pelanggan.php";
    public static final String PICTURE_FOLDER = BASE_URL + "/gambar/";
    //	public  String PICTURE_FOLDER = "http://www.joanriffaldy.esy.es/gambar/";
    public static final String DATA_SERVICE = URL + "/input_sales_order.php";
    public static final String DATA_SERVICE_PRE = URL + "/input_pre_order.php";
    public static final String LOGIN_URL = URL + "/user_login.php";
    public static final String INTENT_FILTER_TAG = "com.joan.superretail.service";

    public static final int SECOND = 1000;
    //	public static final int PERIODIC_TIME =1*SECOND;
    public static final int PERIODIC_TIME = 20 * SECOND;
    public static final int UPDATE_PROGRESS = 8344;
    public static final int NOTIFY_ME_ID = 1337;

    public static final String NEW_BASE_URL = "http://megatrend.co.id";
    public static final String NEW_SYNC_BARANG = URL + "/backend/public/api/android/syncproduct/0";
    public static final String NEW_SYNC_BARANGV2 = URL + "/backend/public/api/android/syncproductv2/0";
    public static final String NEW_DATA_SERVICE = URL + "/backend/public/api/android/insertsalesorder/0";
    public static final String NEW_DATA_SERVICE_PRE = URL + "/backend/public/api/android/insertpresalesorder/0";

}
