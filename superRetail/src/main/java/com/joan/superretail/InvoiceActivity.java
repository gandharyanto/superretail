package com.joan.superretail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joan.superretail.constant.Constant;
import com.joan.superretail.helper.AlertDialogManager;
import com.joan.superretail.helper.DataBaseManager;
import com.joan.superretail.helper.FontManager;
import com.joan.superretail.helper.IPManager;
import com.joan.superretail.helper.ProgressDialogManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

public class InvoiceActivity extends Activity {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    LinearLayout baseLL;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tanggalTV, namaCustomerTV, namaSalesmanTV,
            finishTV, grandTotalTV, keteranganTV;
    String keteranganStr, idPelanggan;
    double sum = 0;
    IPManager ipManager;
    double myLatitude;
    double myLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        dataBase = DataBaseManager.instance();
        Bundle extras = getIntent().getExtras();
        keteranganStr = extras.getString("Keterangan");
        idPelanggan = extras.getString("idPelanggan");
        ipManager = new IPManager();
        GPSTracker gpsTracker = new GPSTracker(this);
        myLatitude = gpsTracker.latitude;
        myLongitude = gpsTracker.longitude;
//		Toast.makeText(getApplicationContext(),idPelanggan,Toast.LENGTH_LONG).show();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tanggalTV = (TextView) findViewById(R.id.tanggalTV);
        namaCustomerTV = (TextView) findViewById(R.id.namaCustomerTV);
        namaSalesmanTV = (TextView) findViewById(R.id.namaSalesmanTV);
        grandTotalTV = (TextView) findViewById(R.id.grandTotalTV);
        finishTV = (TextView) findViewById(R.id.finishTV);
        keteranganTV = (TextView) findViewById(R.id.keteranganTV);
        baseLL = (LinearLayout) findViewById(R.id.baseLL);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        titleTV.setTypeface(bold);
        grandTotalTV.setTypeface(bold);
        tanggalTV.setTypeface(medium);
        namaCustomerTV.setTypeface(medium);
        namaSalesmanTV.setTypeface(medium);
        finishTV.setTypeface(medium);
        tv1.setTypeface(regular);
        tv2.setTypeface(regular);
        tv3.setTypeface(regular);
        tv4.setTypeface(regular);
        tv5.setTypeface(regular);
        tv6.setTypeface(regular);
        tv7.setTypeface(regular);
        tv8.setTypeface(regular);
        tv9.setTypeface(regular);
        tv10.setTypeface(regular);
        Cursor invoiceCsr = dataBase.selectSalesOrder();
        if (invoiceCsr.getCount() > 0) {
            invoiceCsr.moveToFirst();
            boolean first = false;
            int counter = 1;
            while (invoiceCsr.isAfterLast() == false) {
                invoiceCsr.getString(0);
                String idPelanggan = invoiceCsr.getString(1);
                invoiceCsr.getString(2);
                String Kode = invoiceCsr.getString(3);
                String Nama = invoiceCsr.getString(4);
                String Satuan = invoiceCsr.getString(5);
                String SatuanSedang = invoiceCsr.getString(6);
                String SatuanBesar = invoiceCsr.getString(7);
                String IsiSedangStr = invoiceCsr.getString(8);
                String IsiBesarStr = invoiceCsr.getString(9);
                String HargaStr = invoiceCsr.getString(10);
                String D1Str = invoiceCsr.getString(11);
                String D2Str = invoiceCsr.getString(12);
//            	double IsiSedang = Integer.parseInt(invoiceCsr.getString(8).replaceAll("[^\\d]", ""));
//            	double IsiBesar = Integer.parseInt(invoiceCsr.getString(9).replaceAll("[^\\d]", ""));
//            	double Harga = Integer.parseInt(invoiceCsr.getString(10).replaceAll("[^\\d]", ""));
//            	double D1 = Integer.parseInt(invoiceCsr.getString(11).replaceAll("[^\\d]", ""));
//            	double D2 = Integer.parseInt(invoiceCsr.getString(12).replaceAll("[^\\d]", ""));
                double IsiSedang = 0;
                if (IsiSedangStr.length() > 0) {
                    IsiSedang = Double.parseDouble(IsiSedangStr);
                }
                double IsiBesar = Double.parseDouble(IsiBesarStr);
                double Harga = Double.parseDouble(HargaStr);
                double D1 = Double.parseDouble(D1Str);
                double D2 = Double.parseDouble(D2Str);
                String SatuanTerpilih = invoiceCsr.getString(13);
                String qty = invoiceCsr.getString(14);
                String subtotalTemp = invoiceCsr.getString(15);
                double subtotal = Double.parseDouble(subtotalTemp);
                invoiceCsr.getString(16);
                if (first == false) {
                    first = true;
                    Cursor accountCsr = dataBase.selectAccount();
                    accountCsr.moveToFirst();
                    String namaSalesman = accountCsr.getString(4);
                    namaSalesmanTV.setText("Salesman : " + namaSalesman);
                    Cursor pelangganCsr = dataBase.selectIdNamaPelangganById(idPelanggan);
                    pelangganCsr.moveToFirst();
                    String namaPelanggan = pelangganCsr.getString(0);
                    namaCustomerTV.setText("Nama Customer : " + namaPelanggan);
                    tanggalTV.setText("Tanggal : " + getTodayDate());
                    keteranganTV.setText("Keterangan :\n" + keteranganStr);
                }
                View view = LayoutInflater.from(this).inflate(
                        R.layout.model_invoice, null, false);

                TextView noTV = (TextView) view.findViewById(R.id.noTV);
                noTV.setTypeface(regular);
                noTV.setText(Integer.toString(counter));
                TextView kodeTV = (TextView) view.findViewById(R.id.kodeTV);
                kodeTV.setTypeface(regular);
                kodeTV.setText(Kode);
                TextView namaTV = (TextView) view.findViewById(R.id.namaTV);
                namaTV.setTypeface(regular);
                namaTV.setText(Nama);
                TextView qtyTV = (TextView) view.findViewById(R.id.qtyTV);
                qtyTV.setTypeface(regular);
                qtyTV.setText(qty);
                TextView satuanTV = (TextView) view.findViewById(R.id.satuanTV);
                satuanTV.setTypeface(regular);
                satuanTV.setText(SatuanTerpilih);
                TextView isiTV = (TextView) view.findViewById(R.id.isiTV);
                isiTV.setTypeface(regular);
                if (SatuanTerpilih.equals(Satuan)) {
                    isiTV.setText("1");
                } else if (SatuanTerpilih.equals(SatuanSedang)) {
                    isiTV.setText(Integer.toString((int) IsiSedang));
                } else if (SatuanTerpilih.equals(SatuanBesar)) {
                    isiTV.setText(Integer.toString((int) IsiBesar));
                }
                TextView hargaTV = (TextView) view.findViewById(R.id.hargaTV);
                hargaTV.setTypeface(regular);
                hargaTV.setText(formatter.format(Harga));
                TextView d1TV = (TextView) view.findViewById(R.id.d1TV);
                d1TV.setTypeface(regular);
                d1TV.setText(Double.toString(D1));
                TextView d2TV = (TextView) view.findViewById(R.id.d2TV);
                d2TV.setTypeface(regular);
                d2TV.setText(Double.toString(D2));
                TextView subtotalTV = (TextView) view.findViewById(R.id.subtotalTV);
                subtotalTV.setTypeface(regular);
                subtotalTV.setText(formatter.format(subtotal));
                sum = sum + subtotal;
                counter++;
                baseLL.addView(view);
                invoiceCsr.moveToNext();
            }
//        	LinearLayout ln = new LinearLayout(this);
//        	LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, dpToPx(1));
////        	params.setMargins(dpToPx(10), 0, dpToPx(10), 0);
//        	ln.setLayoutParams(params);
//        	ln.setBackgroundColor(getResources().getColor(R.color.divider));
//        	baseLL.addView(ln);
            grandTotalTV.setText("GRAND TOTAL :Rp. " + formatter.format(sum));
        }
        finishTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//				Intent returnIntent = new Intent();
//				setResult(RESULT_OK,returnIntent);
//				finish();
//				Toast.makeText(getApplicationContext(),idPelanggan,Toast.LENGTH_LONG).show();
                Cursor csr = dataBase.selectAccount();
                csr.moveToFirst();
                String idSalesman = csr.getString(3);
                sendLog(idSalesman, String.valueOf(myLatitude), String.valueOf(myLongitude), "Order");
                Intent intent = new Intent(getApplicationContext(), DashboardCustomerActivity.class);
                intent.putExtra("idPelanggan", idPelanggan);
                startActivity(intent);
                finish();

            }
        });
    }

    public void sendLog(final String idSalesman, final String Latitude, final String Longitude, final String Remarks) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.SEND_LOG);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        json.put("idSalesman", idSalesman);
                        json.put("Latitude", Latitude);
                        json.put("Longitude", Longitude);
                        json.put("Remarks", Remarks);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
//									dialog.dismiss();
//									alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("") || arrResult.equals("null")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
//										Toast.makeText(getApplicationContext(), "sendlog gagal", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
//								Toast.makeText(getApplicationContext(), "sendlog result: "+arrResult, Toast.LENGTH_LONG).show();
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
//								Toast.makeText(getApplicationContext(), "sendlog error : "+e.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "sendlog error : " + e.toString(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

//	public void sendLog(final String idSalesman, final String Latitude, final String Longitude, final String Remarks)
//	{
////		Toast.makeText(getApplicationContext(),"SendLog Process",Toast.LENGTH_LONG).show();
//		try
//		{
//			HttpPost post = new HttpPost(ipManager.getIP()+ Constant.SEND_LOG);
//			StringEntity se;
//			HttpResponse response;
//			HttpClient client = new DefaultHttpClient();
//			HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
//			JSONObject json = new JSONObject();
//			json.put("idSalesman",idSalesman);
//			json.put("Latitude",Latitude);
//			json.put("Longitude",Longitude);
//			json.put("Remarks",Remarks);
//			se = new StringEntity( json.toString());
//			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//			post.setEntity(se);
//			response = client.execute(post);
//			StringBuilder sb = new StringBuilder();
//			InputStream in = response.getEntity().getContent();
//			BufferedReader br = new BufferedReader(new InputStreamReader(in));
//			String line;
//			while ((line = br.readLine()) != null)
//			{
//				sb.append(line);
//			}
//			String arrResult = sb.toString();
//			Toast.makeText(getApplicationContext(),arrResult,Toast.LENGTH_LONG).show();
//		}
//		catch(final Exception e)
//		{
//			Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
//		}
//		Looper.loop();
//	}

    private String getTodayDate() {
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        String dateStr;
        String monthStr = null;
        if (date < 10) {
            dateStr = "0" + Integer.toString(date);
        } else {
            dateStr = Integer.toString(date);
        }
        if (month < 10) {
            monthStr = "0" + Integer.toString(month);
        } else {
            monthStr = Integer.toString(month);
        }
        return dateStr + "/" + monthStr + "/" + Integer.toString(year);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }
}
