package com.joan.superretail.model;

public class Orders {

    String idBarang;
    String idPelanggan;
    String idSalesman;
    String Kode;
    String Nama;
    double Harga;
    String Satuan;
    String SatuanTerpilih;
    String SatuanSedang;
    String SatuanBesar;
    double IsiSedang;
    double IsiBesar;
    int Qty;
    double Subtotal;
    double D1;
    double D2;
    double Isi;
    String rowid;

    public Orders(String idBarang, String idPelanggan, String idSalesman, String Kode, String Nama, double Harga
            , String Satuan, String SatuanSedang, String SatuanBesar, double IsiSedang, double IsiBesar
            , int Qty, double Subtotal, double D1, double D2, double Isi, String SatuanTerpilih, String rowid) {
        super();
        this.idBarang = idBarang;
        this.idPelanggan = idPelanggan;
        this.idSalesman = idSalesman;
        this.Kode = Kode;
        this.Nama = Nama;
        this.Harga = Harga;
        this.Satuan = Satuan;
        this.SatuanSedang = SatuanSedang;
        this.SatuanBesar = SatuanBesar;
        this.IsiSedang = IsiSedang;
        this.IsiBesar = IsiBesar;
        this.Qty = Qty;
        this.Subtotal = Subtotal;
        this.D1 = D1;
        this.D2 = D2;
        this.Isi = Isi;
        this.SatuanTerpilih = SatuanTerpilih;
        this.rowid = rowid;
    }

    public String getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(String idBarang) {
        this.idBarang = idBarang;
    }

    public String getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(String idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public String getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getKode() {
        return Kode;
    }

    public void setKode(String Kode) {
        this.Kode = Kode;
    }

    public double getHarga() {
        return Harga;
    }

    public void setHarga(double Harga) {
        this.Harga = Harga;
    }

    public String getSatuan() {
        return Satuan;
    }

    public void setSatuan(String Satuan) {
        this.Satuan = Satuan;
    }

    public String getRowid() {
        return rowid;
    }

    public String getSatuanSedang() {
        return SatuanSedang;
    }

    public void setSatuanSedang(String SatuanSedang) {
        this.SatuanSedang = SatuanSedang;
    }

    public String getSatuanBesar() {
        return SatuanBesar;
    }

    public void setSatuanBesar(String SatuanBesar) {
        this.SatuanBesar = SatuanBesar;
    }

    public String getSatuanTerpilih() {
        return SatuanTerpilih;
    }

    public void setSatuanTerpilih(String SatuanTerpilih) {
        this.SatuanTerpilih = SatuanTerpilih;
    }

    public double getIsiSedang() {
        return IsiSedang;
    }

    public void setIsiSedang(double IsiSedang) {
        this.IsiSedang = IsiSedang;
    }

    public double getIsiBesar() {
        return IsiBesar;
    }

    public void setIsiBesar(double IsiBesar) {
        this.IsiBesar = IsiBesar;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    public double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(double Subtotal) {
        this.Subtotal = Subtotal;
    }

    public double getD1() {
        return D1;
    }

    public void setD1(double D1) {
        this.D1 = D1;
    }

    public double getD2() {
        return D2;
    }

    public void setD2(double D2) {
        this.D2 = D2;
    }

    public double getIsi() {
        return Isi;
    }

    public void setIsi(double Isi) {
        this.Isi = Isi;
    }
}
