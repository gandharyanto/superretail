package com.joan.superretail.model;

import java.util.Comparator;

public class Pelanggan {

    public static Comparator<Pelanggan> StuNameComparator = new Comparator<Pelanggan>() {

        public int compare(Pelanggan s1, Pelanggan s2) {
            String StudentName1 = s1.getName().toUpperCase();
            String StudentName2 = s2.getName().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };
    String id;
    String name;
    String levelHarga;

    public Pelanggan(String id, String name, String levelHarga) {
        super();
        this.id = id;
        this.name = name;
        this.levelHarga = levelHarga;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevelHarga() {
        return levelHarga;
    }

    public void setSelected(String levelHarga) {
        this.levelHarga = levelHarga;
    }
}
