package com.joan.superretail.model;

public class Settings {

    String IP;
    String Nama;
    String Aktif;

    public Settings(String Nama, String IP, String Aktif) {
        super();
        this.IP = IP;
        this.Nama = Nama;
        this.Aktif = Aktif;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String ip) {
        this.IP = ip;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String name) {
        this.Nama = name;
    }

    public String getAktif() {
        return Aktif;
    }

    public void setAktif(String Aktif) {
        this.Aktif = Aktif;
    }
}
